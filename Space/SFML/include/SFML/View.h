/*
 * View.h
 *
 *  Created on: 29-aug.-2013
 *      Author: tom
 */

#ifndef SFML_VIEW_H
#define SFML_VIEW_H

#include "Space/Camera.h"
#include "Space/Perspective.h"
#include "Space/View.h"

#include "Font/Numbers.h"
#include "Geometry/HyperSphere.h"
#include "Geometry/Zonotope.h"

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/OpenGL.hpp>

#include <vector>
#include <utility>
#include <iterator>
#include <stack>
#include <sstream>

namespace SFML {

class View: public Space::View {
private:
    using Camera = Space::Camera<GLfloat, Space::Perspective<GLfloat>>;
    sf::RenderWindow mWindow;
    sf::View mHUD;
    //sf::CircleShape mCircle;
    sf::Font mFont;
    double mFPS;
    void fps(double time) {
        std::ostringstream ss;
        // smooth the FPS measurement over the period of a second
        mFPS = (1.0 - time)*mFPS + 1.0;
        //ss << static_cast<unsigned>(mFPS);
        ss << "test";
        sf::Text fps;
        fps.setFont(mFont);
        fps.setCharacterSize(50);
        fps.setStyle(sf::Text::Bold);
        fps.setColor(sf::Color::White);
        fps.setPosition(0.5, 0.5);
        fps.setString(ss.str());
        mWindow.draw(fps);
    }
    static void project(Space::Perspective<GLfloat> const& projection) {
    	GLfloat sum = (projection.far() + projection.near())/(projection.far() - projection.near());
    	GLfloat product = 2*projection.far()*projection.near()/(projection.far() - projection.near());
        GLfloat matrix[16] =
        { 1.0, 0.0, 0.0, 0.0
        , 0.0, 1.0, 0.0, 0.0
        , 0.0, 0.0, projection.normal()[2]*sum, -1.0
        , 0.0, 0.0, projection.normal()[2]*product, 0.0
        //, projection.normal()[0], projection.normal()[1], projection.normal()[2], 0.0
        };
        //glMultMatrixf(matrix);
    	gluPerspective(10.0, 1.0, 10.0, 100.0);
    	glScalef(1.0, 1.33, 1.0);
    }
    static void rotate(Geometry::Rotation<GLfloat, 3> const& rotation) {
    	gluLookAt( 0, 0, -10, 0, 0, 0, 0, 1, 0 );
        /*Math::Quaternion<GLfloat> const& q = rotation.quaternion();
        GLfloat matrix[16] =
        { q.scalar()*q.scalar() + q.vector()[0]*q.vector()[0] - q.vector()[1]*q.vector()[1] - q.vector()[2]*q.vector()[2], 2*q.vector()[0]*q.vector()[1] - 2*q.scalar()*q.vector()[2], 2*q.vector()[0]*q.vector()[2] + 2*q.scalar()*q.vector()[1], 0.0
        , 2*q.vector()[0]*q.vector()[1] + 2*q.scalar()*q.vector()[2], q.scalar()*q.scalar() - q.vector()[0]*q.vector()[0] + q.vector()[1]*q.vector()[1] - q.vector()[2]*q.vector()[2], 2*q.vector()[1]*q.vector()[2] - 2*q.scalar()*q.vector()[0], 0.0
        , 2*q.vector()[0]*q.vector()[2] - 2*q.scalar()*q.vector()[1], 2*q.vector()[1]*q.vector()[2] + 2*q.scalar()*q.vector()[0], q.scalar()*q.scalar() - q.vector()[0]*q.vector()[0] - q.vector()[1]*q.vector()[1] + q.vector()[2]*q.vector()[2], 0.0
        , 0.0, 0.0, 0.0, 1.0};
        glMultMatrixf(matrix);*/
    }
public:
    View(unsigned horizontal, unsigned vertical, Camera const& c)
    : mWindow(sf::VideoMode(horizontal, vertical, 32), "SpaceInvaders", sf::Style::Default, sf::ContextSettings(24, 8))
    , mFPS(0.0) {
    	//mFont.loadFromMemory(Font::Numbers::data, Font::Numbers::length);
    	if (!mFont.loadFromFile("/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-L.ttf")) {
    		throw 5;
    	}
        mHUD.setCenter(0.0, 0.0);
        mHUD.setSize(2.0*horizontal/vertical, 2.0);
        mWindow.setView(mHUD);
        glShadeModel(GL_SMOOTH);
        glEnable(GL_CULL_FACE);
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        project(c.projection());
        //mFont.loadFromFile("liberation.ttf");
        //glMatrixMode(GL_MODELVIEW);
        //glLoadIdentity();
        glTranslatef(c.position()[0], c.position()[1], c.position()[2]);
        //rotate(c.rotation());
//        GLfloat position[] = {0.0, 0.0, 1.0, 0.0};
//        glLightfv(GL_LIGHT0, GL_POSITION, position);
    }
    virtual ~View() {
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHTING);
        glDisable(GL_CULL_FACE);
    }
    sf::RenderWindow const& window() const {
        return mWindow;
    }
    sf::RenderWindow& window() {
        return mWindow;
    }
    virtual void update(double time) {
//        mWindow.clear(sf::Color::Black);
        //save the openGLstate if using OpenGL
        mWindow.pushGLStates();
        fps(time);
        //restore OpenGL setting that were saved earlier
        mWindow.popGLStates();
        mWindow.display();
    }
};

} /* namespace SFML */
#endif /* SFML_VIEW_H */
