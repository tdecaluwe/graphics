#ifndef SFMLCONTROLLER_H_
#define SFMLCONTROLLER_H_

#include "Space/Controller.h"

#include "Event/Delegate.h"

#include <SFML/Window/Keyboard.hpp>

#include <vector>
#include <utility>

namespace Zombie {

namespace SFML {

class Controller
: public ::Space::Controller {
private:
    std::vector<std::pair<sf::Keyboard::Key, Event::Delegate<void()>>> mActions;
public:
    void add(sf::Keyboard::Key key, Event::Delegate<void()> const& delegate) {
        mActions.push_back(std::make_pair(key, delegate));
    }
    virtual void up(Event::Delegate<void()> const& delegate) {
        add(sf::Keyboard::Key::Up, delegate);
    }
    virtual void down(Event::Delegate<void()> const& delegate) {
        add(sf::Keyboard::Key::Down, delegate);
    }
    virtual void left(Event::Delegate<void()> const& delegate) {
        add(sf::Keyboard::Key::Left, delegate);
    }
    virtual void right(Event::Delegate<void()> const& delegate) {
        add(sf::Keyboard::Key::Right, delegate);
    }
    virtual bool exit() const {
        return sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape);
    }
    virtual void update() {
        for (auto iterator = mActions.begin(); iterator != mActions.end(); ++iterator) {
            if(sf::Keyboard::isKeyPressed(iterator->first)) {
                iterator->second();
            }
        }
    }
};

} /* namespace SFML */

} /* namespace Zombie */
#endif /* SFMLCONTROLLER_H_ */
