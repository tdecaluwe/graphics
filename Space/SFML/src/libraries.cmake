enable_language(C)

find_package(SFML 2 COMPONENTS graphics system window REQUIRED)
find_package(OpenGL REQUIRED)

include_directories(${SFML_INCLUDE_DIR})

set(LIBRARIES ${SFML_LIBRARIES} ${OPENGL_LIBRARIES})
