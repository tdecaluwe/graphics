/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef VIEW_H_
#define VIEW_H_

#include "Space/Object.h"

namespace Space {

// forward declarations
class Player;
class Enemy;
class Rock;

class View
/*: public Object::Reader */{
public:
    virtual ~View() = default;
    //virtual void visit(Player const& p) = 0;
    //virtual void visit(Enemy const& p) = 0;
    //virtual void visit(Rock const& p) = 0;
    virtual void update(double time) = 0;
};

} /* namespace Space */
#endif /* VIEW_H_ */
