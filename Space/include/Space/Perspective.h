/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PERSPECTIVE_H_
#define PERSPECTIVE_H_

#include "Math/Point.h"
#include "Math/Vector.h"

namespace Space {

template <class T>
class Perspective {
private:
    Math::Vector<T, 3> mNormal;
    T mNear;
    T mFar;
public:
    constexpr Perspective(T near, T far);
    constexpr Perspective(Math::Vector<T, 3> const& normal);
    Math::Vector<T, 3> const& normal() const;
    Math::Vector<T, 3>& normal();
    T const& far() const;
    T& far();
    T const& near() const;
    T& near();
};

template <class T>
inline constexpr Perspective<T>::Perspective(T near, T far)
: mNormal { 0.0, 0.0, 1.0 }
, mNear { near }
, mFar { far } {}

template <class T>
inline constexpr Perspective<T>::Perspective(Math::Vector<T, 3> const& normal)
: mNormal { normal } {}

template <class T>
inline Math::Vector<T, 3> const& Perspective<T>::normal() const {
    return mNormal;
}

template <class T>
inline Math::Vector<T, 3>& Perspective<T>::normal() {
    return mNormal;
}

template <class T>
T const& Perspective<T>::far() const {
	return mFar;
}

template <class T>
T& Perspective<T>::far() {
	return mFar;
}

template <class T>
T const& Perspective<T>::near() const {
	return mNear;
}

template <class T>
T& Perspective<T>::near() {
	return mNear;
}

} /* namespace Space */
#endif /* PERSPECTIVE_H_ */
