/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef OBJECT_H_
#define OBJECT_H_

#include "Visit/Visitor.h"
#include "Visit/ConstVisitor.h"

namespace Space {

// forward declarations
class Player;
class Enemy;
class Rock;

class Object {
protected:
    using Reader = Visit::Visitor<Player const, Enemy const, Rock const>;
    using ConstReader = Visit::ConstVisitor<Player const, Enemy const, Rock const>;
public:
    virtual ~Object() = 0;
    virtual void accept(Reader& v) const = 0;
    virtual void accept(ConstReader const& v) const = 0;
};

} /* namespace Space */
#endif /* OBJECT_H_ */
