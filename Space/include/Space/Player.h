/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "Space/Object.h"

#include "Visit/Visitor.h"
#include "Visit/ConstVisitor.h"

namespace Space {

class Player
: public Object {
public:
    virtual ~Player() = default;
    virtual void accept(Reader& v) const {
        v.visit(*this);
    }
    virtual void accept(ConstReader const& v) const {
        v.visit(*this);
    }
};

/*
        if (Geometry::Compare<double>::greater(length(mSteeringDirection), 0.0)) {
            player().accelerate(player().acceleration()/length(mSteeringDirection)*mSteeringDirection*time);
        }
        player().decelerate(gravity()*friction()*player().speed()*time);
 */

} /* namespace Space */
#endif /* PLAYER_H_ */
