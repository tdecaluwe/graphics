/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef CAMERA_H_
#define CAMERA_H_

#include "Geometry/Rotation.h"

#include "Math/Point.h"
#include "Math/Vector.h"

namespace Space {

template <class T, class P>
class Camera {
private:
    Math::Point<T, 3> mPosition;
    Geometry::Rotation<T, 3> mRotation;
    P mProjection;
public:
    constexpr Camera(Math::Point<T, 3> const& position, T near, T far);
    Math::Point<T, 3> const& position() const;
    Math::Point<T, 3>& position();
    Geometry::Rotation<T, 3> const& rotation() const;
    Geometry::Rotation<T, 3>& rotation();
    P const& projection() const;
    P& projection();
    Math::Point<int, 2> transform(Math::Point<T, 3>) {
        return {};
    }
    /*Geometry::Ray<T, 3> invert(Math::Point<int, 2>) {
        return {};
    }*/
};

template <class T, class P>
inline constexpr Camera<T, P>::Camera(Math::Point<T, 3> const& position, T near, T far)
: mPosition { position }
, mRotation { }
, mProjection { near, far } {}

template <class T, class P>
Math::Point<T, 3> const& Camera<T, P>::position() const {
    return mPosition;
}

template <class T, class P>
Math::Point<T, 3>& Camera<T, P>::position() {
    return mPosition;
}

template <class T, class P>
Geometry::Rotation<T, 3> const& Camera<T, P>::rotation() const {
    return mRotation;
}

template <class T, class P>
Geometry::Rotation<T, 3>& Camera<T, P>::rotation() {
    return mRotation;
}

template <class T, class P>
P const& Camera<T, P>::projection() const {
    return mProjection;
}

template <class T, class P>
P& Camera<T, P>::projection() {
    return mProjection;
}

} /* namespace Space */
#endif /* CAMERA_H_ */
