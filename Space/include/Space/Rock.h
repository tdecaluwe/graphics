/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ROCK_H_
#define ROCK_H_

#include "Space/Object.h"

#include "Visit/Visitor.h"
#include "Visit/ConstVisitor.h"

namespace Space {

class Rock
: public Object {
public:
    virtual ~Rock() = default;
    virtual void accept(Reader& v) const {
        v.visit(*this);
    }
    virtual void accept(ConstReader const& v) const {
        v.visit(*this);
    }
};

} /* namespace Space */
#endif /* ROCK_H_ */
