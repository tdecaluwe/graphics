#ifndef ACTOR_H_
#define ACTOR_H_

#include "Space/Object.h"

#include "Geometry/Convex.h"
#include "Math/Point.h"
#include "Math/Vector.h"

#include <memory>
#include <iostream>

namespace Space {

class Actor
: public Object {
private:
    std::unique_ptr<Geometry::Convex<double, 2>> uGeometry;
    Math::Vector<double, 2> mSpeed;
    double mAcceleration;
public:
    Actor(std::unique_ptr<Geometry::Convex<double, 2>>&& g);
    Actor(Math::Point<double, 2>&& p, double r);
    Math::Vector<double, 2> const& speed() const;
    Math::Vector<double, 2>& speed();
    void accelerate(Math::Vector<double, 2> const& s);
    void decelerate(Math::Vector<double, 2> const& s);
    double const& acceleration() const;
    double& acceleration();
    virtual void update(double time);
};

inline Actor::Actor(std::unique_ptr<Geometry::Convex<double, 2>>&& g)
: uGeometry { g }
, mSpeed { 0.0, 0.0 }
, mAcceleration { 0.0 } {}

inline Math::Vector<double, 2> const& Actor::speed() const {
    return mSpeed;
}

inline Math::Vector<double, 2>& Actor::speed() {
    return mSpeed;
}

inline void Actor::accelerate(Math::Vector<double, 2> const& s) {
    mSpeed += s;
}

inline void Actor::decelerate(Math::Vector<double, 2> const& s) {
    mSpeed -= s;
}

inline double const& Actor::acceleration() const {
    return mAcceleration;
}

inline double& Actor::acceleration() {
    return mAcceleration;
}

} /* namespace Space */
#endif /* ACTOR_H_ */
