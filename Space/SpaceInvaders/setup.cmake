#reset local variables
unset(PROJECTS)

# project name
get_filename_component(DIRECTORY_NAME ${CMAKE_CURRENT_LIST_DIR} NAME)
project(${DIRECTORY_NAME} CXX)

# set default build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif(NOT CMAKE_BUILD_TYPE)

set(PROJECT_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR})
