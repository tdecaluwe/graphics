/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#include "SFML/View.h"

#include "Space/Camera.h"
#include "Space/Perspective.h"

#include "Geometry/HyperSphere.h"
#include "Geometry/Soup.h"

#include "Profile/Timer.h"

#include "SFML/OpenGL.hpp"
#include "SFML/Graphics.hpp"

#include <iostream>
#include <cmath>

using Geometry::Sphere;
using Geometry::Soup;

using Geometry::Point;

using Space::Camera;
using Space::Perspective;

using SFML::View;

using namespace std;

int main()
{
	Profile::Timer<std::chrono::milliseconds> timer;
	timer.start();
    Camera<GLfloat, Perspective<GLfloat>> camera = { { 0.0, 0.0, -10.0 }, 10.0, 100.0 };
    camera.rotation() = Geometry::Rotation<GLfloat, 3>::z(0.0);
    View view = { 800, 600, camera };
    auto& window = view.window();
	//sf::RenderWindow window(sf::VideoMode(800, 600), "OpenGL", sf::Style::Default, sf::ContextSettings(32));

    // Camera
    GLdouble eyex = 0;
    GLdouble eyey = 0;
    GLdouble eyez = 10;

    Soup<GLfloat> s = triangulate(Sphere<GLfloat>(1.0));

    GLfloat green[] = { 0.0, 1.0, 0.0, 1.0 };
    GLfloat black[] = { 0.0, 0.0, 0.0, 1.0 };
    GLfloat white[] = { 1.0, 1.0, 1.0, 1.0 };

    // Main loop
    while (window.isOpen())
    {
        // Checking events
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Close the window
            if (event.type == sf::Event::Closed)
                window.close();

            // Resize the window
            if (event.type == sf::Event::Resized)
                glViewport(0, 0, event.size.width, event.size.height);
        }

        // Close the window
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            window.close();

        // Clear the window
        window.clear(sf::Color::Black);

        // Viewport
        glViewport( 0, 0, window.getSize().x, window.getSize().y );

        // Matrix Mode
        //glMatrixMode( GL_PROJECTION );

        // Matrix Load Identity
        //glLoadIdentity();

        // Perspective
        //gluPerspective(window.getSize().y/45.0, 1.0f*window.getSize().x/window.getSize().y, 0.0f, 100.0f);

        // Clear color buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Set Matrix Mode
        glMatrixMode( GL_MODELVIEW );

        // Matrix Load Identity
        glLoadIdentity();

        // Change camera position
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            eyey -= 0.01f;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            eyey += 0.01f;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            eyex -= 0.01f;
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            eyex += 0.01f;

        printf("%f %f\n", eyex, eyey);

        // Set the camera
        gluLookAt( eyex, -eyey, eyez, eyex, -eyey, 0, 0, 1, 0 );

        glScalef(1,-1,1);

        glMaterialfv(GL_FRONT, GL_AMBIENT, black);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, green);

        GLfloat scale = 1.0;

        glBegin(GL_TRIANGLES);
        for (unsigned i = 0; i < 320; ++i) {
        	//Math::Vector<GLfloat, 3> n = normal(s[i]);
            //glNormal3f(n[0], n[1], n[2]);
        	glNormal3f(scale*s[i][0][0], scale*s[i][0][1], scale*s[i][0][2]);
            glVertex3f(scale*s[i][0][0], scale*s[i][0][1], scale*s[i][0][2]);
            glNormal3f(scale*s[i][1][0], scale*s[i][1][1], scale*s[i][1][2]);
            glVertex3f(scale*s[i][1][0], scale*s[i][1][1], scale*s[i][1][2]);
            glNormal3f(scale*s[i][2][0], scale*s[i][2][1], scale*s[i][2][2]);
            glVertex3f(scale*s[i][2][0], scale*s[i][2][1], scale*s[i][2][2]);
        }
        glEnd();

        // Flush the scene
        glFlush();

        view.update(timer.split());
    }
    return 0;
}
