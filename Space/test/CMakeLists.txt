cmake_minimum_required(VERSION 2.8.3)

# reset local variables
set(FRAMEWORK)

# run compile tests
add_subdirectory(compile)
add_subdirectory(reject)

# find boost
if(NOT DEFINED Boost_UNIT_TEST_FRAMEWORK_FOUND)
    find_package(Boost COMPONENTS unit_test_framework)
    find_package(Boost)
endif()

# provide test module sources
configure_file(Test.cpp.in ${CMAKE_CURRENT_BINARY_DIR}/src/Test.cpp @ONLY)

if(${Boost_FOUND})
    if(${Boost_UNIT_TEST_FRAMEWORK_FOUND})
        # dynamically link boost unit_test_framework if it exists
        add_definitions(-DBOOST_TEST_DYN_LINK)
        set(FRAMEWORK ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY})
    else()
        # build the unit_test_framework from the headers
        message("-- Boost testing framework will be compiled from source")
    endif()
    add_subdirectory(src)
else()
    # if boost can't be found, testing is disabled
    message("-- Testing will be disabled")
endif()
