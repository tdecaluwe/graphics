#include "Math/Vector.h"

extern int i, j;
extern float f, g;
extern double d, e;

void function() {
    {
        Math::Vector<float, 2> b { };
        Math::Vector<float, 2> c = { };
    }
    {
        Math::Vector<float, 2> a(f, g);
        Math::Vector<float, 2> b { f, g };
        Math::Vector<float, 2> c = { f, g };
    }
    {
        Math::Vector<float, 2> a(i, g);
        Math::Vector<float, 2> b(f, j);
        Math::Vector<float, 2> c(f, g);
    }
    {
        Math::Vector<double, 2> a = { f, g };
        Math::Vector<double, 2> b = { f, e };
        Math::Vector<double, 2> c = { d, g };
    }
}
