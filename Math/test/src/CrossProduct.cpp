#include "Math/CrossProduct.h"

#include "Math/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using Math::Vector;

using boost::mpl::list;

BOOST_AUTO_TEST_SUITE(TestCrossProduct)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(CrossProduct, T, Types) {
    Vector<T, 3> zero { };
    Vector<T, 3> x { 2.0, 0.0, 0.0 };
    Vector<T, 3> y { 0.0, 2.0, 0.0 };
    Vector<T, 3> z { 0.0, 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x%x, zero);
    BOOST_CHECK_EQUAL(x%y, 2.0*z);
    BOOST_CHECK_EQUAL(x%z, -2.0*y);
    BOOST_CHECK_EQUAL(y%x, -2.0*z);
    BOOST_CHECK_EQUAL(y%y, zero);
    BOOST_CHECK_EQUAL(y%z, 2.0*x);
    BOOST_CHECK_EQUAL(z%x, 2.0*y);
    BOOST_CHECK_EQUAL(z%y, -2.0*x);
    BOOST_CHECK_EQUAL(z%z, zero);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Verbose, T, Types) {
    Vector<T, 3> zero { };
    Vector<T, 3> x { 2.0, 0.0, 0.0 };
    Vector<T, 3> y { 0.0, 2.0, 0.0 };
    Vector<T, 3> z { 0.0, 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x%x, cross(x, x));
    BOOST_CHECK_EQUAL(x%y, cross(x, y));
    BOOST_CHECK_EQUAL(x%z, cross(x, z));
    BOOST_CHECK_EQUAL(y%x, cross(y, x));
    BOOST_CHECK_EQUAL(y%y, cross(y, y));
    BOOST_CHECK_EQUAL(y%z, cross(y, z));
    BOOST_CHECK_EQUAL(z%x, cross(z, x));
    BOOST_CHECK_EQUAL(z%y, cross(z, y));
    BOOST_CHECK_EQUAL(z%z, cross(z, z));
}

BOOST_AUTO_TEST_SUITE_END()
