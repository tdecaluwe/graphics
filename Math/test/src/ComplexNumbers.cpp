#include "Math/Complex.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using Math::Complex;

using boost::mpl::list;

BOOST_AUTO_TEST_SUITE(ComplexNumbers)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(DefaultConstructor, Type, Types) {
    Complex<Type> zero { };
    BOOST_CHECK_EQUAL(zero.real(), 0.0);
    BOOST_CHECK_EQUAL(zero.imaginary(), 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, Type, Types) {
    Complex<Type> complex { 1.0, 2.0 };
    BOOST_CHECK_EQUAL(complex.real(), 1.0);
    BOOST_CHECK_EQUAL(complex.imaginary(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CopyConstructor, Type, Types) {
    Complex<Type> complex { 1.0, 2.0 };
    Complex<Type> copy { complex };
    BOOST_CHECK_EQUAL(copy.real(), 1.0);
    BOOST_CHECK_EQUAL(copy.imaginary(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Assignment, Type, Types) {
    Complex<Type> complex { 1.0, 2.0 };
    Complex<Type> copy;
    copy = complex;
    BOOST_CHECK_EQUAL(copy.real(), 1.0);
    BOOST_CHECK_EQUAL(copy.imaginary(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Comparable, Type, Types) {
    Complex<Type> a { 1.0, 1.0 };
    Complex<Type> b { 1.0, 1.0 };
    Complex<Type> real { 1.0, 0.0 };
    Complex<Type> complex { 0.0, 1.0 };
    BOOST_CHECK_EQUAL(a, a);
    BOOST_CHECK_EQUAL(a, b);
    BOOST_CHECK_NE(a, real);
    BOOST_CHECK_NE(a, complex);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Unary, Type, Types) {
    Complex<Type> a { 6.0, -2.0 };
    Complex<Type> b { -6.0, 2.0 };
    BOOST_CHECK_EQUAL(+a, a);
    BOOST_CHECK_EQUAL(-a, b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Addition, Type, Types) {
    Complex<Type> a { 6.0, -2.0 };
    Complex<Type> b { -1.0, 1.0 };
    Complex<Type> c { 5.0, -1.0 };
    BOOST_CHECK_EQUAL(a + b, c);
    BOOST_CHECK_EQUAL(a += b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Subtraction, Type, Types) {
    Complex<Type> a { 6.0, -2.0 };
    Complex<Type> b { -1.0, 1.0 };
    Complex<Type> c { 7.0, -3.0 };
    BOOST_CHECK_EQUAL(a - b, c);
    BOOST_CHECK_EQUAL(a -= b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Multiplication, Type, Types) {
    Complex<Type> x { 2.0, 0.0 };
    Complex<Type> y { 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x*x, 2.0*x);
    BOOST_CHECK_EQUAL(x*y, 2.0*y);
    BOOST_CHECK_EQUAL(y*x, 2.0*y);
    BOOST_CHECK_EQUAL(y*y, -2.0*x);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Division, Type, Types) {
    Complex<Type> x { 2.0, 0.0 };
    Complex<Type> y { 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x/x, 0.5*x);
    BOOST_CHECK_EQUAL(x/y, -0.5*y);
    BOOST_CHECK_EQUAL(y/x, 0.5*y);
    BOOST_CHECK_EQUAL(y/y, 0.5*x);
}

BOOST_AUTO_TEST_SUITE_END()
