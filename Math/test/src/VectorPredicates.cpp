#include "Math/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

using Math::Vector;

BOOST_AUTO_TEST_SUITE(VectorPredicates)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Clockwise, T, Types) {
    Vector<T, 2> a { 1.0, 0.0 };
    Vector<T, 2> b { 0.0, 1.0 };
    BOOST_CHECK(clockwise(b, a));
    BOOST_CHECK(!clockwise(a, a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CounterClockwise, T, Types) {
    Vector<T, 2> a { 1.0, 0.0 };
    Vector<T, 2> b { 0.0, 1.0 };
    BOOST_CHECK(counterClockwise(a, b));
    BOOST_CHECK(!counterClockwise(a, a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ClockwiseWithZero, T, Types) {
    Vector<T, 2> a { 1.0, 0.0 };
    Vector<T, 2> b { 0.0, 0.0 };
    BOOST_CHECK(!clockwise(b, a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CounterClockwiseWithZero, T, Types) {
    Vector<T, 2> a { 1.0, 0.0 };
    Vector<T, 2> b { 0.0, 0.0 };
    BOOST_CHECK(!counterClockwise(a, b));
}

BOOST_AUTO_TEST_SUITE_END()
