#include "Math/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

#include <utility>

using Math::Vector;

using boost::mpl::list;

BOOST_AUTO_TEST_SUITE(Vectors)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, Type, Types) {
    Vector<Type, 3> vector { 1.0, 2.0, 3.0 };
    BOOST_CHECK_EQUAL(vector[0], 1.0);
    BOOST_CHECK_EQUAL(vector[1], 2.0);
    BOOST_CHECK_EQUAL(vector[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(DefaultInitialization, Type, Types) {
    Vector<Type, 3> vector { 1.0, 2.0, 3.0 };
    Vector<Type, 3>* pointer = new (&vector) Vector<Type, 3>;
    BOOST_CHECK_EQUAL(vector[0], 1.0);
    BOOST_CHECK_EQUAL(vector[1], 2.0);
    BOOST_CHECK_EQUAL(vector[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ValueInitialization, Type, Types) {
    Vector<Type, 3> vector { 1.0, 2.0, 3.0 };
    Vector<Type, 3>* pointer = new (&vector) Vector<Type, 3> {};
    BOOST_CHECK_EQUAL(vector[0], 0.0);
    BOOST_CHECK_EQUAL(vector[1], 0.0);
    BOOST_CHECK_EQUAL(vector[2], 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Origin, Type, Types) {
    Vector<Type, 3> vector = Vector<Type, 3>::origin();
    BOOST_CHECK_EQUAL(vector[0], 0.0);
    BOOST_CHECK_EQUAL(vector[1], 0.0);
    BOOST_CHECK_EQUAL(vector[2], 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CopyConstructor, Type, Types) {
    Vector<Type, 3> vector { 1.0, 2.0, 3.0 };
    Vector<Type, 3> copy { vector };
    BOOST_CHECK_EQUAL(copy[0], 1.0);
    BOOST_CHECK_EQUAL(copy[1], 2.0);
    BOOST_CHECK_EQUAL(copy[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Assignment, Type, Types) {
    Vector<Type, 3> vector { 1.0, 2.0, 3.0 };
    Vector<Type, 3> other;
    other = vector;
    BOOST_CHECK_EQUAL(other[0], 1.0);
    BOOST_CHECK_EQUAL(other[1], 2.0);
    BOOST_CHECK_EQUAL(other[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Comparable, Type, Types) {
    Vector<Type, 3> a { 1.0, 1.0, 1.0 };
    Vector<Type, 3> b { 1.0, 1.0, 1.0 };
    Vector<Type, 3> xy { 1.0, 1.0, 0.0 };
    Vector<Type, 3> xz { 1.0, 0.0, 1.0 };
    Vector<Type, 3> yz { 0.0, 1.0, 1.0 };
    BOOST_CHECK_EQUAL(a, a);
    BOOST_CHECK_EQUAL(a, b);
    BOOST_CHECK_NE(a, xy);
    BOOST_CHECK_NE(a, xz);
    BOOST_CHECK_NE(a, yz);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Unary, Type, Types) {
    Vector<Type, 3> a { 6.0, 2.0, -2.0 };
    Vector<Type, 3> b { -6.0, -2.0, 2.0 };
    BOOST_CHECK_EQUAL(+a, a);
    BOOST_CHECK_EQUAL(-a, b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Addition, Type, Types) {
    Vector<Type, 3> a { 6.0, 2.0, -2.0 };
    Vector<Type, 3> b { -1.0, 1.0, 3.0 };
    Vector<Type, 3> c { 5.0, 3.0, 1.0 };
    BOOST_CHECK_EQUAL(a + b, c);
    BOOST_CHECK_EQUAL(a += b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Subtraction, Type, Types) {
    Vector<Type, 3> a { 6.0, 2.0, -2.0 };
    Vector<Type, 3> b { -1.0, 1.0, 3.0 };
    Vector<Type, 3> c { 7.0, 1.0, -5.0 };
    BOOST_CHECK_EQUAL(a - b, c);
    BOOST_CHECK_EQUAL(a -= b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ScalarProduct, Type, Types) {
    Vector<Type, 3> a { 6.0, 2.0, -2.0 };
    Vector<Type, 3> b { 12.0, 4.0, -4.0 };
    BOOST_CHECK_EQUAL(2.0*a, b);
    BOOST_CHECK_EQUAL(a*2.0, b);
    BOOST_CHECK_EQUAL(a *= 2.0, b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(DotProduct, Type, Types) {
    Vector<Type, 3> x { 2.0, 0.0, 0.0 };
    Vector<Type, 3> y { 0.0, 2.0, 0.0 };
    Vector<Type, 3> z { 0.0, 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x*x, 4.0);
    BOOST_CHECK_EQUAL(x*y, 0.0);
    BOOST_CHECK_EQUAL(x*z, 0.0);
    BOOST_CHECK_EQUAL(y*x, 0.0);
    BOOST_CHECK_EQUAL(y*y, 4.0);
    BOOST_CHECK_EQUAL(y*z, 0.0);
    BOOST_CHECK_EQUAL(z*x, 0.0);
    BOOST_CHECK_EQUAL(z*y, 0.0);
    BOOST_CHECK_EQUAL(z*z, 4.0);
}

BOOST_AUTO_TEST_SUITE_END()
