#include "Math/Dual.h"
#include "Math/Quaternion.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using Math::Dual;
using Math::Quaternion;

using boost::mpl::list;

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(DualQuaternionBasicElements, T, Types) {
    Dual<Quaternion<T>> r = { Quaternion<T>::unity(), Quaternion<T>::zero() };
    Dual<Quaternion<T>> i = { Quaternion<T>::i(), Quaternion<T>::zero() };
    Dual<Quaternion<T>> j = { Quaternion<T>::j(), Quaternion<T>::zero() };
    Dual<Quaternion<T>> k = { Quaternion<T>::k(), Quaternion<T>::zero() };
    BOOST_CHECK_EQUAL(r*r, r);
    BOOST_CHECK_EQUAL(r*i, i);
    BOOST_CHECK_EQUAL(r*j, j);
    BOOST_CHECK_EQUAL(r*k, k);
    BOOST_CHECK_EQUAL(i*r, i);
    BOOST_CHECK_EQUAL(i*i, -r);
    BOOST_CHECK_EQUAL(i*j, k);
    BOOST_CHECK_EQUAL(i*k, -j);
    BOOST_CHECK_EQUAL(j*r, j);
    BOOST_CHECK_EQUAL(j*i, -k);
    BOOST_CHECK_EQUAL(j*j, -r);
    BOOST_CHECK_EQUAL(j*k, i);
    BOOST_CHECK_EQUAL(k*r, k);
    BOOST_CHECK_EQUAL(k*i, j);
    BOOST_CHECK_EQUAL(k*j, -i);
    BOOST_CHECK_EQUAL(k*k, -r);
    Dual<Quaternion<T>> re = { Quaternion<T>::zero(), Quaternion<T>::unity() };
    Dual<Quaternion<T>> ie = { Quaternion<T>::zero(), Quaternion<T>::i() };
    Dual<Quaternion<T>> je = { Quaternion<T>::zero(), Quaternion<T>::j() };
    Dual<Quaternion<T>> ke = { Quaternion<T>::zero(), Quaternion<T>::k() };
    BOOST_CHECK_EQUAL(re*r, re);
    BOOST_CHECK_EQUAL(re*i, ie);
    BOOST_CHECK_EQUAL(re*j, je);
    BOOST_CHECK_EQUAL(re*k, ke);
    BOOST_CHECK_EQUAL(ie*r, ie);
    BOOST_CHECK_EQUAL(ie*i, -re);
    BOOST_CHECK_EQUAL(ie*j, ke);
    BOOST_CHECK_EQUAL(ie*k, -je);
    BOOST_CHECK_EQUAL(je*r, je);
    BOOST_CHECK_EQUAL(je*i, -ke);
    BOOST_CHECK_EQUAL(je*j, -re);
    BOOST_CHECK_EQUAL(je*k, ie);
    BOOST_CHECK_EQUAL(ke*r, ke);
    BOOST_CHECK_EQUAL(ke*i, je);
    BOOST_CHECK_EQUAL(ke*j, -ie);
    BOOST_CHECK_EQUAL(ke*k, -re);
    BOOST_CHECK_EQUAL(r*re, re);
    BOOST_CHECK_EQUAL(r*ie, ie);
    BOOST_CHECK_EQUAL(r*je, je);
    BOOST_CHECK_EQUAL(r*ke, ke);
    BOOST_CHECK_EQUAL(i*re, ie);
    BOOST_CHECK_EQUAL(i*ie, -re);
    BOOST_CHECK_EQUAL(i*je, ke);
    BOOST_CHECK_EQUAL(i*ke, -je);
    BOOST_CHECK_EQUAL(j*re, je);
    BOOST_CHECK_EQUAL(j*ie, -ke);
    BOOST_CHECK_EQUAL(j*je, -re);
    BOOST_CHECK_EQUAL(j*ke, ie);
    BOOST_CHECK_EQUAL(k*re, ke);
    BOOST_CHECK_EQUAL(k*ie, je);
    BOOST_CHECK_EQUAL(k*je, -ie);
    BOOST_CHECK_EQUAL(k*ke, -re);
    Dual<Quaternion<T>> zero = { Quaternion<T>::zero(), Quaternion<T>::zero() };
    BOOST_CHECK_EQUAL(re*re, zero);
    BOOST_CHECK_EQUAL(re*ie, zero);
    BOOST_CHECK_EQUAL(re*je, zero);
    BOOST_CHECK_EQUAL(re*ke, zero);
    BOOST_CHECK_EQUAL(ie*re, zero);
    BOOST_CHECK_EQUAL(ie*ie, zero);
    BOOST_CHECK_EQUAL(ie*je, zero);
    BOOST_CHECK_EQUAL(ie*ke, zero);
    BOOST_CHECK_EQUAL(je*re, zero);
    BOOST_CHECK_EQUAL(je*ie, zero);
    BOOST_CHECK_EQUAL(je*je, zero);
    BOOST_CHECK_EQUAL(je*ke, zero);
    BOOST_CHECK_EQUAL(ke*re, zero);
    BOOST_CHECK_EQUAL(ke*ie, zero);
    BOOST_CHECK_EQUAL(ke*je, zero);
    BOOST_CHECK_EQUAL(ke*ke, zero);
}
