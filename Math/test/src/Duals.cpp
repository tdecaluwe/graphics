#include "Math/Dual.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

#include <utility>

using Math::Dual;

using boost::mpl::list;

BOOST_AUTO_TEST_SUITE(Duals)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(DefaultConstructor, Type, Types) {
    Dual<Type> zero { };
    BOOST_CHECK_EQUAL(zero.real(), 0.0);
    BOOST_CHECK_EQUAL(zero.dual(), 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, Type, Types) {
    Dual<Type> dual { 1.0, 2.0 };
    BOOST_CHECK_EQUAL(dual.real(), 1.0);
    BOOST_CHECK_EQUAL(dual.dual(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CopyConstructor, Type, Types) {
    Dual<Type> dual { 1.0, 2.0 };
    Dual<Type> copy { dual };
    BOOST_CHECK_EQUAL(copy.real(), 1.0);
    BOOST_CHECK_EQUAL(copy.dual(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Assignment, Type, Types) {
    Dual<Type> dual { 1.0, 2.0 };
    Dual<Type> copy;
    copy = dual;
    BOOST_CHECK_EQUAL(copy.real(), 1.0);
    BOOST_CHECK_EQUAL(copy.dual(), 2.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Comparable, Type, Types) {
    Dual<Type> a { 1.0, 1.0 };
    Dual<Type> b { 1.0, 1.0 };
    Dual<Type> real { 1.0, 0.0 };
    Dual<Type> dual { 0.0, 1.0 };
    BOOST_CHECK_EQUAL(a, a);
    BOOST_CHECK_EQUAL(a, b);
    BOOST_CHECK_NE(a, real);
    BOOST_CHECK_NE(a, dual);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Unary, Type, Types) {
    Dual<Type> a { 6.0, -2.0 };
    Dual<Type> b { -6.0, 2.0 };
    BOOST_CHECK_EQUAL(+a, a);
    BOOST_CHECK_EQUAL(-a, b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Addition, Type, Types) {
    Dual<Type> a { 6.0, -2.0 };
    Dual<Type> b { -1.0, 1.0 };
    Dual<Type> c { 5.0, -1.0 };
    BOOST_CHECK_EQUAL(a + b, c);
    BOOST_CHECK_EQUAL(a += b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Subtraction, Type, Types) {
    Dual<Type> a { 6.0, -2.0 };
    Dual<Type> b { -1.0, 1.0 };
    Dual<Type> c { 7.0, -3.0 };
    BOOST_CHECK_EQUAL(a - b, c);
    BOOST_CHECK_EQUAL(a -= b, c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Multiplication, Type, Types) {
    Dual<Type> x { 2.0, 0.0 };
    Dual<Type> y { 0.0, 2.0 };
    BOOST_CHECK_EQUAL(x*x, 2.0*x);
    BOOST_CHECK_EQUAL(x*y, 2.0*y);
    BOOST_CHECK_EQUAL(y*x, 2.0*y);
    BOOST_CHECK_EQUAL(y*y, 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Division, Type, Types) {
    Dual<Type> x { 2.0, 0.0 };
    Dual<Type> y { 2.0, 2.0 };
    Dual<Type> z { 2.0, -2.0 };
    BOOST_CHECK_EQUAL(x/x, 0.5*x);
    BOOST_CHECK_EQUAL(x/y, 0.5*z);
    BOOST_CHECK_EQUAL(y/x, 0.5*y);
    BOOST_CHECK_EQUAL(y/y, 0.5*x);
}

BOOST_AUTO_TEST_SUITE_END()
