#include "Math/Vector.h"
#include "Math/Quaternion.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Math {

BOOST_AUTO_TEST_SUITE(Quaternions)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 2.0, 3.0, 4.0 };
    Vector<Type, 3> vector { 2.0, 3.0, 4.0 };
    BOOST_CHECK_EQUAL(quaternion.scalar(), 1.0);
    BOOST_CHECK_EQUAL(quaternion.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(quaternion.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(quaternion.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(DefaultInitialization, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 2.0, 3.0, 4.0 };
    Quaternion<Type>* pointer = new (&quaternion) Quaternion<Type>;
    Vector<Type, 3> vector { 2.0, 3.0, 4.0 };
    BOOST_CHECK_EQUAL(quaternion.scalar(), 1.0);
    BOOST_CHECK_EQUAL(quaternion.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(quaternion.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(quaternion.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ValueInitialization, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 2.0, 3.0, 4.0 };
    Quaternion<Type>* pointer = new (&quaternion) Quaternion<Type> {};
    Vector<Type, 3> vector = Vector<Type, 3>::origin();
    BOOST_CHECK_EQUAL(quaternion.scalar(), Field<Type>::zero());
    BOOST_CHECK_EQUAL(quaternion.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(quaternion.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(quaternion.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Zero, Type, Types) {
    Quaternion<Type> quaternion = Quaternion<Type>::zero();
    Vector<Type, 3> vector = Vector<Type, 3>::origin();
    BOOST_CHECK_EQUAL(quaternion.scalar(), Field<Type>::zero());
    BOOST_CHECK_EQUAL(quaternion.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(quaternion.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(quaternion.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CopyConstructor, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 1.0, 1.0, 1.0 };
    Vector<Type, 3> vector { 1.0, 1.0, 1.0 };
    Quaternion<Type> copy { quaternion };
    BOOST_CHECK_EQUAL(copy.scalar(), 1.0);
    BOOST_CHECK_EQUAL(copy.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(copy.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(copy.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(MoveConstructor, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 1.0, 1.0, 1.0 };
    Vector<Type, 3> vector { 1.0, 1.0, 1.0 };
    Quaternion<Type> copy { std::move(quaternion) };
    BOOST_CHECK_EQUAL(copy.scalar(), 1.0);
    BOOST_CHECK_EQUAL(copy.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(copy.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(copy.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Assignment, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 1.0, 1.0, 1.0 };
    Vector<Type, 3> vector { 1.0, 1.0, 1.0 };
    Quaternion<Type> copy;
    copy = quaternion;
    BOOST_CHECK_EQUAL(copy.scalar(), 1.0);
    BOOST_CHECK_EQUAL(copy.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(copy.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(copy.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(MoveAssignment, Type, Types) {
    Quaternion<Type> quaternion { 1.0, 1.0, 1.0, 1.0 };
    Vector<Type, 3> vector { 1.0, 1.0, 1.0 };
    Quaternion<Type> copy;
    copy = std::move(quaternion);
    BOOST_CHECK_EQUAL(copy.scalar(), 1.0);
    BOOST_CHECK_EQUAL(copy.vector(0), vector[0]);
    BOOST_CHECK_EQUAL(copy.vector(1), vector[1]);
    BOOST_CHECK_EQUAL(copy.vector(2), vector[2]);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Comparison, Type, Types) {
    Quaternion<Type> a { 1.0, 1.0, 1.0, 1.0 };
    Quaternion<Type> b { 1.0, 1.0, 1.0, 1.0 };
    Quaternion<Type> abc { 1.0, 1.0, 1.0, 0.0 };
    Quaternion<Type> abd { 1.0, 1.0, 0.0, 1.0 };
    Quaternion<Type> acd { 1.0, 0.0, 1.0, 1.0 };
    Quaternion<Type> bcd { 0.0, 1.0, 1.0, 1.0 };
    BOOST_CHECK_EQUAL(a, a);
    BOOST_CHECK_EQUAL(a, b);
    BOOST_CHECK_NE(a, abc);
    BOOST_CHECK_NE(a, abd);
    BOOST_CHECK_NE(a, acd);
    BOOST_CHECK_NE(a, bcd);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Unary, Type, Types) {
    Quaternion<Type> a { 2.0, -3.0, 4.0, -1.0 };
    Quaternion<Type> b { -2.0, 3.0, -4.0, 1.0 };
    BOOST_CHECK_EQUAL(+a, a);
    BOOST_CHECK_EQUAL(-a, b);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Additive, Type, Types) {

}

BOOST_AUTO_TEST_CASE_TEMPLATE(Multiplication, Type, Types) {
    Quaternion<Type> a { 2.0, 0.0, 0.0, 0.0 };
    Quaternion<Type> b { 0.0, 2.0, 0.0, 0.0 };
    Quaternion<Type> c { 0.0, 0.0, 2.0, 0.0 };
    Quaternion<Type> d { 0.0, 0.0, 0.0, 2.0 };
    BOOST_CHECK_EQUAL(a*a, 2.0*a);
    BOOST_CHECK_EQUAL(a*b, 2.0*b);
    BOOST_CHECK_EQUAL(a*c, 2.0*c);
    BOOST_CHECK_EQUAL(a*d, 2.0*d);
    BOOST_CHECK_EQUAL(b*a, 2.0*b);
    BOOST_CHECK_EQUAL(b*b, -2.0*a);
    BOOST_CHECK_EQUAL(b*c, 2.0*d);
    BOOST_CHECK_EQUAL(b*d, -2.0*c);
    BOOST_CHECK_EQUAL(c*a, 2.0*c);
    BOOST_CHECK_EQUAL(c*b, -2.0*d);
    BOOST_CHECK_EQUAL(c*c, -2.0*a);
    BOOST_CHECK_EQUAL(c*d, 2.0*b);
    BOOST_CHECK_EQUAL(d*a, 2.0*d);
    BOOST_CHECK_EQUAL(d*b, 2.0*c);
    BOOST_CHECK_EQUAL(d*c, -2.0*b);
    BOOST_CHECK_EQUAL(d*d, -2.0*a);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Division, Type, Types) {
    Quaternion<Type> a { 2.0, 0.0, 0.0, 0.0 };
    Quaternion<Type> b { 0.0, 2.0, 0.0, 0.0 };
    Quaternion<Type> c { 0.0, 0.0, 2.0, 0.0 };
    Quaternion<Type> d { 0.0, 0.0, 0.0, 2.0 };
    BOOST_CHECK_EQUAL(a/a, 0.5*a);
    BOOST_CHECK_EQUAL(a/b, -0.5*b);
    BOOST_CHECK_EQUAL(a/c, -0.5*c);
    BOOST_CHECK_EQUAL(a/d, -0.5*d);
    BOOST_CHECK_EQUAL(b/a, 0.5*b);
    BOOST_CHECK_EQUAL(b/b, 0.5*a);
    BOOST_CHECK_EQUAL(b/c, -0.5*d);
    BOOST_CHECK_EQUAL(b/d, 0.5*c);
    BOOST_CHECK_EQUAL(c/a, 0.5*c);
    BOOST_CHECK_EQUAL(c/b, 0.5*d);
    BOOST_CHECK_EQUAL(c/c, 0.5*a);
    BOOST_CHECK_EQUAL(c/d, -0.5*b);
    BOOST_CHECK_EQUAL(d/a, 0.5*d);
    BOOST_CHECK_EQUAL(d/b, -0.5*c);
    BOOST_CHECK_EQUAL(d/c, 0.5*b);
    BOOST_CHECK_EQUAL(d/d, 0.5*a);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Math */
