#include "Math/Rotation.h"

#include "Math/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;
using Math::Vector;

namespace Math {

BOOST_AUTO_TEST_SUITE(Rotations)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(TwoDimensional, Type, Types) {
    Type relative = 0.001, absolute = 0.00001;
    Vector<Type, 2> x { 1.0, 0.0 };
    Vector<Type, 2> y { 0.0, 1.0 };
    Rotation<Type, 2> identity { };
    Rotation<Type, 2> ninety { 3.141592653589793/2.0 };
    BOOST_CHECK_EQUAL(identity(x), x);
    BOOST_CHECK_EQUAL(identity(y), y);
    // ninety(x) == y
    BOOST_CHECK_SMALL(ninety(x)[0], absolute);
    BOOST_CHECK_CLOSE(ninety(x)[1], 1.0, relative);
    // ninety(y) == -x
    BOOST_CHECK_CLOSE(ninety(y)[0], -1.0, relative);
    BOOST_CHECK_SMALL(ninety(y)[1], absolute);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ThreeDimensional, Type, Types) {
    Type relative = 0.001, absolute = 0.00001;
    Vector<Type, 3> x { 1.0, 0.0, 0.0 };
    Vector<Type, 3> y { 0.0, 1.0, 0.0 };
    Vector<Type, 3> z { 0.0, 0.0, 1.0 };
    Rotation<Type, 3> identity { };
    Rotation<Type, 3> rx = Rotation<Type, 3>::x(3.141592653589793/2.0);
    Rotation<Type, 3> ry = Rotation<Type, 3>::y(3.141592653589793/2.0);
    Rotation<Type, 3> rz = Rotation<Type, 3>::z(3.141592653589793/2.0);
    BOOST_CHECK_EQUAL(identity(x), x);
    BOOST_CHECK_EQUAL(identity(y), y);
    BOOST_CHECK_EQUAL(identity(z), z);
    // rx(x) == x
    BOOST_CHECK_CLOSE(rx(x)[0], 1.0, relative);
    BOOST_CHECK_SMALL(rx(x)[1], absolute);
    BOOST_CHECK_SMALL(rx(x)[2], absolute);
    // rx(y) == -z
    BOOST_CHECK_SMALL(rx(y)[0], absolute);
    BOOST_CHECK_SMALL(rx(y)[1], absolute);
    BOOST_CHECK_CLOSE(rx(y)[2], 1.0, relative);
    // rx(z) == -y
    BOOST_CHECK_SMALL(rx(z)[0], absolute);
    BOOST_CHECK_CLOSE(rx(z)[1], -1.0, relative);
    BOOST_CHECK_SMALL(rx(z)[2], absolute);
    // ry(x) == -z
    BOOST_CHECK_SMALL(ry(x)[0], absolute);
    BOOST_CHECK_SMALL(ry(x)[1], absolute);
    BOOST_CHECK_CLOSE(ry(x)[2], -1.0, relative);
    // ry(y) == y
    BOOST_CHECK_SMALL(ry(y)[0], absolute);
    BOOST_CHECK_CLOSE(ry(y)[1], 1.0, relative);
    BOOST_CHECK_SMALL(ry(y)[2], absolute);
    // ry(z) == x
    BOOST_CHECK_CLOSE(ry(z)[0], 1.0, relative);
    BOOST_CHECK_SMALL(ry(z)[1], absolute);
    BOOST_CHECK_SMALL(ry(z)[2], absolute);
    // rz(x) == y
    BOOST_CHECK_SMALL(rz(x)[0], absolute);
    BOOST_CHECK_CLOSE(rz(x)[1], 1.0, relative);
    BOOST_CHECK_SMALL(rz(x)[2], absolute);
    // rz(y) == -x
    BOOST_CHECK_CLOSE(rz(y)[0], -1.0, relative);
    BOOST_CHECK_SMALL(rz(y)[1], absolute);
    BOOST_CHECK_SMALL(rz(y)[2], absolute);
    // rz(z) == z
    BOOST_CHECK_SMALL(rz(z)[0], absolute);
    BOOST_CHECK_SMALL(rz(z)[1], absolute);
    BOOST_CHECK_CLOSE(rz(z)[2], 1.0, relative);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
