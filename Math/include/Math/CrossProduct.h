/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_CROSSPRODUCT_H
#define MATH_CROSSPRODUCT_H

namespace Math {

// forward declarations
template <class T, unsigned n>
class Vector;

/**
 * @brief A general empty base class template for classes that do not have a
 * cross product operation.
 *
 * This template is to be used in conjunction with a class template that should
 * provide certain instantiations with a cross product. More specifically it
 * can be used with the Vector<T, 3> template.
 *
 * @tparam T the derived type
 */
template <class T>
class CrossProduct {};

/**
 * @brief A base class for the Vector<T, 3> class template which provides cross
 * product operators through argument dependent lookup.
 *
 * @tparam T models the concept Field
 * @tparam Vector<T, 3> a Vector<T, 3> vector implementation
 */
template <class T>
class CrossProduct<Vector<T, 3>> {
public:
    /**
     * @brief Cross product assignment operator.
     *
     * Calculates in-place the vector obtained by the cross product of two
     * vector instances in three-dimensional space.
     */
    friend Vector<T, 3>& operator%=(Vector<T, 3>& a, Vector<T, 3> const& b) {
        T t[2] = { a[0], a[1] };
        a[0] = a[1]*b[2] - a[2]*b[1];
        a[1] = a[2]*b[0] - t[0]*b[2];
        a[2] = t[0]*b[1] - t[1]*b[0];
        return a;
    }
    /**
     * @brief Cross product operator.
     *
     * Calculates the vector obtained by the cross product of two vector
     * instances in three-dimensional space.
     */
    friend Vector<T, 3> operator%(Vector<T, 3> const& a, Vector<T, 3> const& b) {
        Vector<T, 3> result;
        result[0] = a[1]*b[2] - a[2]*b[1];
        result[1] = a[2]*b[0] - a[0]*b[2];
        result[2] = a[0]*b[1] - a[1]*b[0];
        return result;
    }
    /**
     * @brief Provides a verbose alternative for the operator%() cross product
     * notation.
     *
     * Calculates the vector obtained by the cross product of two vector
     * instances in three-dimensional space.
     */
    friend Vector<T, 3> cross(Vector<T, 3> const& a, Vector<T, 3> const& b) {
        return a%b;
    }
};

} /* namespace Math */
#endif /* MATH_CROSSPRODUCT_H */
