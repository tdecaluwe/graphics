/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_SPATIAL_ROTATION_H
#define MATH_SPATIAL_ROTATION_H

#include "Math/Vector.h"
#include "Math/Quaternion.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

// forward declarations
template <class T, unsigned n>
class Isometry;

template <class T, unsigned n>
class Rotation;

/**
 * @brief A rotation in a three-dimensional real vector space.
 *
 * Rotations in three-dimensional-space are represented by unit quaternions.
 * These can be thought of as elements of the double cover of the special
 * orthogonal group. To ensure normalization an incremental approach is used:
 * after each rotation composition one iteration of the Newton method is
 * executed to normalize the Quaternion.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Rotation<T, 3>
: private boost::equality_comparable<Rotation<T, 3>> {
public:
    /**
     * @brief The default constructor produces an identity Rotation.
     */
    constexpr Rotation();
    /**
     * @brief Construct a Rotation to rotate a certain angle around a certain
     * axis.
     */
    constexpr Rotation(Vector<T, 3> const& vector, T radians);
    /**
     * @brief Rotate a vector.
     *
     * This defines the group action on a three-dimensional real vector space.
     */
    Vector<T, 3> operator()(Vector<T, 3> const& vector);
    /**@{
     * @brief Compose with a second rotation.
     */
    Rotation& operator*=(Rotation const& rotation);
    Rotation& operator/=(Rotation const& rotation);
    /**@}*/
    /**
     * Construct an identity Rotation.
     */
    static constexpr Rotation identity();
    /**
     * Construct a Rotation around the x-axis.
     */
    static constexpr Rotation x(T radians);
    /**
     * Construct a Rotation around the y-axis.
     */
    static constexpr Rotation y(T radians);
    /**
     * Construct a Rotation around the z-axis.
     */
    static constexpr Rotation z(T radians);
private:
    Math::Quaternion<T> mQuaternion;
    constexpr explicit Rotation(Quaternion<T> const& quaternion);
    Rotation& normalize();
    friend class Isometry<T, 3>;
    /**@{
     * @brief Compose two Rotation objects.
     */
    friend Rotation operator*(Rotation const& a, Rotation const& b) {
        return Rotation{ a.mQuaternion*b.mQuaternion }.normalize();
    }
    friend Rotation operator/(Rotation const& a, Rotation const& b) {
        return Rotation{ a.mQuaternion*conjugate(b.mQuaternion) }.normalize();
    }
    /**@}*/
    /**
     * @brief Compare two Rotation objects for equality.
     */
    friend bool operator==(Rotation const& a, Rotation const& b) {
        return a.mQuaternion == b.mQuaternion;
    }
    /**
     * @brief Write a Rotation to an output stream.
     */
    friend std::ostream& operator<<(std::ostream& out, Rotation const& rotation) {
        return out << rotation.mQuaternion;
    }
};

template <class T>
using Versor = Rotation<T, 3>;

template <class T>
inline constexpr Rotation<T, 3>::Rotation()
: mQuaternion { 1.0 } {}

template <class T>
inline constexpr Rotation<T, 3>::Rotation(Math::Quaternion<T> const& quaternion)
: mQuaternion { quaternion } {}

template <class T>
inline Rotation<T, 3>& Rotation<T, 3>::normalize() {
    mQuaternion *= 1.5 - 0.5*Vector<T, 4>::operator*(mQuaternion, mQuaternion);
    return *this;
}

template <class T>
inline constexpr Rotation<T, 3>::Rotation(Vector<T, 3> const& vector, T radians)
: mQuaternion { std::cos(radians/2), std::sin(radians/2)*vector/length(vector) } {}

template <class T>
inline Vector<T, 3> Rotation<T, 3>::operator()(Vector<T, 3> const& vector) {
//    Vector<T, 3> cross =
//    { mQuaternion.vector(1)*vector[2] - mQuaternion.vector(2)*vector[1]
//    , mQuaternion.vector(2)*vector[0] - mQuaternion.vector(0)*vector[2]
//    , mQuaternion.vector(0)*vector[1] - mQuaternion.vector(1)*vector[0] };
//    cross += mQuaternion.scalar()*vector;
//    return
//    { vector[0] + 2*(mQuaternion.vector(1)*cross[2] - mQuaternion.vector(2)*cross[1])
//    , vector[1] + 2*(mQuaternion.vector(2)*cross[0] - mQuaternion.vector(0)*cross[2])
//    , vector[2] + 2*(mQuaternion.vector(0)*cross[1] - mQuaternion.vector(1)*cross[0]) };
     Vector<T, 3> vectorPart = { mQuaternion.vector(0), mQuaternion.vector(1), mQuaternion.vector(2) };
     return vector + (vectorPart + vectorPart)%(vectorPart%vector + mQuaternion[0]*vector);
}

template <class T>
inline Rotation<T, 3>& Rotation<T, 3>::operator*=(Rotation const& other) {
    mQuaternion *= other.mQuaternion;
    return normalize();
}

template <class T>
inline Rotation<T, 3>& Rotation<T, 3>::operator/=(Rotation const& other) {
    mQuaternion *= conjugate(other.mQuaternion);
    return normalize();
}

template <class T>
inline constexpr Rotation<T, 3> Rotation<T, 3>::identity() {
    return { };
}

template <class T>
inline constexpr Rotation<T, 3> Rotation<T, 3>::x(T radians) {
    return Rotation<T, 3>{ { std::cos(radians/2), std::sin(radians/2), 0.0, 0.0 } };
}

template <class T>
inline constexpr Rotation<T, 3> Rotation<T, 3>::y(T radians) {
    return Rotation<T, 3>{ { std::cos(radians/2), 0.0, std::sin(radians/2), 0.0 } };
}

template <class T>
inline constexpr Rotation<T, 3> Rotation<T, 3>::z(T radians) {
    return Rotation<T, 3>{ { std::cos(radians/2), 0.0, 0.0, std::sin(radians/2) } };
}

} /* namespace Math */
#endif /* MATH_SPATIAL_ROTATION_H */
