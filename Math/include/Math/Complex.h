/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_COMPLEX_H
#define MATH_COMPLEX_H

#include "Math/Conjugate.h"
#include "Math/Field.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

/**
 * @brief A complex number.
 *
 * A class representing a complex number \f$a+bi\f$. The value \f$a\f$ can be
 * accessed through real() and \f$b\f$ through imaginary(). The special element
 * \f$i\f$ satisfies the equation \f$i^2=-1\f$.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Complex
: private Conjugate<Complex<T>>
, private boost::additive<Complex<T>>
, private boost::field_operators<Complex<T>, T>
, private boost::equality_comparable<Complex<T>> {
public:
    /**
     * @brief Default constructs complex number.
     */
    constexpr Complex() = default;
    /**
     * @brief Constructs a real-valued complex number.
     *
     * @param real real value
     */
    constexpr Complex(T real);
    /**
     * @brief Constructs a complex number.
     *
     * @param a the real part \f$a\f$
     * @param b the imaginary part \f$b\f$
     */
    constexpr Complex(T a, T b);
    /**@{
     * @brief Accessor methods for the real part.
     */
    T& real();
    T const& real() const;
    /**@}*/
    /**@{
     * @brief Accessor methods for the imaginary part.
     */
    T& imaginary();
    T const& imaginary() const;
    /**@}*/
    /**@{
     * @brief Addition and subtraction
     *
     * @param other a real or complex number
     */
    Complex& operator+=(T other);
    Complex& operator-=(T other);
    Complex& operator+=(Complex const& other);
    Complex& operator-=(Complex const& other);
    /**@}*/
    /**@{
     * @brief Multiplication and division.
     *
     * @param other a real or complex number
     */
    Complex& operator*=(T other);
    Complex& operator/=(T other);
    Complex& operator*=(Complex const& other);
    Complex& operator/=(Complex const& other);
    /**@}*/
    /**
     * @brief Return the identity Complex for the addition.
     */
    static constexpr Complex zero();
    /**
	 * @brief Return the identity Complex for the multiplication.
	 */
    static constexpr Complex unity();
    /**
	 * @brief Return the Complex \f$i\f$.
	 */
    static constexpr Complex i();
private:
    T mReal;
    T mImaginary;
    /**
     * @brief Unary plus.
     *
     * @param complex a complex number
     */
    friend Complex operator+(Complex const& complex) {
        return { complex.mReal, complex.mImaginary };
    }
    /**
     * @brief Unary minus.
     *
     * @param complex a complex number
     */
    friend Complex operator-(Complex const& complex) {
        return { -complex.mReal, -complex.mImaginary };
    }
    /**
     * @brief Multiplication operator.
     *
     * @param a a complex number
     * @param b another complex number
     */
    friend Complex operator*(Complex const& a, Complex const& b) {
        return { a.mReal*b.mReal - a.mImaginary*b.mImaginary, a.mImaginary*b.mReal + a.mReal*b.mImaginary };
    }
    /**
     * @brief Division operator.
     *
     * @param a a complex number
     * @param b another complex number
     */
    friend Complex operator/(Complex const& a, Complex const& b) {
        T nominator = b.mReal*b.mReal + b.mImaginary*b.mImaginary;
        return { (a.mReal*b.mReal + a.mImaginary*b.mImaginary)/nominator, (a.mImaginary*b.mReal - a.mReal*b.mImaginary)/nominator };
    }
    /**
     * @brief Comparison operator.
     *
     * Returns whether a and b are equal.
     *
     * @param a a complex number
     * @param b another complex number
     */
    friend bool operator==(Complex const& a, Complex const& b) {
        return Float::Compare<T>::equal(a.mReal, b.mReal) && Float::Compare<T>::equal(a.mImaginary, b.mImaginary);
    }
};

/**
 * @brief Output a complex number in human readable format.
 *
 * @param out an output stream
 * @param complex a complex number
 */
template <class T>
std::ostream& operator<<(std::ostream& out, Complex<T> const& complex);

template <class T>
inline constexpr Complex<T>::Complex(T real)
: mReal { real }
, mImaginary { Field<T>::zero() } {}

template <class T>
inline constexpr Complex<T>::Complex(T a, T b)
: mReal { a }
, mImaginary { b } {}

template <class T>
inline T& Complex<T>::real() {
    return mReal;
}

template <class T>
inline T const& Complex<T>::real() const {
    return mReal;
}

template <class T>
inline T& Complex<T>::imaginary() {
    return mImaginary;
}

template <class T>
inline T const& Complex<T>::imaginary() const {
    return mImaginary;
}

template <class T>
inline Complex<T>& Complex<T>::operator+=(T other) {
    mReal += other;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator-=(T other) {
    mReal -= other;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator*=(T other) {
    mReal *= other;
    mImaginary *= other;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator/=(T other) {
    mReal /= other;
    mImaginary /= other;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator+=(Complex const& other) {
    mReal += other.mReal;
    mImaginary += other.mImaginary;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator-=(Complex const& other) {
    mReal -= other.mReal;
    mImaginary -= other.mImaginary;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator*=(Complex const& other) {
    T t = other.mImaginary * mImaginary;
    T u = other.mImaginary * mReal;
    mReal *= other.mReal;
    mImaginary *= other.mReal;
    mImaginary += u;
    mReal -= t;
    return *this;
}

template <class T>
inline Complex<T>& Complex<T>::operator/=(Complex const& other) {
    T t = other.mImaginary*mImaginary;
    T u = other.mImaginary*mReal;
    T nominator = other.mReal*other.mReal + other.mImaginary*other.mImaginary;
    mReal *= other.mReal;
    mImaginary *= other.mReal;
    mReal += t;
    mImaginary -= u;
    mReal /= nominator;
    mImaginary /= nominator;
    return *this;
}

template <class T>
inline constexpr Complex<T> Complex<T>::zero() {
	return { Field<T>::zero() };
}

template <class T>
inline constexpr Complex<T> Complex<T>::unity() {
	return { Field<T>::unity() };
}

template <class T>
inline constexpr Complex<T> Complex<T>::i() {
	return { Field<T>::zero(), Field<T>::unity() };
}

} /* namespace Math */
#endif /* MATH_COMPLEX_H */
