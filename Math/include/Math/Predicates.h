/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_PREDICATES_H
#define MATH_PREDICATES_H

namespace Math {

// forward declarations
template <class T, unsigned n>
class Vector;

/**
 * @brief A general empty base class template for class template instantiations
 * that do not have any associated predicates.
 *
 * This template is to be used in conjunction with a class template that should
 * provide certain instantiations with some specific geometric predicates.
 * These predicates can be implemented in a full or partial specialization of
 * this class template.
 *
 * @tparam T any type
 */
template <class T>
class Predicates {};

/**
 * @brief Some predicates on Vector objects.
 */
template <class T>
class Predicates<Vector<T, 2>> {
	/**
	 * @brief return true if Vector y lies on the counter clockwise side of
	 * Vector x.
	 *
	 * @note This predicate differs from the opposite of the clockwise()
	 * predicate when x and y are parallel since parallel Vector objects are
	 * considered to be neither counter clockwise nor clockwise.
	 */
    friend constexpr bool counterClockwise(Vector<T, 2> const& x, Vector<T, 2> const& y) {
        return x[0]*y[1] - x[1]*y[0] > 0;
    }
    /**
	 * @brief return true if Vector y lies on the clockwise side of Vector x.
	 *
	 * @note This predicate differs from the opposite of the counterClockwise()
	 * predicate when x and y are parallel since parallel Vector objects are
	 * considered to be neither clockwise nor counter clockwise.
	 */
    friend constexpr bool clockwise(Vector<T, 2> const& x, Vector<T, 2> const& y) {
        return x[0]*y[1] - x[1]*y[0] < 0;
    }
};

} /* namespace Math */
#endif /* MATH_PREDICATES_H */
