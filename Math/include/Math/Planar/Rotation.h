/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_PLANAR_ROTATION_H
#define MATH_PLANAR_ROTATION_H

#include "Math/Ring.h"
#include "Math/Vector.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

// forward declarations
template <class T, unsigned n>
class Isometry;

template <class T, unsigned n>
class Rotation;

/**
 * @brief A rotation in a two-dimensional real vector space.
 *
 * @note The two-dimensional rotation group contains, as all even-dimensional
 * rotation groups do, the central inversion transformation, accessible through
 * the inversion() method.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Rotation<T, 2>
: private boost::equality_comparable<Rotation<T, 2>> {
public:
    /**
     * @brief Construct an identity Rotation.
     */
    constexpr Rotation();
    /**
     * @brief Construct a Rotation through a given angle.
     */
    constexpr Rotation(T radians);
    /**
     * @brief Rotate a vector.
     *
     * This defines the group action on a two-dimensional real vector space.
     */
    Vector<T, 2> operator()(Vector<T, 2> const& vector);
    /**@{
     * @brief Compose with a second rotation.
     */
    Rotation& operator*=(Rotation const& rotation);
    Rotation& operator/=(Rotation const& rotation);
    /**@}*/
    /**
     * Construct an identity Rotation.
     */
    static constexpr Rotation identity();
    /**
     * Construct an inversion.
     */
    static constexpr Rotation inversion();
private:
    T mCosine;
    T mSine;
    constexpr Rotation(T cosine, T sine);
    Rotation& normalize();
    friend class Isometry<T, 2>;
    /**@{
     * @brief Compose two Rotation objects.
     */
    friend Rotation operator*(Rotation const& a, Rotation const& b) {
        return Rotation{ a.mCosine*b.mCosine - a.mSine*b.mSine, a.mSine*b.mCosine - a.mCosine*b.mSine };
    }
    friend Rotation operator/(Rotation const& a, Rotation const& b) {
        return Rotation{ a.mCosine*b.mCosine + a.mSine*b.mSine, a.mSine*b.mCosine + a.mCosine*b.mSine };
    }
    /**@}*/
    /**
     * @brief Compare two Rotation objects for equality.
     */
    friend bool operator==(Rotation const& a, Rotation const& b) {
        return Float::Compare<T>::equal(a.mCosine , b.mCosine) && Float::Compare<T>::equal(a.mSine, b.mSine);
    }
    /**
     * @brief Write a Rotation to an output stream.
     */
    friend std::ostream& operator<<(std::ostream& out, Rotation const& rotation) {
        return out << rotation.mQuaternion;
    }
};

template <class T>
inline constexpr Rotation<T, 2>::Rotation(T cosine, T sine)
: mCosine { cosine }
, mSine { sine } {}

template <class T>
inline Rotation<T, 2>& Rotation<T, 2>::normalize() {
    T adjustment = 1.5 - 0.5*(mCosine*mCosine + mSine*mSine);
    mCosine *= adjustment;
    mSine *= adjustment;
    return *this;
}

template <class T>
inline constexpr Rotation<T, 2>::Rotation()
: mCosine { 1.0 }
, mSine { } {}

template <class T>
inline constexpr Rotation<T, 2>::Rotation(T radians)
: mCosine { std::cos(radians) }
, mSine { std::sin(radians) } {}

template <class T>
inline Vector<T, 2> Rotation<T, 2>::operator()(Vector<T, 2> const& vector) {
    return { mCosine*vector[0] - mSine*vector[1], mSine*vector[0] + mCosine*vector[1] };
}

template <class T>
inline Rotation<T, 2>& Rotation<T, 2>::operator*=(Rotation const& other) {
    T temporary = mCosine*other.mSine;
    mCosine *= other.mCosine;
    mCosine -= mSine*other.mSine;
    mSine *= other.mCosine;
    mSine -= temporary;
    return normalize();
}

template <class T>
inline Rotation<T, 2>& Rotation<T, 2>::operator/=(Rotation const& other) {
    T temporary = mCosine*other.mSine;
    mCosine *= other.mCosine;
    mCosine += mSine*other.mSine;
    mSine *= other.mCosine;
    mSine += temporary;
    return normalize();
}

template <class T>
inline constexpr Rotation<T, 2> Rotation<T, 2>::identity() {
    return { };
}

template <class T>
inline constexpr Rotation<T, 2> Rotation<T, 2>::inversion() {
    return { -Math::Ring<T>::unity(), -Math::Ring<T>::unity() };
}

} /* namespace Math */
#endif /* MATH_PLANAR_ROTATION_H */
