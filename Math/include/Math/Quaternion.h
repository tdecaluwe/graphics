/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_QUATERNION_H
#define MATH_QUATERNION_H

#include "Math/Conjugate.h"
#include "Math/Vector.h"
#include "Math/Field.h"
#include "Math/Vector.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

/**
 * @brief A quaternion.
 *
 * A quaternion \f$a+bi+cj+dk\f$ has four components \f$a\f$, \f$b\f$, \f$c\f$
 * and \f$d\f$. The real \f$a\f$ can be regarded as the scalar part and
 * \f$bi+cj+dk\f$ as the vector part of the quaternion. The quaternions \f$i\f$,
 * \f$j\f$ and \f$k\f$ satisfy the equations \f$i^2=j^2=k^2=-1\f$ and
 * \f$ijk=-1\f$.
 *
 * This class inherits the four-dimensional Vector class, since the algebra of
 * quaternions is essentially a real four-dimensional Vector space with an
 * additional product (from which it derives it's field structure). Note that
 * operator* is not commutative for quaternions.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Quaternion
: public Vector<T, 4>
, private Conjugate<Quaternion<T>>
, private boost::additive<Quaternion<T>, T>
, private boost::additive<Quaternion<T>, Quaternion<T>>
, private boost::multiplicative<Quaternion<T>, T> {
public:
    /**
     * @brief Default constructor.
     */
    constexpr Quaternion() = default;
    /**
     * @brief Constructs a real-valued quaternion.
     *
     * @param real real value
     */
    constexpr Quaternion(T real);
    /**
     * @brief Constructs a quaternion from scalar and vector parts.
     *
     * @param scalar scalar part
     * @param vector vector part
     */
    constexpr Quaternion(T scalar, Vector<T, 3> const& vector);
    /**
     * @brief Constructs a quaternion.
     *
     * @param a the real value \f$a\f$
     * @param b the real value \f$b\f$
     * @param c the real value \f$c\f$
     * @param d the real value \f$d\f$
     */
    constexpr Quaternion(T a, T b, T c, T d);
    /**@{
     * @brief Accessor methods for the scalar part.
     */
    T& scalar();
    T const& scalar() const;
    /**@}*/
    /**@{
     * @brief Accessor methods for the vector part.
     *
     * @param index the index of the requested vector component (should be
     * between 0 and 2)
     */
    T& vector(unsigned index);
    T const& vector(unsigned index) const;
    /**@}*/
    /**@{
     * @brief Addition and subtraction.
     *
     * @param other quaternion
     */
    using Vector<T, 4>::operator+=;
    using Vector<T, 4>::operator-=;
    Quaternion& operator+=(T other);
    Quaternion& operator-=(T other);
    /**@}*/
    /**@{
     * @brief Multiplication and division.
     *
     * @param other quaternion
     */
    using Vector<T, 4>::operator*=;
    using Vector<T, 4>::operator/=;
    Quaternion& operator*=(Quaternion const& other);
    Quaternion& operator/=(Quaternion const& other);
    /**@}*/
    /**
     * @brief Return the identity Quaternion for the addition.
     */
    static Quaternion zero();
    /**
	 * @brief Return the identity Quaternion for the multiplication.
	 */
    static Quaternion unity();
    /**
	 * @brief Return the Quaternion \f$i\f$.
	 */
    static Quaternion i();
    /**
	 * @brief Return the Quaternion \f$j\f$.
	 */
    static Quaternion j();
    /**
	 * @brief Return the Quaternion \f$k\f$.
	 */
    static Quaternion k();
private:
    /**
     * @brief Unary plus operator.
     *
     * @param v a Quaternion
     */
    friend Quaternion operator+(Quaternion const& v) {
        Quaternion result;
        result[0u] = +v[0u];
        result[1u] = +v[1u];
        result[2u] = +v[2u];
        result[3u] = +v[3u];
        return result;
    }
    /**
     * @brief Unary minus operator.
     *
     * Computes the additive inverse.
     *
     * @param v a Quaternion
     */
    friend Quaternion operator-(Quaternion const& v) {
        Quaternion result;
        result[0u] = -v[0u];
        result[1u] = -v[1u];
        result[2u] = -v[2u];
        result[3u] = -v[3u];
        return result;
    }
    /**
     * @brief Multiplication operator.
     *
     * @param a a quaternion
     * @param b another quaternion
     */
    friend Quaternion operator*(Quaternion const& a, Quaternion const& b) {
        Quaternion result;
        result[0] = a[0]*b[0] - a[1]*b[1] - a[2]*b[2] - a[3]*b[3];
        result[1] = a[0]*b[1] + a[1]*b[0] + a[2]*b[3] - a[3]*b[2];
        result[2] = a[0]*b[2] - a[1]*b[3] + a[2]*b[0] + a[3]*b[1];
        result[3] = a[0]*b[3] + a[1]*b[2] - a[2]*b[1] + a[3]*b[0];
        return result;
    }
    /**
     * @brief division operator.
     *
     * @param a a quaternion
     * @param b another quaternion
     */
    friend Quaternion operator/(Quaternion const& a, Quaternion const& b) {
        Quaternion result;
        result[0] = a[0]*b[0] + a[1]*b[1] + a[2]*b[2] + a[3]*b[3];
        result[1] = a[1]*b[0] - a[0]*b[1] + a[3]*b[2] - a[2]*b[3];
        result[2] = a[2]*b[0] - a[3]*b[1] - a[0]*b[2] + a[1]*b[3];
        result[3] = a[3]*b[0] + a[2]*b[1] - a[1]*b[2] - a[0]*b[3];
        result /= b[0]*b[0] + b[1]*b[1] + b[2]*b[2] + b[3]*b[3];
        return result;
    }
};

/**
 * @brief Output a quaternion in human readable format.
 *
 * @param out an output stream
 * @param quaternion a quaternion
 */
template <class T>
std::ostream& operator<<(std::ostream& out, Quaternion<T> const& quaternion);

template <class T>
inline constexpr Quaternion<T>::Quaternion(T real)
: Vector<T, 4>{ real, Field<T>::zero(), Field<T>::zero(), Field<T>::zero() } {}

template <class T>
inline constexpr Quaternion<T>::Quaternion(T scalar, Vector<T, 3> const& vector)
: Vector<T, 4>{ scalar, vector[0], vector[1], vector[2] } {}

template <class T>
inline constexpr Quaternion<T>::Quaternion(T a, T b, T c, T d)
: Vector<T, 4>{ a, b, c, d } {}

template <class T>
inline T& Quaternion<T>::scalar() {
    return this->operator[](0);
}

template <class T>
inline T const& Quaternion<T>::scalar() const {
    return this->operator[](0);
}

template <class T>
T& Quaternion<T>::vector(unsigned index) {
    return this->operator[](++index);
}

template <class T>
T const& Quaternion<T>::vector(unsigned index) const {
    return this->operator[](++index);
}

template <class T>
inline Quaternion<T>& Quaternion<T>::operator+=(T other) {
    this->operator[](0) += other;
    return *this;
}

template <class T>
inline Quaternion<T>& Quaternion<T>::operator-=(T other) {
    this->operator[](0) -= other;
    return *this;
}

template <class T>
inline Quaternion<T>& Quaternion<T>::operator*=(Quaternion const& q) {
    Quaternion temp = *this;
    this->operator[](0) = temp[0]*q[0] - temp[1]*q[1] - temp[2]*q[2] - temp[3]*q[3];
    this->operator[](1) = temp[0]*q[1] + temp[1]*q[0] + temp[2]*q[3] - temp[3]*q[2];
    this->operator[](2) = temp[0]*q[2] - temp[1]*q[3] + temp[2]*q[0] + temp[3]*q[1];
    this->operator[](3) = temp[0]*q[3] + temp[1]*q[2] - temp[2]*q[1] + temp[3]*q[0];
    return *this;
}

template <class T>
inline Quaternion<T>& Quaternion<T>::operator/=(Quaternion const& q) {
    Quaternion temp = *this;
    this->operator[](0) = temp[0]*q[0] + temp[1]*q[1] + temp[2]*q[2] + temp[3]*q[3];
    this->operator[](1) = temp[1]*q[0] - temp[0]*q[1] + temp[3]*q[2] - temp[2]*q[3];
    this->operator[](2) = temp[2]*q[0] - temp[3]*q[1] - temp[0]*q[2] + temp[1]*q[3];
    this->operator[](3) = temp[3]*q[0] + temp[2]*q[1] - temp[1]*q[2] - temp[0]*q[3];
    this->operator/=(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
    return *this;
}

template <class T>
inline Quaternion<T> Quaternion<T>::zero() {
	return { Field<T>::zero() };
}

template <class T>
inline Quaternion<T> Quaternion<T>::unity() {
	return { Field<T>::unity() };
}

template <class T>
inline Quaternion<T> Quaternion<T>::i() {
	return { Field<T>::zero(), Field<T>::unity(), Field<T>::zero(), Field<T>::zero() };
}

template <class T>
inline Quaternion<T> Quaternion<T>::j() {
	return { Field<T>::zero(), Field<T>::zero(), Field<T>::unity(), Field<T>::zero() };
}

template <class T>
inline Quaternion<T> Quaternion<T>::k() {
	return { Field<T>::zero(), Field<T>::zero(), Field<T>::zero(), Field<T>::unity() };
}

} /* namespace Math */
#endif /* MATH_QUATERNION_H */
