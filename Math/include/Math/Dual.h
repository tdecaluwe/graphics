/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_DUAL_H
#define MATH_DUAL_H

#include "Math/Conjugate.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

// forward declarations
template <class T>
class Complex;
template <class T>
class Quaternion;

/**
 * @brief A dual number.
 *
 * A class representing a dual number \f$a+b\epsilon\f$. The value \f$a\f$ can
 * be accessed through real() and \f$b\f$ through dual(). The special element
 * \f$\epsilon\f$ satisfies the equation \f$\epsilon^2=0\f$.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Dual
: private Conjugate<Dual<T>>
, private boost::additive<Dual<T>>
, private boost::field_operators<Dual<T>, T>
, private boost::equality_comparable<Dual<T>> {
public:
    /**
     * @brief Default constructs dual number.
     */
    constexpr Dual() = default;
    /**
     * @brief Constructs a real-valued dual number.
     *
     * @param real real value
     */
    constexpr Dual(T real);
    /**
     * @brief Constructs a dual number.
     *
     * @param a the real part \f$a\f$
     * @param b the dual part \f$b\f$
     */
    constexpr Dual(T a, T b);
    /**@{
     * @brief Accessor methods for the real part.
     */
    T& real();
    T const& real() const;
    /**@}*/
    /**@{
     * @brief Accessor methods for the dual part.
     */
    T& dual();
    T const& dual() const;
    /**@}*/
    /**@{
     * @brief Addition and subtraction.
     */
    Dual& operator+=(T other);
    Dual& operator-=(T other);
    Dual& operator+=(Dual const& other);
    Dual& operator-=(Dual const& other);
    /**@}*/
    /**@{
     * @brief Multiplication and division.
     */
    Dual& operator*=(T other);
    Dual& operator/=(T other);
    Dual& operator*=(Dual const& other);
    Dual& operator/=(Dual const& other);
    /**@}*/
    /**
     * @brief Return the identity Dual number for the addition.
     */
    static constexpr Dual zero();
    /**
	 * @brief Return the identity Dual number for the multiplication.
	 */
    static constexpr Dual unity();
private:
    T mReal;
    T mDual;
    /**
     * @brief Unary plus.
     *
     * @param dual a dual number
     */
    friend Dual operator+(Dual const& dual) {
        return { dual.mReal, dual.mDual };
    }
    /**
     * @brief Unary minus.
     *
     * @param dual a dual number
     */
    friend Dual operator-(Dual const& dual) {
        return { -dual.mReal, -dual.mDual };
    }
    /**
     * @brief Comparison operator.
     *
     * Returns whether a and b are equal.
     *
     * @param a a dual number
     * @param b another dual number
     */
    friend bool operator==(Dual const& a, Dual const& b) {
        return a.mReal == b.mReal && a.mDual == b.mDual;
    }
    /**
     * @brief Multiplication operator.
     *
     * @param a a dual number
     * @param b another dual number
     */
    friend Dual operator*(Dual const& a, Dual const& b) {
        Dual result;
        result.mReal = a.mReal*b.mReal;
        result.mDual = a.mDual*b.mReal + a.mReal*b.mDual;
        return result;
    }
    /**
     * @brief division operator.
     *
     * @param a a dual number
     * @param b another dual number
     */
    friend Dual operator/(Dual const& a, Dual const& b) {
        Dual result;
        result.mReal = a.mReal/b.mReal;
        result.mDual = (a.mDual -  result.mReal*b.mDual)/b.mReal;
        return result;
    }
};

/**
 * @brief Output a dual number in human readable format.
 *
 * @param out an output stream
 * @param dual a dual number
 */
template <class T>
std::ostream& operator<<(std::ostream& out, Dual<T> const& dual);

/**
 * @brief Output a dual complex number in human readable format.
 *
 * @param out an output stream
 * @param dual a dual complex number
 */
template <class T>
std::ostream& operator<<(std::ostream& out, Dual<Complex<T>> const& dual);

/**
 * @brief Output a dual quaternion in human readable format.
 *
 * @param out an output stream
 * @param dual a dual quaternion
 */
template <class T>
std::ostream& operator<<(std::ostream& out, Dual<Quaternion<T>> const& dual);

template <class T>
inline constexpr Dual<T>::Dual(T real)
: mReal { real }
, mDual { } {}

template <class T>
inline constexpr Dual<T>::Dual(T a, T b)
: mReal { a }
, mDual { b } {}

template <class T>
inline T& Dual<T>::real() {
    return mReal;
}

template <class T>
inline T const& Dual<T>::real() const {
    return mReal;
}

template <class T>
inline T& Dual<T>::dual() {
    return mDual;
}

template <class T>
inline T const& Dual<T>::dual() const {
    return mDual;
}

template <class T>
inline Dual<T>& Dual<T>::operator+=(T other) {
    mReal += other;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator-=(T other) {
    mReal -= other;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator*=(T other) {
    mReal *= other;
    mDual *= other;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator/=(T other) {
    mReal /= other;
    mDual /= other;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator+=(Dual const& other) {
    mReal += other.mReal;
    mDual += other.mDual;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator-=(Dual const& other) {
    mReal -= other.mReal;
    mDual -= other.mDual;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator*=(Dual const& other) {
    mDual *= other.mReal;
    mDual += mReal*other.mDual;
    mReal *= other.mReal;
    return *this;
}

template <class T>
inline Dual<T>& Dual<T>::operator/=(Dual const& other) {
    mReal /= other.mReal;
    mDual /= other.mReal;
    mDual -= mReal*other.mDual/other.mReal;
    return *this;
}

} /* namespace Math */
#endif /* MATH_DUAL_H */
