/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_FIELD_H
#define MATH_FIELD_H

#include "Math/Ring.h"

#include "Float/Float.h"

#include <cmath>

namespace Math {

template <class T>
class Field: public Ring<T> {
public:
    static constexpr T inverse(T in);
	static T sqrt(T in);
    static T rsqrt(T in);
};

template <class T>
inline constexpr T Field<T>::inverse(T in) {
    return Ring<T>::unity()/in;
}

template <class T>
inline T Field<T>::sqrt(T in) {
    return std::sqrt(in);
}

template <class T>
inline T Field<T>::rsqrt(T in) {
    return Ring<T>::unity()/std::sqrt(in);
}

inline float fastInverseSquareRoot(float in) {
    Float::Float<float> rsqrt { in };
    rsqrt.integer() =  0x5f375a86 - ( rsqrt.integer() >> 1 );
    return rsqrt.value() * (1.5f - 0.5f*in*rsqrt.value()*rsqrt.value());
}

inline double fastInverseSquareRoot(double in) {
    Float::Float<double> rsqrt { in };
    rsqrt.integer() =  0x5fe6eb50c7b537a9 - ( rsqrt.integer() >> 1 );
    return rsqrt.value() * (1.5 - 0.5*in*rsqrt.value()*rsqrt.value());
}

} /* namespace Math */
#endif /* MATH_FIELD_H */
