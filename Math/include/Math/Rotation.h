/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_ROTATION_H
#define MATH_ROTATION_H

#include "Math/Planar/Rotation.h"
#include "Math/Spatial/Rotation.h"
#include "Math/Ring.h"
#include "Math/Vector.h"
#include "Math/Quaternion.h"

#include "Float/Compare.h"

#include "boost/operators.hpp"

#include <iosfwd>

namespace Math {

// forward declarations
template <class T, unsigned n>
class Isometry;

template <class T, unsigned n>
class Rotation;

/**
 * @brief A rotation in a four-dimensional real vector space.
 *
 * A four-dimensional rotation can be decomposed in a left- and a
 * right-isoclinic rotation which can respectively be represented by left- and
 * a right-multiplication by a Quaternion.
 *
 * @note The four-dimensional rotation group contains, as all even-dimensional
 * rotation groups do, the central inversion transformation, accessible through
 * the inversion() method.
 *
 * @tparam T a type modeling a real number
 */
template <class T>
class Rotation<T, 4>
: private boost::equality_comparable<Rotation<T, 4>> {
private:
    Math::Quaternion<T> mLeft;
    Math::Quaternion<T> mRight;
    constexpr explicit Rotation(Quaternion<T> const& left, Quaternion<T> const& right);
    Rotation& normalize();
public:
    constexpr Rotation();
    constexpr Rotation(T radians, Vector<T, 4> const& vector);
    Vector<T, 4> operator()(Vector<T, 4> const& vector);
    Rotation& operator*=(Rotation const& rotation);
    Rotation& operator/=(Rotation const& rotation);
    static constexpr Rotation identity();
    static constexpr Rotation inversion();
    friend Rotation operator*(Rotation const& a, Rotation const& b) {
        return Rotation{ a.mLeft*b.mLeft, b.mRight*a.mRight }.normalize();
    }
    friend Rotation operator/(Rotation const& a, Rotation const& b);
    friend bool operator==(Rotation const& a, Rotation const& b) {
        return a.mLeft == b.mLeft && a.mRight == b.mRight;
    }
    friend std::ostream& operator<<(std::ostream& out, Rotation const& rotation) {
        return out << "(left: " << rotation.mLeft << ", right: " << rotation.mRight << ")";
    }
};

} /* namespace Math */
#endif /* MATH_ROTATION_H */
