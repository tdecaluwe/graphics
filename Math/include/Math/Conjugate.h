/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MATH_CONJUGATE_H
#define MATH_CONJUGATE_H

namespace Math {

// forward declarations
template <class T>
class Complex;
template <class T>
class Quaternion;
template <class T>
class Dual;

/**
 * @brief A general empty base class template for classes that do not have a
 * conjugation operation.
 *
 * This template is to be used in conjunction with classes that should provide
 * a conjugation operation.
 *
 * @tparam T the derived type
 */
template <class T>
class Conjugate {};

/**
 * @brief Conjugation functionality for the Complex class template.
 */
template <class T>
class Conjugate<Complex<T>> {
public:
    /**
     * @brief Retrieve the conjugate complex number \f$a-bi\f$.
     *
     * @param complex a Complex number
     */
    friend Complex<T> conjugate(Complex<T> const& complex) {
        return { complex.real(), -complex.imaginary() };
    }
};

/**
 * @brief Conjugation functionality for the Quaternion class template.
 */
template <class T>
class Conjugate<Quaternion<T>> {
public:
    /**
     * @brief Retrieve the conjugate quaternion \f$a-bi-cj-dk\f$.
     *
     * @param quaternion a Quaternion
     */
    friend Quaternion<T> conjugate(Quaternion<T> const& quaternion) {
        return { quaternion.scalar(), -quaternion.vector(0), -quaternion.vector(1), -quaternion.vector(2) };
    }
};

template <class T>
class Conjugate<Dual<T>> {
public:
    /**
     * @brief Retrieve the conjugate dual number \f$a^*+b^*\epsilon\f$.
     *
     * @param dual a dual number
     */
    friend Dual<T> conjugate(Dual<T> const& dual) {
        return { dual.real(), dual.dual() };
    }
};

template <class T>
class Conjugate<Dual<Complex<T>>> {
public:
    /**
     * @brief Retrieve the conjugate dual number \f$a^*+b^*\epsilon\f$.
     *
     * @param dual a dual number
     */
    friend Dual<Complex<T>> conjugate(Dual<Complex<T>> const& dual) {
        return { conjugate(dual.real()), conjugate(dual.dual()) };
    }
};

template <class T>
class Conjugate<Dual<Quaternion<T>>> {
    /**
     * @brief Retrieve the conjugate dual number \f$a^*+b^*\epsilon\f$.
     *
     * @param dual a dual number
     */
    friend Dual<Quaternion<T>> conjugate(Dual<Quaternion<T>> const& dual) {
        return { conjugate(dual.real()), conjugate(dual.dual()) };
    }
};

} /* namespace Math */
#endif /* MATH_CONJUGATE_H */
