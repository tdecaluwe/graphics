/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include "Math/CrossProduct.h"
#include "Math/Predicates.h"
#include "Math/Field.h"

#include "Float/Compare.h"

#include "Memory/Array.h"

#include <boost/operators.hpp>

#include <iosfwd>

namespace Math {

/**
 * @brief A vector in Euclidean space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality of the Euclidean space
 */
template <class T, unsigned n>
class Vector
: public Memory::Array<T, n>
, private Predicates<Vector<T, n>>
, private CrossProduct<Vector<T, n>>
, private boost::additive<Vector<T, n>>
, private boost::multiplicative<Vector<T, n>, T>
, private boost::equality_comparable<Vector<T, n>> {
public:
    using Memory::Array<T, n>::Array;
    /**
     * @brief Addition assignment operator.
     *
     * @param v addend
     */
    Vector& operator+=(Vector const& v);
    /**
     * @brief Subtraction assignment operator.
     *
     * @param v minuend
     */
    Vector& operator-=(Vector const& v);
    /**
     * @brief Multiplication assignment operator.
     *
     * An implementation of scalar multiplication. Additional multiplication
     * operators are provided by boost.
     *
     * @param s multiplier
     */
    Vector& operator*=(T s);
    /**
     * @brief Division assignment operator.
     *
     * An implementation of scalar division. Additional division operators are
     * provided by boost.
     *
     * @param s divisor
     */
    Vector& operator/=(T s);
    /**
     * @brief Return a zero-valued Vector.
     */
    static constexpr Vector origin();
    /**
     * @brief Unary plus operator.
     *
     * @param v a Vector
     */
    friend Vector operator+(Vector const& v) {
        Vector<T, n> result;
        for (unsigned i = 0; i < n; ++i) {
            result[i] = +v[i];
        }
        return result;
    }
    /**
     * @brief Unary minus operator.
     *
     * Computes the additive inverse.
     *
     * @param v a Vector
     */
    friend Vector operator-(Vector const& v) {
        Vector<T, n> result;
        for (unsigned i = 0; i < n; ++i) {
            result[i] = -v[i];
        }
        return result;
    }
    /**
     * @brief Muliplication operator.
     *
     * Computes the dot product of two Vector instances.
     *
     * @param a a Vector
     * @param b another Vector
     */
    friend T operator*(Vector const& a, Vector const& b) {
        T total = 0;
        for (unsigned i = 0; i < n; ++i) {
            total += a[i]*b[i];
        }
        return total;
    }
    /**
     * @brief Equality comparison operator.
     *
     * Additional (inverse) equivalence operators are provided by boost.
     *
     * @param a a Vector
     * @param b another Vector
     */
    friend bool operator==(Vector const& a, Vector const& b) {
        bool result = true;
        for (unsigned i = 0; i < n; ++i) {
            result &= Float::Compare<T>::equal(a[i], b[i]);
        }
        return result;
    }
    /**
     * @brief Calculate the length of a Vector.
     *
     * @param v a Vector
     */
    friend T length(Vector const& v) {
        return Field<T>::sqrt(v*v);
    }
    /**
     * @brief Normalize a Vector.
     *
     * @param v a Vector
     */
    friend Vector normalize(Vector const& v) {
        return v*Field<T>::rsqrt(v*v);
    }
};

template <class T, unsigned n>
inline Vector<T, n>& Vector<T, n>::operator+=(Vector const& v) {
    for (unsigned i = 0; i < n; ++i) {
    	this->operator[](i) += v[i];
    }
    return *this;
}

template <class T, unsigned n>
inline Vector<T, n>& Vector<T, n>::operator-=(Vector const& v) {
    for (unsigned i = 0; i < n; ++i) {
    	this->operator[](i) -= v[i];
    }
    return *this;
}

template <class T, unsigned n>
inline Vector<T, n>& Vector<T, n>::operator*=(T s) {
    for (unsigned i = 0; i < n; ++i) {
    	this->operator[](i) *= s;
    }
    return *this;
}

template <class T, unsigned n>
inline Vector<T, n>& Vector<T, n>::operator/=(T s) {
    for (unsigned i = 0; i < n; ++i) {
        this->operator[](i) /= s;
    }
    return *this;
}

template <class T, unsigned n>
inline constexpr Vector<T, n> Vector<T, n>::origin() {
    return Vector{ };
}

} /* namespace Math */
#endif /* VECTOR_H_ */
