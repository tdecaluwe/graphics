/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#include "Math/Complex.h"

#include <iostream>

namespace Math {

template <class T>
std::ostream& operator<<(std::ostream& out, Complex<T> const& complex) {
   out << complex.real();
   out << (Float::Compare<T>::sign(complex.imaginary()) ? " - " : " + ");
   out << (Float::Compare<T>::sign(complex.imaginary()) ? -complex.imaginary() : complex.imaginary()) << "i";
   return out;
}

template std::ostream& operator<<(std::ostream& out, Complex<float> const& complex);
template std::ostream& operator<<(std::ostream& out, Complex<double> const& complex);

} /* namespace Math */
