/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#include "Math/Quaternion.h"

#include <iostream>

namespace Math {

template <class T>
std::ostream& operator<<(std::ostream& out, Quaternion<T> const& q) {
    out << q.scalar();
    out << (Float::Compare<T>::sign(q.vector(0)) ? " - " : " + ");
    out << (Float::Compare<T>::sign(q.vector(0)) ? -q.vector(0) : q.vector(0)) << "i";
    out << (Float::Compare<T>::sign(q.vector(1)) ? " - " : " + ");
    out << (Float::Compare<T>::sign(q.vector(1)) ? -q.vector(1) : q.vector(1)) << "j";
    out << (Float::Compare<T>::sign(q.vector(2)) ? " - " : " + ");
    out << (Float::Compare<T>::sign(q.vector(2)) ? -q.vector(2) : q.vector(2)) << "k";
    return out;
}

template std::ostream& operator<<(std::ostream& out, Quaternion<float> const& q);
template std::ostream& operator<<(std::ostream& out, Quaternion<double> const& q);

} /* namespace Math */
