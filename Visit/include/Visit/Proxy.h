/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PROXY_H_
#define PROXY_H_

#include <memory>
#include <utility>
#include <type_traits>

#include "Visit/StateVisitor.h"
#include "Visit/Visitor.h"
#include "Visit/Adapter.h"
#include "Visit/Ignore.h"

namespace Visit {

// forward declarations
template <class Type, class ... Types>
class Visitor;
template <class Abstract, class Generic, class ... Types>
class Adapter;

/**
 * @brief Dynamic proxy for any object implementing the provided interface.
 *
 * Implements a dynamic dispatch proxy for a visitor object, while also
 * providing the same visitor interface. Can be used for self-referencing (tree
 * traversal) visitors.
 */
template <class Abstract, class ... Types>
class Proxy;

/**
 * @brief Recursive general Proxy.
 *
 * This general template recursively implements all the required members (a
 * virtual visit method for each class listed as a template parameter after the
 * Abstract base class).
 *
 * @todo Change assignment operators for Proxy objects to general assignment
 * operators for Abstract objects.
 */
template <class Abstract, class Type, class ... Types>
class Proxy<Abstract, Type, Types...>
: public Proxy<Abstract, Types...>::Implementer {
public:
	class Implementer
	: public Proxy<Abstract, Types...>::Implementer {
	protected:
		template <class Generic>
		Implementer(Generic&&);
		Implementer(Implementer const&) = default;
		Implementer(Implementer&&) = default;
		Implementer& operator=(Implementer const&) = default;
		Implementer& operator=(Implementer&&) = default;
	public:
		virtual ~Implementer() = default;
		using Proxy<Abstract, Types...>::Implementer::visit;
		virtual void visit(Type&);
	};
	Proxy();
	template <class Generic>
	Proxy(Generic&&);
	Proxy(Proxy const&) = default;
	Proxy(Proxy&&) = default;
	virtual ~Proxy() = default;
	Proxy& operator=(Proxy const&) = default;
	Proxy& operator=(Proxy&&) = default;
	template <class Generic>
	Proxy& operator=(Generic&&);
	using Proxy<Abstract, Types...>::Implementer::visit;
	virtual void visit(Type&);
};

template <class Abstract, class  Type>
class Proxy<Abstract, Type>: public Abstract {
private:
	std::shared_ptr<Abstract> pVisitor;
public:
	class Implementer: public Abstract {
	protected:
		std::shared_ptr<Abstract> pVisitor;
		template <class Generic>
		Implementer(Generic&&);
		Implementer(Implementer const&) = default;
		Implementer(Implementer&&) = default;
		Implementer& operator=(Implementer const&) = default;
		Implementer& operator=(Implementer&&) = default;
	public:
		virtual ~Implementer() = default;
		virtual void visit(Type&);
	};
	Proxy();
	template <class Generic>
	Proxy(Generic&&);
	Proxy(Proxy const&) = default;
	Proxy(Proxy&&) = default;
	virtual ~Proxy() = default;
	Proxy& operator=(Proxy const&) = default;
	Proxy& operator=(Proxy&&) = default;
	template <class Generic>
	Proxy& operator=(Generic&&);
	virtual void visit(Type&);
};

/**
 * @brief Recursive specialized standard Proxy.
 *
 * Standard means that the abstract visitor base class provided by this
 * framework is used. Specialized means that the Proxy template is
 * specialized in such a way that the list with classes by which the abstract
 * base class Visitor is parameterized should not be separately provided for
 * this class template (if it is provided however, the general template will
 * be used).
 *
 * @tparam Visitor The standard abstract visitor base class template
 */
template <class Type, class ... Types>
class Proxy<Visitor<Type, Types...>>: public Proxy<Visitor<Type, Types...>, Type, Types...> {
public:
	Proxy() = default;
	Proxy(Proxy const&) = default;
	Proxy(Proxy&&) = default;
	Proxy(Proxy<Visitor<Type, Types...>, Type, Types...> const&);
	Proxy(Proxy<Visitor<Type, Types...>, Type, Types...>&&);
	virtual ~Proxy() = default;
	Proxy& operator=(Proxy const&) = default;
	Proxy& operator=(Proxy&&) = default;
	Proxy& operator=(Proxy<Visitor<Type, Types...>, Type, Types...> const&);
	Proxy& operator=(Proxy<Visitor<Type, Types...>, Type, Types...>&&);
	template <class Generic>
	Proxy& operator=(Generic&&);
};


template <class ... Types, class Generic>
Proxy<Visitor<Types...>> proxy(Generic&& g);

template <class Abstract, class Type, class ... Types>
inline Proxy<Abstract, Type, Types...>::Proxy()
: Proxy<Abstract, Types...>::Implementer(Ignore()) {}

template <class Abstract, class Type, class ... Types>
template <class Generic>
Proxy<Abstract, Type, Types...>::Proxy(Generic&& generic)
: Proxy<Abstract, Types...>::Implementer(std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type, Types...>>(std::forward<Generic>(generic))) {}

template <class Abstract, class Type, class ... Types>
template <class Generic>
inline Proxy<Abstract, Type, Types...>& Proxy<Abstract, Type, Types...>::operator=(Generic&& generic) {
	Proxy<Abstract, Types...>::Implementer::pVisitor = std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type, Types...>>(std::forward<Generic>(generic));
	return *this;
}

template <class Abstract, class Type, class ... Types>
inline void Proxy<Abstract, Type, Types...>::visit(Type& t) {
	Proxy<Abstract, Types...>::Implementer::pVisitor->visit(t);
}

template <class Abstract, class Type, class ... Types>
template <class Generic>
Proxy<Abstract, Type, Types...>::Implementer::Implementer(Generic&& generic)
: Proxy<Abstract, Types...>::Implementer(std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type, Types...>>(std::forward<Generic>(generic))) {}

template <class Abstract, class Type, class ... Types>
inline void Proxy<Abstract, Type, Types...>::Implementer::visit(Type& t) {
	Proxy<Abstract, Types...>::Implementer::pVisitor->visit(t);
}

template <class Abstract, class Type>
inline Proxy<Abstract, Type>::Proxy()
: pVisitor(std::make_shared<Adapter<Abstract, Ignore, Type>>()) {}

template <class Abstract, class Type>
template <class Generic>
Proxy<Abstract, Type>::Proxy(Generic&& generic)
: pVisitor(std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type>>(std::forward<Generic>(generic))) {}

template <class Abstract, class Type>
template <class Generic>
inline Proxy<Abstract, Type>& Proxy<Abstract, Type>::operator=(Generic&& generic) {
	pVisitor = std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type>>(std::forward<Generic>(generic));
	return *this;
}

template <class Abstract, class Type>
inline void Proxy<Abstract, Type>::visit(Type& t) {
	pVisitor->visit(t);
}

template <class Abstract, class Type>
template <class Generic>
Proxy<Abstract, Type>::Implementer::Implementer(Generic&& generic)
: pVisitor(std::make_shared<Adapter<Abstract, typename std::remove_reference<Generic>::type, Type>>(std::forward<Generic>(generic))) {}

template <class Abstract, class Type>
inline void Proxy<Abstract, Type>::Implementer::visit(Type& t) {
	pVisitor->visit(t);
}

template <class Type, class ... Types>
template <class Generic>
inline Proxy<Visitor<Type, Types...>>& Proxy<Visitor<Type, Types...>>::operator=(Generic&& generic) {
	Proxy<Visitor<Type, Types...>, Type, Types...>::pVisitor = std::make_shared<Adapter<Visitor<Type, Types...>, typename std::remove_reference<Generic>::type, Type, Types...>>(std::forward<Generic>(generic));
	return *this;
}

template <class Type, class ... Types>
inline Proxy<Visitor<Type, Types...>>::Proxy(Proxy<Visitor<Type, Types...>, Type, Types...> const& p) {
	Proxy<Visitor<Type, Types...>, Type, Types...>::pVisitor = p.pVisitor;
}

template <class Type, class ... Types>
inline Proxy<Visitor<Type, Types...>>::Proxy(Proxy<Visitor<Type, Types...>, Type, Types...>&& p) {
	Proxy<Visitor<Type, Types...>, Type, Types...>::pVisitor = std::move(p.pVisitor);
}

template <class Type, class ... Types>
inline Proxy<Visitor<Type, Types...>>& Proxy<Visitor<Type, Types...>>::operator=(Proxy<Visitor<Type, Types...>, Type, Types...> const& p) {
	Proxy<Visitor<Type, Types...>, Type, Types...>::pVisitor = p.pVisitor;
	return *this;
}

template <class Type, class ... Types>
inline Proxy<Visitor<Type, Types...>>& Proxy<Visitor<Type, Types...>>::operator=(Proxy<Visitor<Type, Types...>, Type, Types...>&& p) {
	Proxy<Visitor<Type, Types...>, Type, Types...>::pVisitor = std::move(p.pVisitor);
	return *this;
}

template <class ... Types, class Generic>
inline Proxy<Visitor<Types...>> proxy(Generic&& g) {
	Proxy<Visitor<Types...>> c;
	return c = std::forward<Generic>(g);
}

} /* namespace Visit */
#endif /* PROXY_H_ */
