/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef CONSTVOLATILEVISITOR_H_
#define CONSTVOLATILEVISITOR_H_

#include "Visit/ConstVisitor.h"
#include "Visit/VolatileVisitor.h"

namespace Visit {

/**
 * @brief An abstract base class implementing a visitor interface for some
 * given set of types.
 *
 * This class provides an abstract method to visit objects of each provided
 * type. It serves as a base class for any class contemplating to implement
 * visitor methods with a constant volatile this parameter.
 */
template <class ... Types>
class ConstVolatileVisitor
: public ConstVolatileVisitor<Types>... {};

template <class Type>
class ConstVolatileVisitor<Type>
: ConstVisitor<Type>
, VolatileVisitor<Type> {
public:
    virtual void visit(Type&) const volatile = 0;
};

} /* namespace Visit */
#endif /* CONSTVOLATILEVISITOR_H_ */
