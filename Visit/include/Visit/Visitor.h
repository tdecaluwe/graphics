/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef VISITOR_H_
#define VISITOR_H_

namespace Visit {

/**
 * @brief An abstract base class implementing a visitor interface for some
 * given set of types.
 *
 * This class provides an abstract method to visit objects of each provided
 * type. It serves as a base class for any class contemplating to implement
 * visitor methods with a mutable this parameter.
 */
template <class Type, class ... Types>
class Visitor
: protected Visitor<Types...> {
public:
    /**@{
     * @name Constructors
     */
	Visitor() = default;
	Visitor(Visitor const&) = default;
	Visitor(Visitor&&) = default;
	/**@}*/
	virtual ~Visitor() = default;
	Visitor& operator=(Visitor const&) = default;
	Visitor& operator=(Visitor&&) = default;
    /**@{
     * @name Visitor interface
     */
	virtual void visit(Type&) = 0;
	using Visitor<Types...>::visit;
	/**@}*/
};

template <class Type>
class Visitor<Type> {
public:
	Visitor() = default;
	Visitor(Visitor const&) = default;
	Visitor(Visitor&&) = default;
	virtual ~Visitor() = default;
	Visitor& operator=(Visitor const&) = default;
	Visitor& operator=(Visitor&&) = default;
	virtual void visit(Type&) = 0;
};

} /* namespace Visit */
#endif /* VISITOR_H_ */
