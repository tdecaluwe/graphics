/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ADAPTER_H_
#define ADAPTER_H_

// standard library dependencies
#include <utility>
#include <type_traits>

// dependencies
#include "Visit/StateVisitor.h"

namespace Visit {

// forward declarations
template <class Type, class ... Types>
class Visitor;

template <class Abstract, class Generic, class ... Types>
class Adapter;

template <class Abstract, class Generic, class Type, class ... Types>
class Adapter<Abstract, Generic, Type, Types...>
: public Adapter<Abstract, Generic, Types...>::Implementer {
public:
	class Implementer
	: public Adapter<Abstract, Generic, Types...>::Implementer {
	protected:
		explicit Implementer(Generic const&);
		explicit Implementer(Generic&&);
		Implementer(Implementer const&) = default;
		Implementer(Implementer&&) = default;
		Implementer& operator=(Implementer const&) = default;
		Implementer& operator=(Implementer&&) = default;
	public:
		virtual ~Implementer() = default;
		using Adapter<Abstract, Generic, Types...>::Implementer::visit;
		virtual void visit(Type&);
	};
	explicit Adapter(Generic const&);
	explicit Adapter(Generic&&);
	Adapter(Adapter const&) = default;
	Adapter(Adapter&&) = default;
	virtual ~Adapter() = default;
	Adapter& operator=(Generic const&);
	Adapter& operator=(Generic&&);
	Adapter& operator=(Adapter const&) = default;
	Adapter& operator=(Adapter&&) = default;
	using Adapter<Abstract, Generic, Types...>::Implementer::visit;
	virtual void visit(Type&);
};

template <class Abstract, class Generic, class Type>
class Adapter<Abstract, Generic, Type>: public Abstract {
protected:
	Generic mGeneric;
public:
	class Implementer: public Abstract {
	protected:
		Generic mGeneric;
		explicit Implementer(Generic const&);
		explicit Implementer(Generic&&);
		Implementer(Implementer const&) = default;
		Implementer(Implementer&&) = default;
		Implementer& operator=(Implementer const&) = default;
		Implementer& operator=(Implementer&&) = default;
	public:
        /**
         * @todo Replace implementation by `= default` when supported by gcc.
         */
		virtual ~Implementer() {};
		virtual void visit(Type&);
	};
	explicit Adapter(Generic const&);
	explicit Adapter(Generic&&);
	Adapter(Adapter const&) = default;
	Adapter(Adapter&&) = default;
	virtual ~Adapter() = default;
	Adapter& operator=(Generic const&);
	Adapter& operator=(Generic&&);
	Adapter& operator=(Adapter const&) = default;
	Adapter& operator=(Adapter&&) = default;
	virtual void visit(Type&);
};

template <class Generic, class Type, class ... Types>
class Adapter<Visitor<Type, Types...>, Generic>
: public Adapter<Visitor<Type, Types...>, Generic, Type, Types...> {
public:
	explicit Adapter(Generic const&);
	explicit Adapter(Generic&&);
	Adapter(Adapter<Visitor<Type, Types...>, Generic, Type, Types...> const&);
	Adapter(Adapter<Visitor<Type, Types...>, Generic, Type, Types...>&&);
	Adapter(Adapter const&) = default;
	Adapter(Adapter&&) = default;
	virtual ~Adapter() = default;
	Adapter& operator=(Generic const&);
	Adapter& operator=(Generic&&);
	Adapter& operator=(Adapter<Visitor<Type, Types...>, Generic, Type, Types...> const&);
	Adapter& operator=(Adapter<Visitor<Type, Types...>, Generic, Type, Types...>&&);
	Adapter& operator=(Adapter const&) = default;
	Adapter& operator=(Adapter&&) = default;
};

template <class State, class Generic, class Type, class ... Types>
class Adapter<StateVisitor<State, Type, Types...>, Generic>
: public Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...> {
public:
	explicit Adapter(Generic const&);
	explicit Adapter(Generic&&);
	Adapter(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...> const&);
	Adapter(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>&&);
	Adapter(Adapter const&) = default;
	Adapter(Adapter&&) = default;
	virtual ~Adapter() = default;
	Adapter& operator=(Generic const&);
	Adapter& operator=(Generic&&);
	Adapter& operator=(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...> const&);
	Adapter& operator=(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>&&);
	Adapter& operator=(Adapter const&) = default;
	Adapter& operator=(Adapter&&) = default;
	virtual State state() const;
};

template <class Type, class ... Types, class Generic>
Adapter<Visitor<Type, Types...>, typename std::remove_reference<Generic>::type> adapt(Generic&& g);

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>::Adapter(Generic const& g)
: Adapter<Abstract, Generic, Types...>::Implementer(g) {}

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>::Adapter(Generic&& g)
: Adapter<Abstract, Generic, Types...>::Implementer(std::move(g)) {}

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>& Adapter<Abstract, Generic, Type, Types...>::operator=(Generic const& g) {
	Adapter<Abstract, Generic, Types...>::Implementer::mGeneric = g;
	return *this;
}

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>& Adapter<Abstract, Generic, Type, Types...>::operator=(Generic&& g) {
	Adapter<Abstract, Generic, Types...>::Implementer::mGeneric = std::move(g);
	return *this;
}

template <class Abstract, class Generic, class Type, class ... Types>
inline void Adapter<Abstract, Generic, Type, Types...>::visit(Type& t) {
	Adapter<Abstract, Generic, Types...>::Implementer::mGeneric.visit(t);
}

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>::Implementer::Implementer(Generic const& g)
: Adapter<Abstract, Generic, Types...>::Implementer(g) {}

template <class Abstract, class Generic, class Type, class ... Types>
inline Adapter<Abstract, Generic, Type, Types...>::Implementer::Implementer(Generic&& g)
: Adapter<Abstract, Generic, Types...>::Implementer(std::move(g)) {}


template <class Abstract, class Generic, class Type, class ... Types>
inline void Adapter<Abstract, Generic, Type, Types...>::Implementer::visit(Type& t) {
	Adapter<Abstract, Generic, Types...>::Implementer::mGeneric.visit(t);
}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>::Adapter(Generic const& g)
: mGeneric(g) {}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>::Adapter(Generic&& g)
: mGeneric(std::move(g)) {}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>& Adapter<Abstract, Generic, Type>::operator=(Generic const& g) {
	mGeneric = g;
}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>& Adapter<Abstract, Generic, Type>::operator=(Generic&& g) {
	mGeneric = std::move(g);
}

template <class Abstract, class Generic, class Type>
inline void Adapter<Abstract, Generic, Type>::visit(Type& t) {
	mGeneric.visit(t);
}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>::Implementer::Implementer(Generic const& g)
: mGeneric(g) {}

template <class Abstract, class Generic, class Type>
inline Adapter<Abstract, Generic, Type>::Implementer::Implementer(Generic&& g)
: mGeneric(std::move(g)) {}

template <class Abstract, class Generic, class Type>
inline void Adapter<Abstract, Generic, Type>::Implementer::visit(Type& t) {
	mGeneric.visit(t);
}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>::Adapter(Generic const& g)
: Adapter<Visitor<Type, Types...>, Generic, Type, Types...>(g) {}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>::Adapter(Generic&& g)
: Adapter<Visitor<Type, Types...>, Generic, Type, Types...>(std::move(g)) {}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>::Adapter(Adapter<Visitor<Type, Types...>, Generic, Type, Types...> const& a)
: Adapter<Visitor<Type, Types...>, Generic, Type, Types...>(a) {}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>::Adapter(Adapter<Visitor<Type, Types...>, Generic, Type, Types...>&& a)
: Adapter<Visitor<Type, Types...>, Generic, Type, Types...>(std::move(a)) {}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>& Adapter<Visitor<Type, Types...>, Generic>::operator=(Generic const& g) {
	Adapter<Visitor<Type, Types...>, Generic, Type, Types...>::mGeneric = g;
}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>& Adapter<Visitor<Type, Types...>, Generic>::operator=(Generic&& g) {
	Adapter<Visitor<Type, Types...>, Generic, Type, Types...>::mGeneric = std::move(g);
}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>& Adapter<Visitor<Type, Types...>, Generic>::operator=(Adapter<Visitor<Type, Types...>, Generic, Type, Types...> const& a) {
	Adapter<Visitor<Type, Types...>, Generic, Type, Types...>::mGeneric = a.mGeneric;
}

template <class Generic, class Type, class ... Types>
inline Adapter<Visitor<Type, Types...>, Generic>& Adapter<Visitor<Type, Types...>, Generic>::operator=(Adapter<Visitor<Type, Types...>, Generic, Type, Types...>&& a) {
	Adapter<Visitor<Type, Types...>, Generic, Type, Types...>::mGeneric = std::move(a.mGeneric);
}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>::Adapter(Generic const& g)
: Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>(g) {}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>::Adapter(Generic&& g)
: Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>(std::move(g)) {}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>::Adapter(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...> const& a)
: Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>(a) {}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>::Adapter(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>&& a)
: Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>(std::move(a)) {}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>& Adapter<StateVisitor<State, Type, Types...>, Generic>::operator=(Generic const& g) {
	Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>::mGeneric = g;
}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>& Adapter<StateVisitor<State, Type, Types...>, Generic>::operator=(Generic&& g) {
	Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>::mGeneric = std::move(g);
}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>& Adapter<StateVisitor<State, Type, Types...>, Generic>::operator=(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...> const& a) {
	Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>::mGeneric = a.mGeneric;
}

template <class State, class Generic, class Type, class ... Types>
inline Adapter<StateVisitor<State, Type, Types...>, Generic>& Adapter<StateVisitor<State, Type, Types...>, Generic>::operator=(Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>&& a) {
	Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>::mGeneric = std::move(a.mGeneric);
}

template <class State, class Generic, class Type, class ... Types>
inline State Adapter<StateVisitor<State, Type, Types...>, Generic>::state() const {
	return Adapter<StateVisitor<State, Type, Types...>, Generic, Type, Types...>::mGeneric.state();
}

template <class Type, class ... Types, class Generic>
inline Adapter<Visitor<Type, Types...>, typename std::remove_reference<Generic>::type> adapt(Generic&& g) {
	return Adapter<Visitor<Type, Types...>, typename std::remove_reference<Generic>::type>(std::forward<Generic>(g));
}

} /* namespace Visit */
#endif /* ADAPTER_H_ */
