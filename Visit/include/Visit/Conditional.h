/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef CONDITIONAL_H_
#define CONDITIONAL_H_

// standard library dependencies
#include <utility>
#include <type_traits>

// dependencies
#include "Visit/Pass.h"

namespace Visit {

// forward declarations
template <class>
struct State;

template <class Condition, class Then, class Alt = Pass>
class Conditional {
private:
	static_assert(std::is_convertible<bool, typename State<Condition>::Type>::value, "");
	Condition condition;
	Then then;
	Alt alt;
public:
	Conditional(Condition, Then);
	Conditional(Condition, Then, Alt);
	template <class Type>
	void visit(Type&);
	friend struct State<typename std::enable_if<std::is_convertible<typename State<Then>::Type, typename State<Alt>::Type>::value, Conditional<Condition, Then, Alt>>::type>;
};

template <class Condition, class Then, class Alt>
struct State<Conditional<Condition, Then, Alt>> {
	static_assert(std::is_convertible<typename State<Then>::Type, typename State<Alt>::Type>::value || std::is_convertible<typename State<Alt>::Type, typename State<Then>::Type>::value, "Both State types should have a common representation");
	typedef typename std::conditional<std::is_convertible<typename State<Then>::Type, typename State<Alt>::Type>::value, typename State<Then>::Type, typename std::conditional<std::is_convertible<typename State<Alt>::Type, typename State<Then>::Type>::value, typename State<Alt>::Type, void>::type>::type Type;
	static Type get(Conditional<Condition, Then, Alt> const&);
};

template <class Condition, class Then, class Alt>
Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type, typename std::remove_reference<Alt>::type> conditional(Condition&& c, Then&& t, Alt&& a);

template <class Condition, class Then>
Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type> conditional(Condition&& c, Then&& t);

template <class Condition, class Then, class Alt>
inline Conditional<Condition, Then, Alt>::Conditional(Condition c, Then t): condition(c), then(t), alt() {}

template <class Condition, class Then, class Alt>
inline Conditional<Condition, Then, Alt>::Conditional(Condition c, Then t, Alt a): condition(c), then(t), alt(a) {}

template <class Condition, class Then, class Alt>
template <class Type>
inline void Conditional<Condition, Then, Alt>::visit(Type& t) {
	condition.visit(t);
	if (State<Condition>::get(condition)) {
		then.visit(t);
	} else {
		alt.visit(t);
	}
}
template <class Condition, class Then, class Alt>
typename State<Conditional<Condition, Then, Alt>>::Type State<Conditional<Condition, Then, Alt>>::get(Conditional<Condition, Then, Alt> const& c) {
	return State<Condition>::get(c.condition) ? State<Then>::get(c.then) : State<Alt>::get(c.alt);
}

template <class Condition, class Then, class Alt>
inline Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type, typename std::remove_reference<Alt>::type> conditional(Condition&& c, Then&& t, Alt&& a) {
	return Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type, typename std::remove_reference<Alt>::type>(std::forward<Condition>(c), std::forward<Then>(t), std::forward<Alt>(a));
}

template <class Condition, class Then, class Alt>
inline Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type> conditional(Condition&& c, Then&& t) {
	return Conditional<typename std::remove_reference<Condition>::type, typename std::remove_reference<Then>::type>(std::forward<Condition>(c), std::forward<Then>(t));
}

} /* namespace Visit */
#endif /* CONDITIONAL_H_ */
