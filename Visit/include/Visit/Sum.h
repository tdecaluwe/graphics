/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SUM_H_
#define SUM_H_

#include <utility>

namespace Visit {

template <class Generic>
class Sum {
private:
	using State = decltype(static_cast<Generic*>(nullptr)->state());
	Generic mGeneric;
	State mSum;
public:
	Sum(Generic);
	template <class Type>
	void visit(Type&);
	State state() const;
};

template <class Generic>
Sum<typename std::remove_reference<Generic>::type> sum(Generic&&);

template <class Generic>
Sum<Generic>::Sum(Generic g): mGeneric(g), mSum(0) {}

template <class Generic>
template <class Type>
void Sum<Generic>::visit(Type& t) {
	mGeneric.visit(t);
	mSum += mGeneric.state();
}

template <class Generic>
inline typename Sum<Generic>::State Sum<Generic>::state() const {
	return mSum;
}

template <class Generic>
inline Sum<typename std::remove_reference<Generic>::type> sum(Generic&& g) {
	return Sum<typename std::remove_reference<Generic>::type>(std::forward<Generic>(g));
}

} /* namespace Visit */
#endif /* SUM_H_ */
