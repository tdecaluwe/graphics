/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef STORE_H_
#define STORE_H_

// standard library dependencies
#include <utility>

// dependencies
#include "Visit/Visitor.h"

namespace Visit {

template<class Type, class Storage>
class Store: public Visitor<Type> {
private:
	Storage mStorage;
public:
	Storage const& storage() const;
	virtual void visit() const;
};

} /* namespace Visit */
#endif /* STORE_H_ */
