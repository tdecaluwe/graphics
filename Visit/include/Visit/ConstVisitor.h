/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef CONSTVISITOR_H_
#define CONSTVISITOR_H_

#include "Visit/Visitor.h"

namespace Visit {

/**
 * @brief An abstract base class implementing a visitor interface for some
 * given set of types.
 *
 * This class provides an abstract method to visit objects of each provided
 * type. It serves as a base class for any class contemplating to implement
 * visitor methods with a constant this parameter. This means objects of this
 * type can't change their state while visiting targets.
 */
template <class Type, class ... Types>
class ConstVisitor
: public ConstVisitor<Types...>::template Implementer<Visitor<Type, Types...>> {
public:
    template <class Base>
    class Implementer
    : public ConstVisitor<Types...>::template Implementer<Base> {
    public:
        virtual void visit(Type&) const = 0;
        virtual void visit(Type&) {
            static_cast<ConstVisitor const&>(*this).visit();
        }
        using Visitor<Types...>::visit;
    };
public:
    /**@{
     * @name Visitor interface
     */
    virtual void visit(Type&) const = 0;
    virtual void visit(Type&) {
        static_cast<ConstVisitor const&>(*this).visit();
    }
    using ConstVisitor<Types...>::template Implementer<Visitor<Type, Types...>>::visit;
    /**@}*/
};

template <class Type>
class ConstVisitor<Type>
: public Visitor<Type> {
public:
    template <class Base>
    class Implementer
    : public Base {
    public:
        virtual void visit(Type&) const = 0;
        virtual void visit(Type&) {
            static_cast<ConstVisitor const&>(*this).visit();
        }
    };
public:
    virtual void visit(Type&) const = 0;
    virtual void visit(Type&) {
        static_cast<ConstVisitor const&>(*this).visit();
    }
};

} /* namespace Visit */
#endif /* CONSTVISITOR_H_ */
