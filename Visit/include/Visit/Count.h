/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef COUNT_H_
#define COUNT_H_

// standard library dependencies
#include <utility>

namespace Visit {

template <class Generic>
class Count {
private:
	Generic generic;
	unsigned count;
public:
	Count(Generic);
	template <class Type>
	void visit(Type&);
	unsigned state() const;
};

template <class Generic>
Count<typename std::remove_reference<Generic>::type> count(Generic&&);

template <class Generic>
Count<Generic>::Count(Generic g): generic(g), count(0) {}

template <class Generic>
template <class Type>
inline void Count<Generic>::visit(Type& t) {
	generic.visit(t);
	count += 1;
}

template <class Generic>
inline unsigned Count<Generic>::state() const {
	return count;
}

template <class Generic>
inline Count<typename std::remove_reference<Generic>::type> count(Generic&& g) {
	return Count<typename std::remove_reference<Generic>::type>(std::forward<Generic>(g));
}

} /* namespace Visit */
#endif /* COUNT_H_ */
