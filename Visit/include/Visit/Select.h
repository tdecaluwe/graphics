/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SELECT_H_
#define SELECT_H_

// standard library dependencies
#include <utility>

namespace Visit {

template <class Selected>
class Select {
private:
	bool mState;
public:
	template <class Type>
	void visit(Type&);
	void visit(Selected&);
	bool state() const;
};

template <class Selected>
Select<Selected> select();

template <class Selected>
template <class Type>
inline void Select<Selected>::visit(Type& t) {
	mState = false;
}

template <class Selected>
inline void Select<Selected>::visit(Selected& t) {
	mState = true;
}

template <class Selected>
inline bool Select<Selected>::state() const {
	return mState;
}

template <class Selected>
inline Select<Selected> select() {
	return Select<Selected>();
}

} /* namespace Visit */
#endif /* SELECT_H_ */
