/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef APPLY_H_
#define APPLY_H_

#include <utility>
#include <type_traits>

#include "Function/Function.h"

#include "Visit/Sequence.h"

namespace Visit {

// forward declarations
template <class>
struct State;

/*
 * https://groups.google.com/forum/?fromgroups=#!topic/comp.lang.c++.moderated/dQ-jgVzarEU
 */
template <class F, class G, class ... Generics>
class Apply: public Sequence<G, Generics...>, Apply<F, Generics...>::template Implementer<1> {
protected:
	F function;
	using Result = typename Function::Function<decltype(&F::operator())>::Result;
	static_assert(std::is_convertible<decltype(static_cast<G*>(nullptr)->state()), typename Function::Function<decltype(&F::operator())>::template Argument<0>::Type>::value, "");
	template <unsigned int ... indices>
	Result call(typename Sequence<G, Generics...>::template Indices<indices...>) const;
public:
	template <unsigned int index>
	class Implementer: Apply<F, Generics...>::template Implementer<index + 1> {
	protected:
		static_assert(std::is_convertible<decltype(static_cast<G*>(nullptr)->state()), typename Function::Function<decltype(&F::operator())>::template Argument<index>::Type>::value, "");
	};
	Apply(F, G, Generics...);
	Apply(G, Generics...);
	Result state() const;
};

template <class F, class G>
class Apply<F, G>: public Sequence<G> {
protected:
	F function;
	static_assert(std::is_convertible<decltype(static_cast<G*>(nullptr)->state()), typename Function::Function<decltype(&F::operator())>::template Argument<0>::Type>::value, "");
public:
	using Result = typename Function::Function<decltype(&F::operator())>::Result;
	template <unsigned int index>
	class Implementer {
	protected:
		static_assert(std::is_convertible<decltype(static_cast<G*>(nullptr)->state()), typename Function::Function<decltype(&F::operator())>::template Argument<index>::Type>::value, "");
	};
	Apply(F, G);
	Apply(G);
	Result state() const;
};

template <class F, class G, class ... Generics>
Apply<F, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...> apply(G&& g, Generics&&... gen);

template <class F, class G, class ... Generics>
Apply<typename std::remove_reference<F>::type, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...> apply(F&& f, G&& g, Generics&&... gen);

template <class F, class G, class ... Generics>
inline Apply<F, G, Generics...>::Apply(F f, G g, Generics... gen): Sequence<G, Generics...>(g, gen...), function(f) {}

template <class F, class G, class ... Generics>
inline Apply<F, G, Generics...>::Apply(G g, Generics... gen): Sequence<G, Generics...>(g, gen...) {}

template <class F, class G, class ... Generics>
template <unsigned int ... indices>
inline typename Apply<F, G, Generics...>::Result Apply<F, G, Generics...>::call(typename Sequence<G, Generics...>::template Indices<indices...>) const {
	return function(State<G>::get(std::get<0>(Sequence<G, Generics...>::generics)), State<Generics>::get(std::get<indices>(Sequence<G, Generics...>::generics))...);
}

template <class F, class G, class ... Generics>
inline typename Apply<F, G, Generics...>::Result Apply<F, G, Generics...>::state() const {
	return call(typename Sequence<G, Generics...>::template Generator<sizeof...(Generics) + 1>::Type());
}

template <class F, class G>
inline Apply<F, G>::Apply(G g): Sequence<G>(g) {}

template <class F, class G>
inline Apply<F, G>::Apply(F f, G g): Sequence<G>(g), function(f) {}

template <class F, class G>
inline typename Apply<F, G>::Result Apply<F, G>::state() const {
	return function(State<G>::get(std::get<0>(Sequence<G>::generics)));
}

template <class F, class G, class ... Generics>
inline Apply<F, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...> apply(G&& g, Generics&&... gen) {
	return Apply<F, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...>(std::forward<G>(g), std::forward<Generics>(gen)...);
}

template <class F, class G, class ... Generics>
inline Apply<typename std::remove_reference<F>::type, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...> apply(F&& f, G&& g, Generics&&... gen) {
	return Apply<typename std::remove_reference<F>::type, typename std::remove_reference<G>::type, typename std::remove_reference<Generics>::type...>(std::forward<F>(f), std::forward<G>(g), std::forward<Generics>(gen)...);
}

} /* namespace Visit */
#endif /* APPLY_H_ */
