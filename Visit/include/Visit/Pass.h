/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PASS_H_
#define PASS_H_

namespace Visit {

class Pass {
public:
	template <class Type>
	void visit(Type&);
	bool state() const;
};

Pass pass();

template <class Type>
inline void Pass::visit(Type& t) {}

inline bool Pass::state() const {
	return true;
}

} /* namespace Visit */
#endif /* PASS_H_ */
