#include <utility>

#include <boost/test/unit_test.hpp>

#include "Visit/Visitor.h"
#include "Visit/Pass.h"

#include "Visit/Adapter.h"

namespace Visit {

BOOST_AUTO_TEST_SUITE(TestAdapter)

BOOST_AUTO_TEST_CASE(RecursiveCase) {
    int i = -10;
    Adapter<Visitor<int, unsigned>, Pass, int, unsigned> a1 { Pass() };
    Adapter<Visitor<int, unsigned>, Pass, int, unsigned> a2 { Pass() };
    Adapter<Visitor<int, unsigned>, Pass, int, unsigned> a3 { a1 };
    Adapter<Visitor<int, unsigned>, Pass, int, unsigned> a4 { std::move(a2) };
    Pass pass;
    (a1 = Pass()).visit(i);
    (a2 = pass).visit(i);
    (a3 = a1).visit(i);
    (a4 = std::move(a2)).visit(i);

}

BOOST_AUTO_TEST_CASE(BaseCase)
{
    Adapter<Visitor<int>, Pass, int> a1 { Pass() };
    int i = -10;
    a1.visit(i);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Visit */
