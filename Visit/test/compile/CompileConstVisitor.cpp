/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#include "AbstractHost.h"

#include "Visit/ConstVisitor.h"

extern AbstractHost& host;
extern AbstractHost const& constHost;
extern AbstractHost volatile& volatileHost;
extern AbstractHost volatile const& constVolatileHost;

void function() {
    {
        extern Visit::ConstVisitor<SomeHost, AnotherHost>& visitor;
        host.accept(visitor);
    }
    {
        extern Visit::ConstVisitor<SomeHost const, AnotherHost const>& visitor;
        host.accept(visitor);
        constHost.accept(visitor);
    }
    {
        extern Visit::ConstVisitor<SomeHost volatile, AnotherHost volatile>& visitor;
        host.accept(visitor);
        volatileHost.accept(visitor);
    }
    {
        extern Visit::ConstVisitor<SomeHost const volatile, AnotherHost const volatile>& visitor;
        host.accept(visitor);
        constHost.accept(visitor);
        volatileHost.accept(visitor);
        constVolatileHost.accept(visitor);
    }
}
