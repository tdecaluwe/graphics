/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ABSTRACTHOST_H_
#define ABSTRACTHOST_H_

#include "Visit/ConstVisitor.h"
#include "Visit/Visitor.h"
#include "Visit/VolatileVisitor.h"
#include "Visit/ConstVolatileVisitor.h"

// forward declarations
class SomeHost;
class AnotherHost;

class AbstractHost {
public:
    virtual ~AbstractHost() {};
    virtual void accept(Visit::Visitor<SomeHost, AnotherHost>& v) = 0;
    virtual void accept(Visit::ConstVisitor<SomeHost, AnotherHost> const& v) = 0;
    virtual void accept(Visit::VolatileVisitor<SomeHost, AnotherHost> volatile& v) = 0;
    virtual void accept(Visit::ConstVolatileVisitor<SomeHost, AnotherHost> const volatile& v) = 0;
    virtual void accept(Visit::Visitor<SomeHost const, AnotherHost const>& v) const = 0;
    virtual void accept(Visit::ConstVisitor<SomeHost const, AnotherHost const> const& v) const = 0;
    virtual void accept(Visit::VolatileVisitor<SomeHost const, AnotherHost const> volatile& v) const = 0;
    virtual void accept(Visit::ConstVolatileVisitor<SomeHost const, AnotherHost const> const volatile& v) const = 0;
    virtual void accept(Visit::Visitor<SomeHost volatile, AnotherHost volatile>& v) volatile = 0;
    virtual void accept(Visit::ConstVisitor<SomeHost volatile, AnotherHost volatile> const& v) volatile = 0;
    virtual void accept(Visit::VolatileVisitor<SomeHost volatile, AnotherHost volatile> volatile& v) volatile = 0;
    virtual void accept(Visit::ConstVolatileVisitor<SomeHost volatile, AnotherHost volatile> const volatile& v) volatile = 0;
    virtual void accept(Visit::Visitor<SomeHost const volatile, AnotherHost const volatile>& v) const volatile = 0;
    virtual void accept(Visit::ConstVisitor<SomeHost const volatile, AnotherHost const volatile> const& v) const volatile = 0;
    virtual void accept(Visit::VolatileVisitor<SomeHost const volatile, AnotherHost const volatile> volatile& v) const volatile = 0;
    virtual void accept(Visit::ConstVolatileVisitor<SomeHost const volatile, AnotherHost const volatile> const volatile& v) const volatile = 0;
};

#endif /* ABSTRACTHOST_H_ */
