/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ANOTHERHOST_H_
#define ANOTHERHOST_H_

#include "AbstractHost.h"

#include "Visit/ConstVisitor.h"
#include "Visit/Visitor.h"
#include "Visit/VolatileVisitor.h"
#include "Visit/ConstVolatileVisitor.h"

class AnotherHost
: public AbstractHost {
public:
    virtual void accept(Visit::Visitor<Host, AnotherHost>& v) {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVisitor<Host, AnotherHost> const& v) {
        return v.visit(*this);
    }
    virtual void accept(Visit::VolatileVisitor<Host, AnotherHost> volatile& v) {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVolatileVisitor<Host, AnotherHost> const volatile& v) {
        return v.visit(*this);
    }
    virtual void accept(Visit::Visitor<Host const, AnotherHost const>& v) const {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVisitor<Host const, AnotherHost const> const& v) const {
        return v.visit(*this);
    }
    virtual void accept(Visit::VolatileVisitor<Host const, AnotherHost const> volatile& v) const {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVolatileVisitor<Host const, AnotherHost const> const volatile& v) const {
        return v.visit(*this);
    }
    virtual void accept(Visit::Visitor<Host volatile, AnotherHost volatile>& v) volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVisitor<Host volatile, AnotherHost volatile> const& v) volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::VolatileVisitor<Host volatile, AnotherHost volatile> volatile& v) volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVolatileVisitor<Host volatile, AnotherHost volatile> const volatile& v) volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::Visitor<Host const volatile, AnotherHost const volatile>& v) const volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVisitor<Host const volatile, AnotherHost const volatile> const& v) const volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::VolatileVisitor<Host const volatile, AnotherHost const volatile> volatile& v) const volatile {
        return v.visit(*this);
    }
    virtual void accept(Visit::ConstVolatileVisitor<Host const volatile, AnotherHost const volatile> const volatile& v) const volatile {
        return v.visit(*this);
    }
};

#endif /* ANOTHERHOST_H_ */
