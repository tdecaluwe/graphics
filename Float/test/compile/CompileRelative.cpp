#include "Float/Relative.h"

extern float fraction;

extern float f, g;
extern double d, e;

void equal() {
    f == g << Float::relative(fraction);
    f == e << Float::relative(fraction);
    d == g << Float::relative(fraction);
    d == e << Float::relative(fraction);
    f != g << Float::relative(fraction);
    f != e << Float::relative(fraction);
    d != g << Float::relative(fraction);
    d != e << Float::relative(fraction);
}

void less() {
    f < g << Float::relative(fraction);
    f < e << Float::relative(fraction);
    d < g << Float::relative(fraction);
    d < e << Float::relative(fraction);
    f <= g << Float::relative(fraction);
    f <= e << Float::relative(fraction);
    d <= g << Float::relative(fraction);
    d <= e << Float::relative(fraction);
}

void greater() {
    f > g << Float::relative(fraction);
    f > e << Float::relative(fraction);
    d > g << Float::relative(fraction);
    d > e << Float::relative(fraction);
    f >= g << Float::relative(fraction);
    f >= e << Float::relative(fraction);
    d >= g << Float::relative(fraction);
    d >= e << Float::relative(fraction);
}
