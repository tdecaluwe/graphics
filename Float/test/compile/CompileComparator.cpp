#include "SomeFloatingPoint.h"

#include "Float/Builtin.h"
#include "Float/Comparator.h"

extern SomeFloatingPoint<float> s, t;

void function() {
    s == t << Float::builtin();
    s != t << Float::builtin();
    s < t << Float::builtin();
    s <= t << Float::builtin();
    s > t << Float::builtin();
    s >= t << Float::builtin();
}
