#include "Float/ULP.h"

extern unsigned units;

extern float f, g;
extern double d, e;

void equal() {
    f == g << Float::ulp(units);
    f == e << Float::ulp(units);
    d == g << Float::ulp(units);
    d == e << Float::ulp(units);
    f != g << Float::ulp(units);
    f != e << Float::ulp(units);
    d != g << Float::ulp(units);
    d != e << Float::ulp(units);
}

void less() {
    f < g << Float::ulp(units);
    f < e << Float::ulp(units);
    d < g << Float::ulp(units);
    d < e << Float::ulp(units);
    f <= g << Float::ulp(units);
    f <= e << Float::ulp(units);
    d <= g << Float::ulp(units);
    d <= e << Float::ulp(units);
}

void greater() {
    f > g << Float::ulp(units);
    f > e << Float::ulp(units);
    d > g << Float::ulp(units);
    d > e << Float::ulp(units);
    f >= g << Float::ulp(units);
    f >= e << Float::ulp(units);
    d >= g << Float::ulp(units);
    d >= e << Float::ulp(units);
}
