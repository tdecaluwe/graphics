#include "Float/Builtin.h"

extern float f, g;
extern double d, e;

void equal() {
    f == g << Float::builtin();
    f == e << Float::builtin();
    d == g << Float::builtin();
    d == e << Float::builtin();
    f != g << Float::builtin();
    f != e << Float::builtin();
    d != g << Float::builtin();
    d != e << Float::builtin();
}

void less() {
    f < g << Float::builtin();
    f < e << Float::builtin();
    d < g << Float::builtin();
    d < e << Float::builtin();
    f <= g << Float::builtin();
    f <= e << Float::builtin();
    d <= g << Float::builtin();
    d <= e << Float::builtin();
}

void greater() {
    f > g << Float::builtin();
    f > e << Float::builtin();
    d > g << Float::builtin();
    d > e << Float::builtin();
    f >= g << Float::builtin();
    f >= e << Float::builtin();
    d >= g << Float::builtin();
    d >= e << Float::builtin();
}
