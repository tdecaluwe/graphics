#include "Float/Compare.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

#include <limits>

using boost::mpl::list;

namespace Float {

BOOST_AUTO_TEST_SUITE(TestCompare)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Equal, Type, Types) {
    Type a = { };
    Type b = a + Compare<Type>::threshold()*std::numeric_limits<Type>::denorm_min();
    Type c = b + std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::equal(a, b), true);
    BOOST_CHECK_EQUAL(Compare<Type>::equal(a, c), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Less, Type, Types) {
    Type a = { };
    Type b = a + Compare<Type>::threshold()*std::numeric_limits<Type>::denorm_min();
    Type c = b + std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::less(a, b), false);
    BOOST_CHECK_EQUAL(Compare<Type>::less(a, c), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Greater, Type, Types) {
    Type a = { };
    Type b = a + Compare<Type>::threshold()*std::numeric_limits<Type>::denorm_min();
    Type c = b + std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::greater(b, a), false);
    BOOST_CHECK_EQUAL(Compare<Type>::greater(c, a), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Positive, Type, Types) {
    Type a = { }, b = -a;
    Type c = a + std::numeric_limits<Type>::denorm_min();
    Type d = a - std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::positive(a), false);
    BOOST_CHECK_EQUAL(Compare<Type>::positive(b), false);
    BOOST_CHECK_EQUAL(Compare<Type>::positive(c), true);
    BOOST_CHECK_EQUAL(Compare<Type>::positive(d), false);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Negative, Type, Types) {
    Type a = { }, b = -a;
    Type c = a + std::numeric_limits<Type>::denorm_min();
    Type d = a - std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::negative(a), false);
    BOOST_CHECK_EQUAL(Compare<Type>::negative(b), false);
    BOOST_CHECK_EQUAL(Compare<Type>::negative(c), false);
    BOOST_CHECK_EQUAL(Compare<Type>::negative(d), true);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Sign, Type, Types) {
    Type a = { }, b = -a;
    Type c = a + std::numeric_limits<Type>::denorm_min();
    Type d = a - std::numeric_limits<Type>::denorm_min();
    BOOST_CHECK_EQUAL(Compare<Type>::sign(a), false);
    BOOST_CHECK_EQUAL(Compare<Type>::sign(b), true);
    BOOST_CHECK_EQUAL(Compare<Type>::sign(c), false);
    BOOST_CHECK_EQUAL(Compare<Type>::sign(d), true);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Float */
