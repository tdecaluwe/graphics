cmake_minimum_required(VERSION 2.8.8)

# reset local variables
unset(DEPENDENCIES)
unset(SOURCES)
unset(LIBRARIES)
unset(TESTS)

# read dependencies
include(dependencies.cmake OPTIONAL)

# set target name
set(TARGET "Test${PROJECT_NAME}")

# get all source files
discover_sources("${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")
include("${CMAKE_CURRENT_BINARY_DIR}/sources.cmake")

add_executable("${TARGET}" EXCLUDE_FROM_ALL ${CMAKE_CURRENT_BINARY_DIR}/Test.cpp ${SOURCES})
discover_sources_hook("${TARGET}" "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")

# test for successful build
add_test(${PROJECT_NAME}/Build "${CMAKE_COMMAND}" --build ${CMAKE_BINARY_DIR} --target ${TARGET})

# build dependencies
list(APPEND DEPENDENCIES ${PROJECT_NAME} Discover)
build_dependencies("${TARGET}" "${DEPENDENCIES}")

# add libraries
include(libraries.cmake OPTIONAL)
target_link_libraries(Test${PROJECT_NAME} ${LIBRARIES} ${FRAMEWORK})

# add include directory
if(EXISTS "${PROJECT_SOURCE_DIR}/test/include")
    # add test include files
    set_property(TARGET ${TARGET} APPEND PROPERTY INCLUDE_DIRECTORIES "${PROJECT_SOURCE_DIR}/test/include")
endif()

discover_tests("${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")

# reset local variables
mark_as_advanced(FORCE INPUT)
mark_as_advanced(FORCE OUTPUT)
unset(INPUT)

set(OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/tests.cmake")

foreach(SOURCE ${SOURCES})
    get_source_file_property(LOCATION ${SOURCE} LOCATION)
    set(INPUT ${INPUT} ${LOCATION})
endforeach()

add_custom_command(TARGET Test${PROJECT_NAME} PRE_BUILD COMMAND Discover ${INPUT} ${OUTPUT} VERBATIM)

if(EXISTS ${OUTPUT})
    include(${OUTPUT})
endif()

get_property(EXECUTABLE_PATH TARGET Test${PROJECT_NAME} PROPERTY LOCATION)

foreach(TEST ${TESTS})
    add_test(${PROJECT_NAME}/${TEST} ${EXECUTABLE_PATH} --run_test=${TEST})
    set_tests_properties(${PROJECT_NAME}/${TEST} PROPERTIES DEPENDS Test${PROJECT_NAME}/Build)
endforeach()
