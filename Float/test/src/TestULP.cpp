#include "Float/ULP.h"

#include <boost/test/unit_test.hpp>

namespace Float {

BOOST_AUTO_TEST_SUITE(TestULP)

BOOST_AUTO_TEST_CASE(Create) {
    BOOST_CHECK_EQUAL(ulp(5u).difference(), 5u);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Float */
