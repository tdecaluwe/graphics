#include "Float/Comparator.h"

#include "Float/Builtin.h"
#include "Float/ULP.h"
#include "Float/Relative.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Float {

BOOST_AUTO_TEST_SUITE(TestComparator)

using Types = list<float, double, long double>;

BOOST_AUTO_TEST_CASE(Test) {
    float f { 2.0 }, g { 2.0 };
    BOOST_CHECK_EQUAL(f, g << builtin());
    /*BOOST_CHECK_EQUAL(2.0, 2.0 << builtin());
    BOOST_CHECK_EQUAL(f, 2.0 << ulp(5));
    BOOST_CHECK_EQUAL(2.0, 2.0 << ulp(4));
    BOOST_CHECK_EQUAL(f, 2.0 << relative(0.1));
    BOOST_CHECK_EQUAL(2.0, 2.0 << relative(0.1));*/
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Float */
