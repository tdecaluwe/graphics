#include "Float/Relative.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Float {

BOOST_AUTO_TEST_SUITE(TestRelative)

using Types = list<float, double, long double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Create, Type, Types) {
    BOOST_CHECK_EQUAL(relative(Type{ 1.0 }).fraction(), Type{ 1.0 });
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Float */
