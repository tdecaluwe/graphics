#ifndef SOMECONVERTIBLETOSOMEFLOATINGPOINT_H_
#define SOMECONVERTIBLETOSOMEFLOATINGPOINT_H_

#include "SomeFloatingPoint.h"

template <class F>
struct SomeConvertibleToSomeFloatingPoint {
    F number;
    operator SomeFloatingPoint() {
        return { number };
    }
};

#endif /* SOMECONVERTIBLETOSOMEFLOATINGPOINT_H_ */
