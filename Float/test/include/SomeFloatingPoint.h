#ifndef SOMEFLOATINGPOINT_H_
#define SOMEFLOATINGPOINT_H_

#include "Float/EqualityComparable.h"

template <class F>
struct SomeFloatingPoint
: Float::EqualityComparable<SomeFloatingPoint<F>> {
    F number;
    template <class Policy>
    friend bool operator==(SomeFloatingPoint f, Comparator<SomeFloatingPoint, Policy> const& c) {
        return f.number == c.object().number;
    }
};

#endif /* SOMEFLOATINGPOINT_H_ */
