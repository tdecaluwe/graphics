#ifndef SOMECONVERTIBLETOFLOAT_H_
#define SOMECONVERTIBLETOFLOAT_H_

template <class F>
struct SomeConvertibleToFloat {
    F number;
    operator F() {
        return { number };
    }
};

#endif /* SOMECONVERTIBLETOFLOAT_H_ */
