/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef COMPARATOR_H_
#define COMPARATOR_H_

#include "Float/ScalarComparableSwitch.h"

#include <ostream>
#include <utility>

namespace Float {

/**
 * @brief A wrapper for any type in need of floating point based comparison
 * functionality.
 *
 * This class wraps any object of a scalar (floating point) type or class type
 * in combination with an additional policy describing how floating point
 * comparisons are to be carried out.
 */
template <class T, class Policy>
class Comparator
: private ScalarComparableSwitch<Comparator<T, Policy>> {
private:
	T mObject;
    Policy mPolicy;
public:
    constexpr Comparator(T const& t, Policy p);
    /**
     * @brief Conversion from other Comparator type objects.
     *
     * @tparam U a type convertible to T
     *
     * @param c another Comparator object
     */
    template <class U>
    constexpr Comparator(Comparator<U, Policy> const& c);
    /**
     * @brief Move conversion from other Comparator type objects.
     *
     * @tparam U a type convertible to T
     *
     * @param c another Comparator object
     */
    template <class U>
    constexpr Comparator(Comparator<U, Policy>&& c);
    /**
     * @brief Getter for the wrapped object.
     */
    constexpr T const& object() const;
    /**
     * @brief Getter for the policy object.
     */
    constexpr Policy policy() const;
    friend std::ostream& operator<<(std::ostream& out, Comparator const& c) {
        return out << c.object();
    }
};

/**@{
 * @name Comparison forwarding
 *
 * @brief Forward unresolved comparisons to some equivalent comparison.
 *
 * For mixed comparisons --- acting on two different types --- it is not always
 * feasible to inject the necessary comparison operators through the Comparator
 * class template for both types. This is especially true --- but not limited
 * to --- comparisons involving one scalar floating point type and one
 * user-defined type. Consider the following code:
 *
 *     SomePolicy policy;
 *     float f;
 *     UserDefined u;
 *     u == f << policy;
 *
 * The Comparator class instantiated from `f` and `policy` cannot possibly know
 * all types to which `f` can be compared. Therefore some general forwarding
 * comparison operator is defined to attach the policy object to the left hand
 * side of the comparison.
 */
template <class Left, class T, class Policy>
inline bool operator<(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() >= std::forward<Left>(l) << c.policy();
}

template <class Left, class T, class Policy>
inline bool operator>(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() <= std::forward<Left>(l) << c.policy();
}

template <class Left, class T, class Policy>
inline bool operator<=(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() > std::forward<Left>(l) << c.policy();
}

template <class Left, class T, class Policy>
inline bool operator>=(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() < std::forward<Left>(l) << c.policy();
}

template <class Left, class T, class Policy>
inline bool operator==(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() == std::forward<Left>(l) << c.policy();
}

template <class Left, class T, class Policy>
inline bool operator!=(Left&& l, Comparator<T, Policy> const& c) {
	return c.object() != std::forward<Left>(l) << c.policy();
}
/**@}*/

template <class T, class Policy>
inline constexpr Comparator<T, Policy>::Comparator(T const& t, Policy p)
: mObject{ t }
, mPolicy{ p } {}

template <class T, class Policy>
template <class U>
inline constexpr Comparator<T, Policy>::Comparator(Comparator<U, Policy> const& u)
: mObject{ u.object() }
, mPolicy{ u.policy() } {}


template <class T, class Policy>
template <class U>
inline constexpr Comparator<T, Policy>::Comparator(Comparator<U, Policy>&& u)
: mObject{ std::move(u.object()) }
, mPolicy{ std::move(u.policy()) } {}

template <class T, class Policy>
inline constexpr T const& Comparator<T, Policy>::object() const {
    return mObject;
}

template <class T, class Policy>
inline constexpr Policy Comparator<T, Policy>::policy() const {
    return mPolicy;
}

} /* namespace Float */
#endif /* COMPARATOR_H_ */
