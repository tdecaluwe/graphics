/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef COMPARE_H_
#define COMPARE_H_

#include "Float/Float.h"

namespace Float {

template <typename Raw>
class Compare {
private:
	// How many ULP's (Units in the Last Place) we want to tolerate when
	// comparing two numbers.  The larger the value, the more error we
	// allow.  A 0 value means that two numbers must be exactly the same
	// to be considered equal.
	//
	// The maximum error of a single floating-point operation is 0.5
	// units in the last place. On Intel CPU's, all floating-point
	// calculations are done with 80-bit precision, while double has 64
	// bits.  Therefore, 4 should be enough for ordinary use.
	//
	// See the following article for more details on ULP:
	// http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm.
	static const unsigned sMaxULPDifference = 4;
public:
	static bool equal(Raw, Raw);
	static bool less(Raw, Raw);
	static bool greater(Raw, Raw);
	static bool positive(Raw);
	static bool negative(Raw);
    static bool zero(Raw);
	static bool sign(Raw);
    static unsigned threshold();
};

template <class Raw>
inline bool Compare<Raw>::equal(Raw lhs, Raw rhs) {
	Float<Raw> a { lhs };
	Float<Raw> b { rhs };
    if (a.valid() && b.valid()) {
		a.bits() = a.biased();
		b.bits() = b.biased();
		return (a.bits() < b.bits() ? b.bits() - a.bits() : a.bits() - b.bits()) <= sMaxULPDifference;
	} else {
		return false;
	}
}

template <class Raw>
inline bool Compare<Raw>::less(Raw lhs, Raw rhs) {
	Float<Raw> a { lhs };
	Float<Raw> b { rhs };
	if (a.valid() && b.valid()) {
		a.bits() = a.biased();
		b.bits() = b.biased();
		return a.bits() < b.bits() && b.bits() - a.bits() > sMaxULPDifference;
	} else {
		return false;
	}
}

template <class Raw>
inline bool Compare<Raw>::greater(Raw lhs, Raw rhs) {
	Float<Raw> a { lhs };
	Float<Raw> b { rhs };
	if (a.valid() && b.valid()) {
		a.bits() = a.biased();
		b.bits() = b.biased();
		return a.bits() > b.bits() && a.bits() - b.bits() > sMaxULPDifference;
	} else {
		return false;
	}
}

template <class Raw>
inline bool Compare<Raw>::positive(Raw number) {
	return number > 0;
}

template <class Raw>
inline bool Compare<Raw>::negative(Raw number) {
	return number < 0;
}

template <class Raw>
inline bool Compare<Raw>::zero(Raw number) {
	static auto const zero = Float<Raw>::zero().bits();
	Float<Raw> a { number };
	if (a.valid()) {
		a.bits() = a.biased();
		return (a.bits() < zero ? zero - a.bits() : a.bits() - zero) <= sMaxULPDifference;
	} else {
		return false;
	}
}

template <class Raw>
inline bool Compare<Raw>::sign(Raw number) {
	return Float<Raw>{ number }.sign();
}

template <class Raw>
inline unsigned Compare<Raw>::threshold() {
	return sMaxULPDifference;
}

} /* namespace Float */
#endif /* COMPARE_H_ */
