/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef FLOAT_H_
#define FLOAT_H_

#include <limits>
#include <type_traits>
#include <utility>

#include <boost/integer.hpp>

namespace Float {

template <class Raw>
class Float {
private:
	using Bits = typename boost::uint_t<sizeof(Raw)*8>::least;
	using Signed = typename boost::int_t<sizeof(Raw)*8>::least;
	static const unsigned int sMantissaSize = std::numeric_limits<Raw>::digits - 1u;
	static const unsigned int sExponentSize = sizeof(Raw)*8u - 1u - sMantissaSize;
	static const Bits sSignMask = Bits{ 1u } << (sizeof(Raw)*8u - 1u);
	static const Bits sMantissaMask = ~sSignMask >> sExponentSize;
	static const Bits sExponentMask = ~(sSignMask | sMantissaMask);
	union {
		Raw mRaw;
		Bits mBits;
		Signed mSigned;
	} data;
	Bits exponent() const { return data.mBits & sExponentMask; }
	Bits mantissa() const { return data.mBits & sMantissaMask; }
public:
	constexpr explicit Float(Raw in): data{in} {}
	Float& operator=(Raw in) { value() = in; return *this; }
	Raw& value() {
		return data.mRaw;
	}
	Raw const& value() const {
		return data.mRaw;
	}
	Bits& bits() {
		return data.mBits;
	}
	Bits const& bits() const {
		return data.mBits;
	}
	Signed& integer() {
		return data.mSigned;
	}
	Signed const& integer() const {
		return data.mSigned;
	}
	bool sign() const {
		return static_cast<bool>(bits() & sSignMask);
	}
	bool valid() const {
		return value() == value();
	}
	bool nan() const {
		return !valid();
	}
	Bits biased() const {
		return sign() ? ~bits() + 1 : sSignMask | bits();
	}
	typename std::enable_if<(15 & -15) == 1, Signed>::type twosComplement() const {
		return (integer() > 0 ? integer() : -integer())^(integer() & sSignMask);
	}
    static constexpr Float<Raw> zero();
};

template <>
inline constexpr Float<float> Float<float>::zero() {
    return Float<float>{ 0.0f };
}

template <>
inline constexpr Float<double> Float<double>::zero() {
    return Float<double>{ 0.0 };
}

} /* namespace Float */
#endif /* FLOAT_H_ */
