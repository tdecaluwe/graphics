/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ULP_H_
#define ULP_H_

#include "Float/Comparator.h"

namespace Float {

/**
 * @brief A floating point comparison policy allowing some maximum number of
 * ULPs (Units in the Last Place).
 */
class ULP {
private:
    unsigned mDifference;
    friend class ScalarComparable<float, ULP>;
    friend class ScalarComparable<double, ULP>;
    friend class ScalarComparable<long double, ULP>;
    template <class F>
    static constexpr bool less(F a, F b, ULP ulps);
    template <class F>
    static constexpr bool greater(F a, F b, ULP ulps);
    template <class F>
    static constexpr bool equal(F a, F b, ULP ulps);
    template <class F>
    static constexpr bool unequal(F a, F b, ULP ulps);
public:
    constexpr explicit ULP(unsigned difference);
    constexpr unsigned difference() const;
    /**
     * @brief Comparator assembly for ULP comparison policies and floats.
     */
    friend constexpr Comparator<float, ULP> operator<<(float f, ULP ulps) {
        return { f, ulps };
    }
    /**
     * @brief Comparator assembly for ULP comparison policies and doubles.
     */
    friend constexpr Comparator<double, ULP> operator<<(double f, ULP ulps) {
        return { f, ulps };
    }
    /**
     * @brief Comparator assembly for ULP comparison policies and long doubles.
     */
    friend constexpr Comparator<long double, ULP> operator<<(long double f, ULP ulps) {
        return { f, ulps };
    }
};

template <class F>
inline constexpr bool ULP::less(F a, F b, ULP ulps) {
    return a < b;
}

template <class F>
inline constexpr bool ULP::greater(F a, F b, ULP ulps) {
    return a > b;
}

template <class F>
inline constexpr bool ULP::equal(F a, F b, ULP ulps) {
    return a == b;
}

template <class F>
inline constexpr bool ULP::unequal(F a, F b, ULP ulps) {
    return a != b;
}

inline constexpr ULP ulp(unsigned difference) {
    return ULP{ difference };
}

inline constexpr ULP::ULP(unsigned difference)
: mDifference { difference } {}

inline constexpr unsigned ULP::difference() const {
    return mDifference;
}

} /* namespace Float */
#endif /* ULP_H_ */
