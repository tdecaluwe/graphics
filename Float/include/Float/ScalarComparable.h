/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SCALARCOMPARABLE_H_
#define SCALARCOMPARABLE_H_

namespace Float {

// forward declarations
template <class, class>
class Comparator;

/**
 * @brief An empty base class for friend injection purposes.
 */
template <class F, class Policy>
class ScalarComparable {
private:
    static constexpr bool less(F a, F b, Policy p);
    static constexpr bool greater(F a, F b, Policy p);
    static constexpr bool equal(F a, F b, Policy p);
    static constexpr bool unequal(F a, F b, Policy p);
public:
    /**@{
     *
     * @name Comparison operators
     *
     * @brief Comparator objects based on a builtin type will be provided with
     * the following operator overloads.
     *
     * These overloads will dispatch the calls to the static functions provided
     * by the Policy classes. As such the comparison code is not separated from
     * the class definitions which allows this library to be easily extended
     * with additional policies.
     *
     * @return a boolean value
     */
    friend constexpr bool operator<(F f, Comparator<F, Policy> const& c) {
        return ScalarComparable::less(f, c.object(), c.policy());
    }
    friend constexpr bool operator>(F f, Comparator<F, Policy> const& c) {
        return ScalarComparable::greater(f, c.object(), c.policy());
    }
    friend constexpr bool operator<=(F f, Comparator<F, Policy> const& c) {
        return ScalarComparable::greater(c.object(), f, c.policy());
    }
    friend constexpr bool operator>=(F f, Comparator<F, Policy> const& c) {
    	return ScalarComparable::less(c.object(), f, c.policy());
    }
    friend constexpr bool operator==(F f, Comparator<F, Policy> const& c) {
        return ScalarComparable::equal(f, c.object(), c.policy());
    }
    friend constexpr bool operator!=(F f, Comparator<F, Policy> const& c) {
        return ScalarComparable::unequal(f, c.object(), c.policy());
    }
    /**@}*/
};

template <class F, class Policy>
inline constexpr bool ScalarComparable<F, Policy>::less(F a, F b, Policy p) {
    return Policy::less(a, b, p);
}

template <class F, class Policy>
inline constexpr bool ScalarComparable<F, Policy>::greater(F a, F b, Policy p) {
    return Policy::greater(a, b, p);
}

template <class F, class Policy>
inline constexpr bool ScalarComparable<F, Policy>::equal(F a, F b, Policy p) {
    return Policy::equal(a, b, p);
}

template <class F, class Policy>
inline constexpr bool ScalarComparable<F, Policy>::unequal(F a, F b, Policy p) {
    return Policy::unequal(a, b, p);
}

} /* namespace Float */
#endif /* SCALARCOMPARABLE_H_ */
