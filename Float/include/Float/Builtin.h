/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef BUILTIN_H_
#define BUILTIN_H_

#include "Float/Comparator.h"

namespace Float {

/**
 * @brief A floating point comparison policy class matching the builtin
 * comparison operations.
 *
 * @todo Change visibility of comparison functions to private and remove
 * doxygen conditionals when friend class bug is resolved in gcc.
 */
class Builtin {
public:
private:
    friend class ScalarComparable<float, Builtin>;
    friend class ScalarComparable<double, Builtin>;
    friend class ScalarComparable<long double, Builtin>;
    template <class F>
    static constexpr bool less(F a, F b, Builtin);
    template <class F>
    static constexpr bool greater(F a, F b, Builtin);
    template <class F>
    static constexpr bool equal(F a, F b, Builtin);
    template <class F>
    static constexpr bool unequal(F a, F b, Builtin);
public:
    /**
     * @brief Default constructor.
     *
     * Since this class does not have any private or protected data members a
     * declaration (and non-default definition) of a (default) constructor is
     * desirable to prevent this class from being treated as an aggregate.
     */
    constexpr Builtin();
    /**
     * @brief Comparator assembly for Builtin comparison policies and floats.
     */
    friend constexpr Comparator<float, Builtin> operator<<(float f, Builtin b) {
        return { f, b };
    }
    /**
     * @brief Comparator assembly for Builtin comparison policies and doubles.
     */
    friend constexpr Comparator<double, Builtin> operator<<(double f, Builtin b) {
    	return { f, b };
    }
    /**
     * @brief Comparator assembly for Builtin comparison policies and long doubles.
     */
    friend constexpr Comparator<long double, Builtin> operator<<(long double f, Builtin b) {
    	return { f, b };
    }
};

inline constexpr Builtin::Builtin() {}

inline constexpr Builtin builtin() {
    return { };
}

template <class F>
inline constexpr bool Builtin::less(F a, F b, Builtin) {
    return a < b;
}

template <class F>
inline constexpr bool Builtin::greater(F a, F b, Builtin) {
    return a > b;
}

template <class F>
inline constexpr bool Builtin::equal(F a, F b, Builtin) {
    return a == b;
}

template <class F>
inline constexpr bool Builtin::unequal(F a, F b, Builtin) {
    return a != b;
}

} /* namespace Float */
#endif /* BUILTIN_H_ */
