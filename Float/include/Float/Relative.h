/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef RELATIVE_H_
#define RELATIVE_H_

#include "Float/Comparator.h"

#include <type_traits>
#include <utility>

namespace Float {

/**
 * @brief A floating point comparison policy allowing some maximum relative
 * difference.
 */
template <class Fraction>
class Relative {
private:
    Fraction mFraction;
    void fraction(Fraction f);
    friend class ScalarComparable<float, Relative>;
    friend class ScalarComparable<double, Relative>;
    friend class ScalarComparable<long double, Relative>;
    template <class F>
    static constexpr bool less(F a, F b, Relative r);
    template <class F>
    static constexpr bool greater(F a, F b, Relative r);
    template <class F>
    static constexpr bool equal(F a, F b, Relative r);
    template <class F>
    static constexpr bool unequal(F a, F b, Relative r);
public:
    constexpr explicit Relative(Fraction fraction);
    template <class G>
    constexpr Relative(Relative<G> const& relative);
    template <class G>
    constexpr Relative(Relative<G>&& relative);
    constexpr Relative(Relative const& relative) = default;
    constexpr Relative(Relative&& relative) = default;
    ~Relative() = default;
    Relative& operator=(Relative const& relative) = default;
    Relative& operator=(Relative&& relative) = default;
    template <class G>
    Relative& operator=(Relative<G> const& relative);
    template <class G>
    Relative& operator=(Relative<G>&& relative);
    constexpr Fraction fraction() const;
    /**
     * @brief Comparator assembly for Relative comparison policies and floats.
     */
    friend constexpr Comparator<float, Relative> operator<<(float f, Relative r) {
        return { f, r };
    }
    /**
     * @brief Comparator assembly for Relative comparison policies and doubles.
     */
    friend constexpr Comparator<double, Relative> operator<<(double f, Relative r) {
        return { f, r };
    }
    /**
     * @brief Comparator assembly for Relative comparison policies and long
     * doubles.
     */
    friend constexpr Comparator<long double, Relative> operator<<(long double f, Relative r) {
        return { f, r };
    }
};

template <class F>
inline constexpr Relative<typename std::decay<F>::type> relative(F&& fraction) {
    return Relative<typename std::decay<F>::type>{ std::forward<F>(fraction) };
}

template <class Fraction>
template <class F>
inline constexpr bool Relative<Fraction>::less(F a, F b, Relative r) {
    return a < b;
}

template <class Fraction>
template <class F>
inline constexpr bool Relative<Fraction>::greater(F a, F b, Relative r) {
	return a > b;
}

template <class Fraction>
template <class F>
inline constexpr bool Relative<Fraction>::equal(F a, F b, Relative r) {
	return a == b;
}

template <class Fraction>
template <class F>
inline constexpr bool Relative<Fraction>::unequal(F a, F b, Relative r) {
	return a != b;
}

template <class Fraction>
inline void Relative<Fraction>::fraction(Fraction fraction) {
    mFraction = fraction;
}

template <class Fraction>
inline constexpr Relative<Fraction>::Relative(Fraction fraction)
: mFraction { fraction } {}

template <class Fraction>
template <class G>
inline constexpr Relative<Fraction>::Relative(Relative<G> const& relative)
: mFraction { relative.fraction() } {}

template <class Fraction>
template <class G>
inline constexpr Relative<Fraction>::Relative(Relative<G>&& relative)
: mFraction { relative.fraction() } {}

template <class Fraction>
template <class G>
inline Relative<Fraction>& Relative<Fraction>::operator=(Relative<G> const& relative) {
    fraction(relative.fraction());
    return *this;
}

template <class Fraction>
template <class G>
inline Relative<Fraction>& Relative<Fraction>::operator=(Relative<G>&& relative) {
    fraction(std::move(relative.fraction()));
    return *this;
}

template <class Fraction>
inline constexpr Fraction Relative<Fraction>::fraction() const {
    return mFraction;
}

} /* namespace Float */
#endif /* RELATIVE_H_ */
