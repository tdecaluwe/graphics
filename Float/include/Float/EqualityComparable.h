/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef EQUALITYCOMPARABLE_H_
#define EQUALITYCOMPARABLE_H_

#include "Float/Builtin.h"
#include "Float/Comparable.h"

// forward declarations
class ULP;
template <class>
class Relative;
template <class, class>
class Comparator;

namespace Float {

/**
 * @brief A base class for floating point based classes implementing equality
 * comparison.
 *
 * The non-template equality comparison overloads in this class handle implicit
 * conversions and forward the call to the implementation in the user class
 * (which can be templated on policy type).
 */
template <class T, class U = T>
class EqualityComparable
: private Comparable<T, U> {
public:
	/**
	 * @brief Annotate comparisons without a policy with the Builtin policy.
	 */
	friend constexpr bool operator==(T const& t, U const& u) {
		return t == u << builtin();
	}
    /**
     * @brief Equality comparison operator with Comparator support.
     *
     * @param t an object of type T
     * @param c a wrapped object of type U with a Builtin comparison policy
     */
    friend constexpr bool operator==(T const& t, Comparator<U, Builtin> const& c) {
        return t == c;
    }
    /**
     * @brief Equality comparison operator with Comparator support.
     *
     * @param t an object of type T
     * @param c a wrapped object of type U with an ULP comparison policy
     */
    friend constexpr bool operator==(T const& t, Comparator<U, ULP> const& c) {
        return t == c;
    }
    /**
     * @brief Equality comparison operator with Comparator support.
     *
     * @param t an object of type T
     * @param c a wrapped object of type U with a Relative<float> comparison
     * policy
     */
    friend constexpr bool operator==(T const& t, Comparator<U, Relative<float>> const& c) {
        return t == c;
    }
    /**
     * @brief Equality comparison operator with Comparator support.
     *
     * @param t an object of type T
     * @param c a wrapped object of type U with a Relative<double> comparison
     * policy
     */
    friend constexpr bool operator==(T const& t, Comparator<U, Relative<double>> const& c) {
        return t == c;
    }
    /**
     * @brief Equality comparison operator with Comparator support.
     *
     * @param t an object of type T
     * @param c a wrapped object of type U with a Relative<long double>
     * comparison policy
     */
    friend constexpr bool operator==(T const& t, Comparator<U, Relative<long double>> const& c) {
        return t == c;
    }
};

} /* namespace Float */

#endif /* EQUALITYCOMPARABLE_H_ */
