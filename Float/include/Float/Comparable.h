/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef COMPARABLE_H_
#define COMPARABLE_H_

#include "Float/Builtin.h"
#include "Float/Comparator.h"
#include "Float/Relative.h"
#include "Float/ULP.h"

namespace Float {

/**
 * @brief A base class for bridging user classes with this floating point
 * comparison framework.
 *
 * This class provides bitwise left shift operator overloads for annotating
 * any object of a user-defined type with a floating point comparison policy.
 * These overloads are injected in the surrounding namespace of the provided
 * type if it inherits this base class.
 *
 * The choice of the bitwise left shift operator for annotation purposes is
 * based on the following requirements:
 *
 * * Annotation should not interact with the comparison operators, since we
 * want to annotate one of the arguments --- not the result.
 * * However, the annotation operator shouldn't take precedence over
 * arithmetic operators, since an arithmetic expression can be used in one of
 * the sides of a comparison.
 *
 * This leaves us with only the bitwise left shift (`<<`) and the bitwise right
 * shift (`>>`) operator. The left shift was chosen --- by convention --- to be
 * used on the right side of a comparison expression:
 *
 *     float a = 1.0;
 *     if (a == 1.0 << policy) doSomething();
 *
 * Instances of the following classes can be passed as a comparison policy:
 *
 * * Builtin, which forwards all comparisons to the builtin floating point
 * comparisons.
 * * ULP, which expresses the maximum difference as a maximum number of Units
 * in the Last Place (ULPs), which may be especially suited for comparisons
 * involving zero.
 * * Relative, to allow for some maximum relative error.
 */
template <class T, class U = T>
class Comparable {
public:
    /**
     * @brief Comparator assembly for Builtin comparison policies.
     *
     * @param t the object to be wrapped in a Comparator<T, Relative<F>>
     * @param b the Builtin comparison policy
     */
    friend Comparator<T, Builtin> operator<<(T const& t, Builtin b) {
        return { t, b };
    }
    /**
     * @brief Comparator assembly for ULP comparison policies.
     *
     * @param t the object to be wrapped in a Comparator<T, Relative<F>>
     * @param u the ULP comparison policy
     */
    friend Comparator<T, ULP> operator<<(T const& t, ULP u) {
        return { t, u };
    }
    /**
     * @brief Comparator assembly for Relative<Float> comparison policies.
     *
     * Implicit conversions are not needed for Comparator assembly, which means
     * a templated version can be used. In fact all overloads could be
     * implemented as one general template, however more explicit overloads are
     * preferred due to the lack of concepts.
     *
     * @tparam F the floating point type to represent the comparison
     * tolerance.
     *
     * @param t the object to be wrapped in a Comparator<T, Relative<F>>
     * @param r the Relative<F> comparison policy
     */
    template <class F>
    friend Comparator<T, Relative<F>> operator<<(T const& t, Relative<F> r) {
        return { t, r };
    }
};

} /* namespace Float */
#endif /* COMPARABLE_H_ */
