#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <string>
#include <regex>
#include <list>
#include <stdexcept>
#include <memory>

using namespace std;

int main (int argc, char **argv) {
    if (argc < 2) {
        throw runtime_error("Missing argument");
    }

    string contents;
    for (unsigned i = 1; i < argc - 1; ++i) {
        ifstream in(argv[i]);
        contents += string((istreambuf_iterator<char>(in)), istreambuf_iterator<char>());
    }

    string result;
    fstream file(argv[argc - 1]);
    ostringstream out(result);

    result.reserve(1024);

    string slash = "\\/";
    string blank = "[:space:]";
    string whitespace = "[" + blank + "]*";

    string macro_argument = whitespace + "([^(,)]*)" + whitespace;

    string auto_test_suite = "BOOST_AUTO_TEST_SUITE" + whitespace + "\\(" + macro_argument + "\\)";
    string fixture_test_suite = "BOOST_FIXTURE_TEST_SUITE" + whitespace + "\\(" + macro_argument + "," + macro_argument + "\\)";
    string test_suite = auto_test_suite + "|" + fixture_test_suite;

    string auto_test_suite_end = "BOOST_AUTO_TEST_SUITE_END" + whitespace + "\\(" + whitespace + "\\)";

    string auto_test_case = "BOOST_AUTO_TEST_CASE" + whitespace + "\\(" + macro_argument + "\\)";
    string fixture_test_case = "BOOST_FIXTURE_TEST_CASE" + whitespace + "\\(" + macro_argument + "," + macro_argument + "\\)";
    string auto_test_case_template = "BOOST_AUTO_TEST_CASE_TEMPLATE" + whitespace + "\\(" + macro_argument + "," + macro_argument + "," + macro_argument + "\\)";
    string fixture_test_case_template = "BOOST_FIXTURE_TEST_CASE_TEMPLATE" + whitespace + "\\(" + macro_argument + "," + macro_argument + "," + macro_argument + "," + macro_argument + "\\)";
    string test_case = auto_test_case + "|" + fixture_test_case + "|" + auto_test_case_template + "|" + fixture_test_case_template;

    string multiline_comment = slash + "\\*" + "([*][^/]|[^*])*" + "\\*" + slash;
    string comment = slash + slash + "[^\n]*";

    list<string> path;

    out << "set(TESTS";

    basic_regex<char> regex(comment + "|" + multiline_comment + "|" + test_suite + "|" + test_case + "|" + auto_test_suite_end);
    for (regex_iterator<string::const_iterator> iterator(contents.begin(), contents.end(), regex); iterator != regex_iterator<string::const_iterator>(); ++iterator) {
        regex_iterator<string::const_iterator>::value_type const& match = *iterator;
        // comment:                  0 submatches
        // multiline_comment:        1 submatches
        // auto_test_suite:          1 submatches
        if (regex_match(match[0].str(), basic_regex<char>(auto_test_suite))) {
            path.push_back(match[2]);
        }
        // fixture_test_suite:       2 submatches
        if (regex_match(match[0].str(), basic_regex<char>(fixture_test_suite))) {
            path.push_back(match[3]);
        }
        // auto_test_case:           1 submatches
        if (regex_match(match[0].str(), basic_regex<char>(auto_test_case))) {
            out << " ";
            for (auto it = path.begin(); it != path.end(); ++it) {
                out << *it << "/";
            }
            out << match[5];
        }
        // fixture_test_case:        2 submatches
        if (regex_match(match[0].str(), basic_regex<char>(fixture_test_case))) {
            out << " ";
            for (auto it = path.begin(); it != path.end(); ++it) {
                out << *it << "/";
            }
            out << match[6];
        }
        // auto_test_case_template:  3 submatches
        if (regex_match(match[0].str(), basic_regex<char>(auto_test_case_template))) {
            out << " ";
            for (auto it = path.begin(); it != path.end(); ++it) {
                out << *it << "/";
            }
            out << match[8] << "*";
        }
        // fixture_test_case_template:  4 submatches
        if (regex_match(match[0].str(), basic_regex<char>(fixture_test_case_template))) {
            out << " ";
            for (auto it = path.begin(); it != path.end(); ++it) {
                out << *it << "/";
            }
            out << match[11] << "*";
        }
        // auto_test_suite_end:      0 submatches
        if (regex_search(match[0].str(), basic_regex<char>(auto_test_suite_end))) {
            if (path.empty()) {
                throw std::runtime_error("Invalid test program");
            } else {
                path.pop_back();
            }
        }
    }

    out << ")";

    istreambuf_iterator<char> start(file);
    istreambuf_iterator<char> end;
    string content(start, end);

    if (result != content) {
        file << result;
    }
    return 0;
}
