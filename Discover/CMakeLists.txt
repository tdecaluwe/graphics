# @file
# @author Tom De Caluwé <decaluwe.t at gmail.com>
#
# @copyright Copyright 2013 Tom De Caluwé.
#
# @par License
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see http://www.gnu.org/licenses/.

# This is the project level CMakeLists.txt file. It should be used in any
# directory consisting of a separate project. It performs the following tasks:
#
# * Setting up a new cmake project
# * Search and include the necessary cmake script files
# * Search for the boost unit test framework
# * Search and setup doxygen
# * Add the src directory, responsible for building the artifact
# * Add any subprojects
# * Add the test directory

cmake_minimum_required(VERSION 2.8.3)

#reset local variables
unset(PROJECTS)
unset(TAGFILES)
unset(DEPENDENCIES)

add_definitions(-std=c++11 -pedantic-errors)
enable_testing()

# project name
get_filename_component(DIRECTORY_NAME ${CMAKE_CURRENT_LIST_DIR} NAME)
project(${DIRECTORY_NAME} CXX)

set(WORKSPACE ${PROJECT_SOURCE_DIR}/..)
list(APPEND WORKSPACE_PATH ${WORKSPACE_PATH} ${WORKSPACE})

# set default build type
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE RelWithDebInfo CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif()

# set binary output directories
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# set appropriate cmake module path
if(EXISTS "${PROJECT_SOURCE_DIR}/cmake/Modules")
    set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake/Modules")
endif()

# additional project configuration
include(setup.cmake OPTIONAL)

if(NOT DEFINED CBUILD_FOUND)
    # look for necessary functions
    # include cmake discovery script
    include(${CMAKE_SOURCE_DIR}/cmake/discover.cmake)
    # include cmake dependency build script
    include(${CMAKE_SOURCE_DIR}/cmake/build.cmake)
    set(CBUILD_FOUND "TRUE")
endif()

# find boost
if(NOT DEFINED Boost_UNIT_TEST_FRAMEWORK_FOUND)
    find_package(Boost COMPONENTS unit_test_framework)
    if(NOT Boost_UNIT_TEST_FRAMEWORK_FOUND)
        find_package(Boost)
    endif()
endif()

#find doxygen
if(NOT DEFINED DOXYGEN_FOUND)
    find_package(Doxygen)
endif()

# add a target to generate API documentation with Doxygen
if(DOXYGEN_FOUND)
    if(NOT DOXYGEN_DOC_DIR)
        set(DOXYGEN_DOC_DIR "${CMAKE_SOURCE_DIR}/doc/")
        set(DOXYGEN_CURRENT_DOC_DIR "${DOXYGEN_DOC_DIR}")
    endif()
    if(EXISTS "${PROJECT_SOURCE_DIR}/README.md" AND NOT IS_DIRECTORY "${PROJECT_SOURCE_DIR}/README.md")
        set(DOXYGEN_MAINPAGE "${PROJECT_SOURCE_DIR}/README.md")
    else()
        set(DOXYGEN_MAINPAGE)
    endif()
    file(MAKE_DIRECTORY ${DOXYGEN_DOC_DIR}/${PROJECT_NAME})
    # set doxygen variables
    set(DOXYGEN_TAGFILE ${CMAKE_BINARY_DIR}/Tag${PROJECT_NAME})
    set(DOXYGEN_OUTPUT_DIRECTORY ${DOXYGEN_DOC_DIR}/${PROJECT_NAME})
    # add doxygen command
    add_custom_command(OUTPUT ${DOXYGEN_TAGFILE} COMMAND ${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile MAIN_DEPENDENCY ${PROJECT_SOURCE_DIR}/Doxyfile.in COMMENT "Generating documentation for ${PROJECT_NAME}")
    # provide a phony target to build the documentation
    add_custom_target(Doc${PROJECT_NAME} DEPENDS ${DOXYGEN_TAGFILE} ${PROJECT_BINARY_DIR}/Doxyfile)
    include(src/dependencies.cmake OPTIONAL)
    foreach(DEPENDENCY ${DEPENDENCIES})
        # link to dependent project documentation
        set(DOXYGEN_TAGFILES "${DOXYGEN_TAGFILES} \"${CMAKE_BINARY_DIR}/Tag${DEPENDENCY}=../../${DEPENDENCY}/html\"")
        add_dependencies(Doc${PROJECT_NAME} Doc${DEPENDENCY})
    endforeach()
    # produce a doxygen configuration file
    configure_file(${PROJECT_SOURCE_DIR}/Doxyfile.in ${PROJECT_BINARY_DIR}/Doxyfile @ONLY)
else()
    message("-- Documentation generation will be disabled")
endif()

# add source subdirectories
add_subdirectory(src)

# add nested projects
include(projects.cmake OPTIONAL)
build_projects("${PROJECTS}")

# add test subdirectory
add_subdirectory(test)
