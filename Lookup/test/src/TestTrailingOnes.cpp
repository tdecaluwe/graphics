#include "Lookup/TrailingOnes.h"

#include <boost/test/unit_test.hpp>

namespace Lookup {

BOOST_AUTO_TEST_SUITE(TestTrailingOnes)

template <unsigned size>
using Table = TrailingOnes<unsigned, size>;

BOOST_AUTO_TEST_CASE(Size) {
    BOOST_CHECK_EQUAL(Table<31u>::size(), 32);
    BOOST_CHECK_EQUAL(Table<32u>::size(), 32);
    BOOST_CHECK_EQUAL(Table<33u>::size(), 64);
}

BOOST_AUTO_TEST_CASE(FourBit) {
    using Table = TrailingOnes<unsigned, 16u>;
    BOOST_CHECK_EQUAL(Table::get(0x0), 0);
    BOOST_CHECK_EQUAL(Table::get(0x1), 1);
    BOOST_CHECK_EQUAL(Table::get(0x2), 0);
    BOOST_CHECK_EQUAL(Table::get(0x3), 2);
    BOOST_CHECK_EQUAL(Table::get(0x4), 0);
    BOOST_CHECK_EQUAL(Table::get(0x5), 1);
    BOOST_CHECK_EQUAL(Table::get(0x6), 0);
    BOOST_CHECK_EQUAL(Table::get(0x7), 3);
    BOOST_CHECK_EQUAL(Table::get(0x8), 0);
    BOOST_CHECK_EQUAL(Table::get(0x9), 1);
    BOOST_CHECK_EQUAL(Table::get(0xA), 0);
    BOOST_CHECK_EQUAL(Table::get(0xB), 2);
    BOOST_CHECK_EQUAL(Table::get(0xC), 0);
    BOOST_CHECK_EQUAL(Table::get(0xD), 1);
    BOOST_CHECK_EQUAL(Table::get(0xE), 0);
    BOOST_CHECK_EQUAL(Table::get(0xF), 4);
}

BOOST_AUTO_TEST_CASE(Test) {
    using Table = TrailingOnes<unsigned char, 256u>;
    BOOST_CHECK_EQUAL(Table::get(0x0), 0);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Lookup */
