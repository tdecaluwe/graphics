#include "Lookup/Table.h"

#include <boost/test/unit_test.hpp>

namespace Lookup {

BOOST_AUTO_TEST_SUITE(TestTable)

BOOST_AUTO_TEST_CASE(Instantiation) {
    using Table = Table<int, 5, 7, -3, 9, -23, 16>;
    BOOST_CHECK_EQUAL(Table::get(0), 5);
    BOOST_CHECK_EQUAL(Table::get(1), 7);
    BOOST_CHECK_EQUAL(Table::get(2), -3);
    BOOST_CHECK_EQUAL(Table::get(3), 9);
    BOOST_CHECK_EQUAL(Table::get(4), -23);
    BOOST_CHECK_EQUAL(Table::get(5), 16);
    BOOST_CHECK_EQUAL(Table::size(), 6);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Lookup */
