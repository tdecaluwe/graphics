/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#include "Lookup/Population.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Lookup {

BOOST_AUTO_TEST_SUITE(TestPopulation)

using Types = list<char, unsigned char, short, unsigned short, int, unsigned int, long, unsigned long>;

/**
 * @brief Test for correct size of recursively generated Population tables.
 *
 * @test Since these tables are produced recursively in a binary fashion their
 * sizes will always be a power of two.
 */
BOOST_AUTO_TEST_CASE_TEMPLATE(Size, Type, Types) {
    BOOST_CHECK_EQUAL((Population<Type, 31u>::size()), 32);
    BOOST_CHECK_EQUAL((Population<Type, 32u>::size()), 32);
    BOOST_CHECK_EQUAL((Population<Type, 33u>::size()), 64);
}

/**
 * @brief Test a 4-bit Population table instantiation.
 *
 * @test This test checks the correctness of a 4-bit Population table by
 * hardwiring the expected outcomes in the test conditional statements.
 */
BOOST_AUTO_TEST_CASE_TEMPLATE(FourBit, Type, Types) {
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x0)), 0);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x1)), 1);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x2)), 1);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x3)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x4)), 1);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x5)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x6)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x7)), 3);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x8)), 1);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0x9)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xA)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xB)), 3);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xC)), 2);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xD)), 3);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xE)), 3);
    BOOST_CHECK_EQUAL((Population<Type, 16u>::get(0xF)), 4);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Lookup */
