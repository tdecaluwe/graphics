/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef REVERT_H_
#define REVERT_H_

#include "Lookup/Combine.h"
#include "Lookup/Pack.h"

namespace Lookup {

template <class List>
class Revert;

template <class T, T ... list>
class Revert<Pack<T, list...>> {
private:
    template <T first, T ... t>
    struct Implementation {
        using Type = typename Combine<Pack<T, t...>, Pack<T, first>>::Type;
    };
public:
    using Type = typename Implementation<list...>::Type;
};

template <class T>
class Revert<Pack<T>> {
public:
    using Type = Pack<T>;
};

} /* namespace Lookup */
#endif /* REVERT_H_ */
