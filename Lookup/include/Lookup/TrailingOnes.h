/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRAILINGONES_H_
#define TRAILINGONES_H_

#include "Lookup/BinaryRecursion.h"
#include "Lookup/Identity.h"
#include "Lookup/MakeTable.h"
#include "Lookup/Pack.h"
#include "Lookup/Revert.h"

namespace Lookup {

template <class List>
class IncrementFirst;

template <class T, T ... list>
class IncrementFirst<Pack<T, list...>> {
private:
    template <T first, T ... t>
    struct Implementation {
        using Type = Pack<T, first + 1, t...>;
    };
public:
    using Type = typename Implementation<list...>::Type;
};

/**
 * @brief A lookup table returning the number of set bits at the end of a
 * bitfield.
 *
 * This is also equal to the offset of the first unset bit. Also note that the
 * number of unset bits at the end of a bitfield can be found by first
 * subtracting said bitfield by one and subsequently determining the number of
 * set bits.
 */
template <class I, unsigned size>
class TrailingOnes
: public MakeTable<typename Revert<typename BinaryRecursion<I, size, IncrementFirst, Identity, Pack<I, 0>>::Type>::Type>::Type {};

} /* namespace Lookup */
#endif /* TRAILINGONES_H_ */
