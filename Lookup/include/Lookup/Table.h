/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TABLE_H_
#define TABLE_H_

#include <boost/integer.hpp>

namespace Lookup {

/**
 * @brief A class template which constructs a lookup table from its template
 * parameters.
 */
template <class T, T ... table>
class Table {
private:
    static constexpr T const sTable[sizeof...(table)] = { table... };
    static constexpr unsigned bits(unsigned size);
public:
    using Index = typename boost::uint_t<bits(sizeof...(table))>::fast;
    static constexpr unsigned size();
    static constexpr T get(Index index);
};

template <class T, T ... table>
constexpr T const Table<T, table...>::sTable[sizeof...(table)];

template <class T, T ... table>
inline constexpr unsigned Table<T, table...>::bits(unsigned size) {
    return size > 1 ? bits((size+1)/2) + 1 : 0;
}

template <class T, T ... table>
inline constexpr unsigned Table<T, table...>::size() {
    return sizeof...(table);
}

template <class T, T ... table>
inline constexpr T Table<T, table...>::get(Index index) {
    return sTable[index];
}

} /* namespace Lookup */
#endif /* TABLE_H_ */
