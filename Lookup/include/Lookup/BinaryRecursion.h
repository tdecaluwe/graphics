/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef BINARYRECURSION_H_
#define BINARYRECURSION_H_

#include "Lookup/Combine.h"
#include "Lookup/Pack.h"

namespace Lookup {

template <class T, unsigned size, template <class> class Left, template <class> class Right, class List>
class BinaryRecursion;

template <class T, unsigned size, template <class> class Left, template <class> class Right, T ... list>
class BinaryRecursion<T, size, Left, Right, Pack<T, list...>>
: public BinaryRecursion<T, (size + 1)/2, Left, Right, typename Combine<typename Left<Pack<T, list...>>::Type, typename Right<Pack<T, list...>>::Type>::Type> {};

template <class T, template <class> class Left, template <class> class Right, T ... list>
class BinaryRecursion<T, 1u, Left, Right, Pack<T, list...>> {
public:
    using Type = Pack<T, list...>;
};

} /* namespace Lookup */
#endif /* BINARYRECURSION_H_ */
