#include "Thread/ScopedUnlock.h"

#include <boost/test/unit_test.hpp>

#include <mutex>

using std::mutex;
using std::unique_lock;
using std::defer_lock;

namespace Thread {

BOOST_AUTO_TEST_SUITE(TestScopedUnlock)

BOOST_AUTO_TEST_CASE(Semantics) {
    mutex m;
    unique_lock<mutex> lock(m, defer_lock);
    {
        BOOST_CHECK_EQUAL(lock.try_lock(), true);
        ScopedUnlock<unique_lock<mutex>> scopedUnlock(lock);
        BOOST_CHECK_EQUAL(lock.owns_lock(), false);
    }
    BOOST_CHECK_EQUAL(lock.owns_lock(), true);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Thread */
