#include "Thread/ScopedLock.h"

#include <boost/test/unit_test.hpp>

#include <mutex>

using std::mutex;
using std::unique_lock;
using std::defer_lock;

namespace Thread {

BOOST_AUTO_TEST_SUITE(TestScopedLock)

BOOST_AUTO_TEST_CASE(Semantics) {
    mutex m;
    unique_lock<mutex> lock(m, defer_lock);
    {
        ScopedLock<unique_lock<mutex>> scopedLock(lock);
        BOOST_CHECK_EQUAL(lock.owns_lock(), true);
    }
    BOOST_CHECK_EQUAL(lock.owns_lock(), false);
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Thread */
