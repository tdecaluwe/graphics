/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef BUFFER_H_
#define BUFFER_H_

#include <queue>
#include <mutex>

#include "Semaphore.h"

namespace Thread {

template <class Type, unsigned size>
class Buffer {
private:
	std::queue<Type> mBuffer;
	Semaphore mEmpty;
	Semaphore mFull;
	std::mutex mMutex;
public:
	Buffer();
	void push(Type t);
	Type get();
};

template <class Type, unsigned size>
inline Buffer<Type, size>::Buffer(): mFull(0), mEmpty(size) {}

template <class Type, unsigned size>
inline void Buffer<Type, size>::push(Type t) {
	mEmpty.wait();
	mMutex.lock();
	mBuffer.emplace(std::move(t));
	mMutex.unlock();
	mFull.notify();
}

template <class Type, unsigned size>
inline Type Buffer<Type, size>::get() {
	mFull.wait();
	mMutex.lock();
	Type t = mBuffer.front();
	mBuffer.pop();
	mMutex.unlock();
	mEmpty.notify();
	return t;
}

} /* namespace Thread */
#endif /* BUFFER_H_ */
