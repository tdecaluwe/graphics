/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef UNSCOPEDLOCK_H_
#define UNSCOPEDLOCK_H_

namespace Thread {

template <class Lockable>
class UnscopedLock {
private:
	Lockable& rLockable;
public:
	UnscopedLock(Lockable& l): rLockable(l) {}
	~UnscopedLock() {
		rLockable.lock();
	}
};

} /* namespace Thread */
#endif /* UNSCOPEDLOCK_H_ */
