/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef QUEUE_H_
#define QUEUE_H_

#include <queue>
#include <mutex>
#include <iostream>

#include "Semaphore.h"

namespace Thread {

template <class Type>
class Queue {
private:
	std::queue<Type> mQueue;
	Semaphore mSize;
	std::mutex mMutex;
public:
	Queue();
	void push(Type t);
	Type get();
	unsigned size() const;
};

template <class Type>
inline Queue<Type>::Queue(): mSize(0) {}

template <class Type>
inline void Queue<Type>::push(Type t) {
	mMutex.lock();
	mQueue.emplace(std::move(t));
	mMutex.unlock();
	mSize.notify();
}

template <class Type>
inline Type Queue<Type>::get() {
	mSize.wait();
	mMutex.lock();
	Type t = mQueue.front();
	mQueue.pop();
	mMutex.unlock();
	return t;
}

template <class Type>
inline unsigned Queue<Type>::size() const {
	return mQueue.size();
}

} /* namespace Thread */
#endif /* QUEUE_H_ */
