/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SEMAPHORE_H_
#define SEMAPHORE_H_

#include <mutex>
#include <condition_variable>

namespace Thread {

class Semaphore {
private:
	std::mutex mMutex;
	std::condition_variable mCondition;
	unsigned mCount;
public:
	Semaphore();
	explicit Semaphore(unsigned c);
	void notify();
	void wait();
	unsigned count() const;
};

inline Semaphore::Semaphore(): mCount(0) {}

inline Semaphore::Semaphore(unsigned c): mCount(c) {}

inline void Semaphore::notify() {
	std::lock_guard<std::mutex> lock(mMutex);
	++mCount;
	mCondition.notify_one();
}
inline void Semaphore::wait() {
	std::unique_lock<std::mutex> lock(mMutex);
	mCondition.wait(lock, [this]() { return static_cast<bool>(mCount); });
	--mCount;
}

inline unsigned Semaphore::count() const {
	return mCount;
}

} /* namespace Thread */
#endif /* SEMAPHORE_H_ */
