/**
 * @namespace Light
 *
 * @brief A header only library providing light related functionality.
 *
 * The main purpose consists of providing a way to represent light and it's
 * behavior in the context of graphical applications. To this end, light can be
 * thought of as consisting of different channels, since we can never
 * (efficiently) represent a complete spectrum. Such a channel corresponds with
 * a spectral intensity distribution. Do keep in mind that channels can also be
 * implicitly defined, meaning they only correspond to tristimulus values
 * (which is equivalent to color and saturation) in another color space.
 * Especially certain RGB color spaces are defined in such a way.
 */
namespace Light {

} /* namespace Light */
