/**
 * @file
 *
 * @copyright Copyright 2012 Google.
 *
 * @par License
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy
 * of the License at http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

#ifndef FONT_NUMBERS_H
#define FONT_NUMBERS_H

namespace Font {

/**
 * @brief A set of numerical glyphs.
 *
 * This class contains the numerical glyphs from the google Roboto font (light
 * version, weight 300).
 */
class Numbers {
public:
    /**
     * @brief Binary data
     */
    static unsigned char const data[];
    /**
     * @brief The size of the provided data
     */
    static unsigned int const length;
};

} /* namespace Font */
#endif /* FONT_NUMBERS_H */
