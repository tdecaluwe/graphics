#include "Math/Vector.h"

#include "Geometry/Zonotope.h"

#include "BulletCollision/CollisionDispatch/btCollisionDispatcher.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"

#include <iostream>

using Math::Vector;

using Geometry::Soup;
using Geometry::Zonotope;

using namespace std;

int main() {
    btSphereShape a(5.0), b(3.0);
    Soup<float> s;
    Zonotope<float, 3> z
    { Vector<float, 3>(0.5, 0.0, 0.0)
    , Vector<float, 3>(0.0, 0.5, 0.0)
    , Vector<float, 3>(2.5, 2.5, 2.5)
    , Vector<float, 3>(0.0, 0.0, 0.5) };
    auto tris = hull(z);
    std::cout << tris.size() << std::endl;
    for (Geometry::Triangle<float, 3> tri : tris) {
        std::cout << tri[0] + Vector<float, 3>{ 3.0, 3.0, 3.0 } << ", ";
        std::cout << tri[1] + Vector<float, 3>{ 3.0, 3.0, 3.0 } << ", ";
        std::cout << tri[2] + Vector<float, 3>{ 3.0, 3.0, 3.0 } << std::endl;
    }
}
