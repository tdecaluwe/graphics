enable_language(C)

find_package(OpenGL REQUIRED)

include_directories(/usr/include/bullet)

set(LIBRARIES BulletCollision LinearMath ${OPENGL_LIBRARIES})
