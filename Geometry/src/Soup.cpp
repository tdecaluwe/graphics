#include "Geometry/Soup.h"

#include "Geometry/Vector.h"

#include "Math/Field.h"

#include <array>

namespace Geometry {

template <class T>
Soup<T> triangulate(Point<T, 3> const& p, Sphere<T> const& s) {
    unsigned const iterations = 2;
	T zero = { 0.0 };
	T unit = { 1.0 };
	T phi = { (1 + Math::Field<T>::sqrt(T{ 5.0 }))/T{ 2.0 } };
	std::vector<std::array<Math::Vector<T, 3>, 3>> geometry;
	geometry.reserve(20 << 2*iterations);
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +unit, +phi, zero }, { zero, +unit, +phi }, { -unit, +phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +unit, +phi, zero }, { -unit, +phi, zero }, { zero, +unit, -phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +unit, -phi, zero }, { -unit, -phi, zero }, { zero, -unit, +phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +unit, -phi, zero }, { zero, -unit, -phi }, { -unit, -phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { zero, +unit, +phi }, { +phi, zero, +unit }, { zero, -unit, +phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { zero, +unit, +phi }, { zero, -unit, +phi }, { -phi, zero, +unit } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { zero, +unit, -phi }, { zero, -unit, -phi }, { +phi, zero, -unit } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { zero, +unit, -phi }, { -phi, zero, -unit }, { zero, -unit, -phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, +unit }, { +unit, +phi, zero }, { +phi, zero, -unit } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, +unit }, { +phi, zero, -unit }, { +unit, -phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, +unit }, { -phi, zero, -unit }, { -unit, +phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, +unit }, { -unit, -phi, zero }, { -phi, zero, -unit } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, -unit }, { -unit, -phi, zero }, { zero, -unit, -phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, -unit }, { zero, -unit, -phi }, { +unit, -phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, -unit }, { zero, +unit, -phi }, { -unit, +phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, -unit }, { +unit, +phi, zero }, { zero, +unit, -phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, +unit }, { zero, -unit, +phi }, { -unit, -phi, zero } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, +unit }, { +unit, -phi, zero }, { zero, -unit, +phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { -phi, zero, +unit }, { -unit, +phi, zero }, { zero, +unit, +phi } } });
	geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ { { +phi, zero, +unit }, { zero, +unit, +phi }, { +unit, +phi, zero } } });
	for (unsigned iteration = 0; iteration < iterations; ++iteration) {
        for (unsigned i = 0; i < 20 << 2*iteration; ++i) {
            Math::Vector<T, 3> a = (geometry[i][1] + geometry[i][2])/2.0;
            Math::Vector<T, 3> b = (geometry[i][0] + geometry[i][2])/2.0;
            Math::Vector<T, 3> c = (geometry[i][0] + geometry[i][1])/2.0;
            geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ geometry[i][0], c, b });
            geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ geometry[i][1], a, c });
            geometry.emplace_back(std::array<Math::Vector<T, 3>, 3>{ geometry[i][2], b, a });
            geometry[i][0] = a;
            geometry[i][0] = b;
            geometry[i][0] = c;
        }
	}
	Soup<T> icosahedron;
	icosahedron.reserve(20 << 2*iterations);
	for (unsigned i = 0; i < 20 << 2*iterations; ++i) {
		icosahedron.add({p + normalize(geometry[i][0]), p + normalize(geometry[i][1]), p + normalize(geometry[i][2])});
	}
	return std::move(icosahedron);
}

template Soup<float> triangulate<float>(Point<float, 3> const&, Sphere<float> const& s);
template Soup<double> triangulate<double>(Point<double, 3> const&, Sphere<double> const& s);

} /* namespace Geometry */
