#include "Geometry/Zonotope.h"

namespace Geometry {

template <class T, unsigned n>
unsigned Zonotope<T, n>::maximumPolytopeSize() const {
    unsigned x = mVectors.size();
    unsigned product = 1;
    unsigned result = 1;
    for (unsigned index = 1; index < n; ++index) {
        x -= 1;
        result *= index;
        product *= x;
        result += product;
    }
    return 2*result/Size::binomial(n - 1);
}

template class Zonotope<float, 1>;
template class Zonotope<float, 2>;
template class Zonotope<float, 3>;
template class Zonotope<float, 4>;

template class Zonotope<double, 1>;
template class Zonotope<double, 2>;
template class Zonotope<double, 3>;
template class Zonotope<double, 4>;

} /* namespace Geometry */
