#include "Geometry/Triangulate.h"

#include "Geometry/Point.h"
#include "Geometry/Vector.h"
#include "Geometry/Zonotope.h"

#include "Math/Ring.h"

#include <vector>
#include <iostream>

namespace Geometry {

template <class T>
std::vector<Triangle<T, 3>> Triangulate<Zonotope<T, 3>>::hull(Zonotope<T, 3> const& z) {
    std::vector<T[6]> lines;
    lines.reserve(z.size());
    // the
    for (unsigned i = 0; i < z.size(); ++i) {
        // dual plucker coordinates of the intersections of the planes defined
        // by the normal vectors and the plane at infinity
        lines[i][0] = z[i][0];
        lines[i][1] = z[i][1];
        lines[i][2] = z[i][2];
        lines[i][3] = 0.0;
        lines[i][4] = 0.0;
        lines[i][5] = 0.0;
    }
    std::vector<Triangle<T, 3>> triangles;
    triangles.reserve(z.maximumPolytopeSize());
    Point<T, 3> p = Point<T, 3>::origin();
    for (unsigned i = 0; i < z.size(); ++i) {
        for (unsigned j = 0; j < i; ++j) {
            Vector<T, 3> intersection = z[i]%z[j];
            Vector<T, 3> total = Vector<T, 3>::origin();
            for (unsigned k = 0; k < z.size(); ++k) {
                if (k != i && k != j) {
                    if (intersection*z[k] > Math::Ring<T>::zero()) {
                        total += z[k];
                    } else {
                        total -= z[k];
                    }
                }
            }
            triangles.push_back(Triangle<T, 3>{ p + total + z[i] + z[j], p + total + z[i] - z[j], p + total - z[i] + z[j] });
            triangles.push_back(Triangle<T, 3>{ p + total - z[i] - z[j], p + total + z[i] - z[j], p + total - z[i] + z[j] });
        }
    }
    return triangles;
}

template class Triangulate<Zonotope<float, 3>>;
template class Triangulate<Zonotope<double, 3>>;

} /* namespace Geometry */
