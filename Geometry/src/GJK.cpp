#include "Geometry/GJK.h"

using Float::Compare;
using Math::Vector;

namespace Geometry {

template <>
bool GJK<float, 1>::collide(Convex<float, 1> const& a, Convex<float, 1> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<float, 2>::collide(Convex<float, 2> const& a, Convex<float, 2> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<float, 3>::collide(Convex<float, 3> const& a, Convex<float, 3> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<float, 4>::collide(Convex<float, 4> const& a, Convex<float, 4> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<double, 1>::collide(Convex<double, 1> const& a, Convex<double, 1> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<double, 2>::collide(Convex<double, 2> const& a, Convex<double, 2> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<double, 3>::collide(Convex<double, 3> const& a, Convex<double, 3> const& b) {
	return implementation(a, b);
}

template <>
bool GJK<double, 4>::collide(Convex<double, 4> const& a, Convex<double, 4> const& b) {
	return implementation(a, b);
}

} /* namespace Geometry */
