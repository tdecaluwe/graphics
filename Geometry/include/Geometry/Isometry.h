/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ISOMETRY_H_
#define ISOMETRY_H_

#include "Geometry/Point.h"
#include "Geometry/Translation.h"
#include "Geometry/Vector.h"

#include "Math/Dual.h"
#include "Math/Field.h"
#include "Math/Quaternion.h"

#include "boost/operators.hpp"

#include <iostream>

namespace Geometry {

/**
 * @brief Representing proper isometric transformations in euclidean space.
 */
template <class T, unsigned n>
class Isometry;

template <class T>
class Isometry<T, 3>
: private boost::multiplicative<Isometry<T, 3>, Isometry<T, 3>>
, private boost::equality_comparable<Isometry<T, 3>> {
private:
    Math::Dual<Math::Quaternion<T>> mDualQuaternion;
public:
    constexpr Isometry();
//    constexpr Isometry(Rotation<T, 3> const& rotation);
    constexpr Isometry(Translation<T, 3> const& translation);
    Point<T, 3> operator()(Point<T, 3> const& point);
    Isometry& operator*=(Isometry const& other);
    Isometry& operator/=(Isometry const& other);
    static constexpr Isometry identity();
//    friend Isometry operator*(Translation<T, 3> const& t, Rotation<T, 3> const& r) {
//    	return { r.mQuaternion, 0.5*Math::Quaternion<T>{ 0.0, t.mVector }*r.mQuaternion };
//    }
//    friend Isometry operator*(Rotation<T, 3> const& r, Translation<T, 3> const& t) {
//		return { r.mQuaternion, 0.5*Math::Quaternion<T>{ 0.0, t.mVector }*r.mQuaternion };
//	}
    friend bool operator==(Isometry const& a, Isometry const& b) {
        return a.mDualQuaternion == b.mDualQuaternion;
    }
    friend std::ostream& operator<<(std::ostream& out, Isometry const& isometry) {
        return out << isometry.mDualQuaternion;
    }
};

template <class T>
inline constexpr Isometry<T, 3>::Isometry()
: mDualQuaternion { Math::Ring<Math::Quaternion<T>>::unity(), Math::Ring<Math::Quaternion<T>>::zero() } {}

template <class T>
inline constexpr Isometry<T, 3>::Isometry(Translation<T, 3> const& t)
: mDualQuaternion { Math::Ring<Math::Quaternion<T>>::unity(), { Math::Field<T>::zero(), 0.5*t.mVector } } {}

//template <class T>
//constexpr Isometry<T, 3>::Isometry(Rotation<T, 3> const& r)
//: mDualQuaternion { r.mQuaternion, Math::Ring<Math::Quaternion<T>>::zero() } {}

template <class T>
inline Point<T, 3> Isometry<T, 3>::operator()(Point<T, 3> const& point) {
    Math::Quaternion<T> real = mDualQuaternion.real();
    Math::Quaternion<T> dual = mDualQuaternion.dual();
    Math::Quaternion<T> p = { 1.0,  p[0],  p[1],  p[2] };
    Math::Quaternion<T> r = real*p*conjugate(real);
    return Point<T, 3>( r[0],  r[1],  r[2] );
//    Math::Dual<Math::Quaternion<T>> left = mDualQuaternion;
//    Math::Dual<Math::Quaternion<T>> middle = { { Math::Ring<Math::Quaternion<T>>::unity() }, { Math::Field<T>::zero(), point - Point<T, 3>::origin() } };
//    Math::Dual<Math::Quaternion<T>> right = conjugate(mDualQuaternion);
//    auto temp = left*middle;
//    std::cout << left << "*" << middle << " = " << temp << std::endl;
//    auto temp2 = temp*right;
//    std::cout << temp << "*" << right << " = " << temp2 << std::endl;
//    Math::Quaternion<T> result = (left*middle*right).dual();
//    return Point<T, 3>::origin() + Vector<T, 3>(result.vector(0), result.vector(1), result.vector(2));

//    Math::Quaternion<T> lr = mDualQuaternion.real();
//    Math::Quaternion<T> ld = mDualQuaternion.dual();
//    Math::Quaternion<T> middle = Math::Quaternion<T>{ Math::Field<T>::zero(), point - Math::Point<T, 3>::origin() };
//    Math::Quaternion<T> rr = conjugate(mDualQuaternion.real());
//    Math::Quaternion<T> rd = conjugate(mDualQuaternion.dual());
//    return Math::Point<T, 3>::origin() + (lr*middle*rr).real().vector();
}

template <class T>
inline Isometry<T, 3>& Isometry<T, 3>::operator*=(Isometry const& other) {
    mDualQuaternion *= other.mDualQuaternion;
    return *this;
}

template <class T>
inline Isometry<T, 3>& Isometry<T, 3>::operator/=(Isometry const& other) {
    mDualQuaternion *= other.mDualQuaternion;
    return *this;
}

template <class T>
inline constexpr Isometry<T, 3> Isometry<T, 3>::identity() {
	return { };
}

} /* namespace Geometry */
#endif /* ISOMETRY_H_ */
