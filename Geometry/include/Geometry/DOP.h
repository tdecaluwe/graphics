/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef DOP_H_
#define DOP_H_

#include "Geometry/Convex.h"
#include "Geometry/Normal.h"

#include "Math/Point.h"
#include "Math/Vector.h"

#include <array>

namespace Geometry {

/**
 * @brief A discrete oriented polytope
 *
 * This is a bounding box class with a variable number of bounding hyperplanes.
 * The normals of the hyperplanes are chosen at compile time by providing
 * Normal template instantiations. Although a discrete oriented polytope is a
 * convex geometry it does not implement the Convex interface since it is only
 * intended to be used as a bounding volume.
 *
 * Keep in mind that for each Normal two hyperplanes will be constructed.
 *
 * @tparam T models the concept Field
 * @tparam n the dimensionality
 * @tparam P a list of Normal instantiations which represent the normals of the
 * bounding hyperplanes.
 *
 * @todo Add static asserts to check the dimensionality of the provided Normal
 * instantiations (through a recursive template base class).
 *
 * @todo implement Convex intersection functions.
 */
template <class T, unsigned int n, class ... P>
class DOP {
private:
    std::array<T, sizeof...(P)> mLower;
    std::array<T, sizeof...(P)> mUpper;
public:
    /**
     * @brief Construct a bounding box for any Convex object
     *
     * @param c a Convex object
     */
    constexpr DOP(Convex<T, n> const& c);
    /**
     * @brief determine whether two DOP instantiation objects intersect.
     *
     * @param a a DOP object
     * @param b another DOP object
     */
    friend bool operator&(DOP<T, n, P...> const& a, DOP<T, n, P...> const& b) {
        bool intersect;
        for (unsigned i = 0; i < n && (intersect = a.mLower[i] <= b.mLower[i] && b.mLower[i] <= a.mLower[i]); ++i);
        return intersect;
    }
    /**
     * @brief determine whether a DOP instantiation object intersects a Convex object.
     *
     * @param a a DOP object
     * @param b Convex object
     */
    friend bool operator&(DOP<T, n, P...> const& a, Convex<T, n> const& b) {
        bool result = false;
        return result;
    }
    /**
     * @brief determine whether a Convex object intersects a DOP instantiation object.
     *
     * @param a Convex object
     * @param b a DOP object
     */
    friend bool operator&(Convex<T, n> const& a, DOP<T, n, P...> const& b) {
        bool result = false;
        return result;
    }
};

template <class T, unsigned n, class ... P>
inline constexpr DOP<T, n, P...>::DOP(Convex<T, n> const& c)
: mLower{{(c.support(P::template get<T>()) - Math::Point<T, n>())*P::template get<T>()...}}
, mUpper{{(c.support(-P::template get<T>()) - Math::Point<T, n>())*P::template get<T>()...}} {}

} /* namespace Geometry */
#endif /* DOP_H_ */
