/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_TRIANGULATE_H
#define GEOMETRY_TRIANGULATE_H

#include "Geometry/Triangle.h"

#include <vector>

namespace Geometry {

// forward declarations
template <class T, unsigned n>
class Zonotope;

template <class T>
class Triangulate {};

template <class T>
class Triangulate<Zonotope<T, 3>> {
private:
    static std::vector<Triangle<T, 3>> hull(Zonotope<T, 3> const& z);
    /**
     * @brief Calculate an explicit representation of this Zonotope containing
     * all extreme points.
     *
     * @note see ftp://ftp.ifor.math.ethz.ch/pub/fukuda/paper/qpzono0210.pdf
     */
    friend std::vector<Triangle<T, 3>> hull(Zonotope<T, 3> const& z) {
        return Triangulate<Zonotope<T, 3>>::hull(z);
    }
};

} /* namespace Geometry */
#endif /* GEOMETRY_TRIANGULATE_H */
