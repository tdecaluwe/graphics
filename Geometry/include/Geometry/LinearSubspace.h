/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_LINEARSUBSPACE_H
#define GEOMETRY_LINEARSUBSPACE_H

#include "Geometry/Vector.h"

namespace Geometry {

/**
 * @brief A k-dimensional linear subspace of an n-dimensional vector space.
 *
 * Instances of this class represent an element of the Grassmannian \f$G(k,
 * \mathbb{R}^n)\f$, the set of all k-dimensional linear subspaces of
 * \f$\mathbb{R}^n)\f$.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 * @tparam k subspace dimensionality
 */
template <class T, unsigned n, unsigned k>
class LinearSubspace {
private:

public:
    static constexpr unsigned size() {
        return n*LinearSubspace<T, n - 1, k - 1>::size()/k;
    }
};

template <class T, unsigned n>
class LinearSubspace<T, n, 1> {
private:

public:
    static constexpr unsigned size() {
        return n;
    }
};

template <class T, unsigned n>
class LinearSubspace<T, n, 0> {
private:

public:
    static constexpr unsigned size() {
        return 1u;
    }
};

} /* namespace Geometry */
#endif /* GEOMETRY_LINEARSUBSPACE_H */
