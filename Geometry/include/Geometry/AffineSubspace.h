/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_AFFINESUBSPACE_H
#define GEOMETRY_AFFINESUBSPACE_H

#include "Geometry/Point.h"
#include "Geometry/Vector.h"

namespace Geometry {

/**
 * @brief A k-dimensional affine subspace of an n-dimensional affine space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 * @tparam k subspace dimensionality
 */
template <class T, unsigned n, unsigned k>
class AffineSubspace
: protected LinearSubspace<T, n + 1, k + 1> {};

/**
 * @brief A representation of a line in n-dimensional affine space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
using Line = AffineSubspace<T, n, 1>;

/**
 * @brief A representation of a plane in n-dimensional affine space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
using Plane = AffineSubspace<T, n, 2>;

} /* namespace Geometry */
#endif /* GEOMETRY_AFFINESUBSPACE_H */
