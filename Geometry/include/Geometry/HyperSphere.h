/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef HYPERSPHERE_H_
#define HYPERSPHERE_H_

#include "Geometry/Convex.h"
#include "Geometry/Vector.h"

#include "Math/Field.h"

namespace Geometry {

/**
 * @brief A hypersphere
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned int n>
class HyperSphere
: public Convex<T, n> {
private:
    T mRadius;
public:
    /**@{
     * @name Constructors
     */
    HyperSphere(T r);
    /**@}*/
    /**@{
     * @name Visitor interface
     */
    virtual void accept(typename Convex<T, n>::Reader& v) const;
    virtual void accept(typename Convex<T, n>::Writer& v);
    virtual void accept(typename Convex<T, n>::ConstReader const& v) const;
    virtual void accept(typename Convex<T, n>::ConstWriter const& v);
    /**@}*/
    virtual Vector<T, n> support(Vector<T, n> const& dir) const;
    /**@{
     * @name Accessors
     */
    T const& radius() const { return mRadius; }
    T& radius() { return mRadius; }
    /**@}*/
};

/**
 * @brief A circle
 *
 * @tparam T models the concept Field
 */
template <class T>
using Circle = HyperSphere<T, 2>;

/**
 * @brief A sphere
 *
 * @tparam T models the concept Field
 */
template <class T>
using Sphere = HyperSphere<T, 3>;

template <class T, unsigned int n>
inline HyperSphere<T, n>::HyperSphere(T r)
: mRadius { r } {}

template <class T, unsigned int n>
inline void HyperSphere<T, n>::accept(typename Convex<T, n>::Reader& v) const {
    v.visit(*this);
}

template <class T, unsigned int n>
inline void HyperSphere<T, n>::accept(typename Convex<T, n>::Writer& v) {
    v.visit(*this);
}

template <class T, unsigned int n>
inline void HyperSphere<T, n>::accept(typename Convex<T, n>::ConstReader const& v) const {
    v.visit(*this);
}

template <class T, unsigned int n>
inline void HyperSphere<T, n>::accept(typename Convex<T, n>::ConstWriter const& v) {
    v.visit(*this);
}

template <class T, unsigned int n>
inline Vector<T, n> HyperSphere<T, n>::support(Vector<T, n> const& dir) const {
    return mRadius*normalize(dir);
}

} /* namespace Geometry */
#endif /* HYPERSPHERE_H_ */
