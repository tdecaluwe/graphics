/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef ZONOTOPE_H_
#define ZONOTOPE_H_

#include "Geometry/Convex.h"
#include "Geometry/Soup.h"
#include "Geometry/Triangulate.h"
#include "Geometry/Vector.h"

#include "Float/Compare.h"

#include <boost/integer.hpp>

#include <initializer_list>
#include <vector>

namespace Geometry {

/**
 * @brief A zonotope
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
class Zonotope
: public Convex<T, n>
, private Triangulate<Zonotope<T, 3>> {
private:
    class Size;
    std::vector<Vector<T, n>> mVectors;
public:
    /**@{
     * @name Constructors
     */
    Zonotope(std::initializer_list<Vector<T, n>> vectors);
    /**@}*/
    Vector<T, n>& operator[](unsigned index);
    Vector<T, n> const& operator[](unsigned index) const;
    constexpr unsigned size() const;
    virtual Vector<T, n> support(Vector<T, n> const& dir) const;
    unsigned maximumPolytopeSize() const;
    /**@{
     * @name Visitor interface
     */
    virtual void accept(typename Convex<T, n>::Reader& v) const;
    virtual void accept(typename Convex<T, n>::Writer& v);
    virtual void accept(typename Convex<T, n>::ConstReader const& v) const;
    virtual void accept(typename Convex<T, n>::ConstWriter const& v);
    /**@}*/
};

template <class T, unsigned n>
class Zonotope<T, n>::Size {
private:
    friend Zonotope<T, n>;
    static constexpr boost::int_t<32>::fast stirling(unsigned m, unsigned k) {
        return m == k ? 1 : m == 0 ? 0 : stirling(m - 1, k - 1) - (m - 1)*stirling(m - 1, k);
    }
    static constexpr boost::int_t<32>::fast binomial(unsigned index) {
        return index < 2 ? 1 : index*binomial(index - 1);
    }
    static constexpr boost::int_t<32>::fast loop(unsigned index, unsigned start, unsigned stop) {
        return stirling(stop + 1, index + 1) + stop*(start < stop ? loop(index, start, stop - 1) : 0);
    }
    static constexpr boost::int_t<32>::fast coefficient(unsigned index) {
        return loop(index, index, n - 1);
    }
};

/**
 * @brief A zonogon
 *
 * @tparam T models the concept Field
 */
template <class T>
using Zonogon = Zonotope<T, 2>;

/**
 * @brief A zonohedron
 *
 * @tparam T models the concept Field
 */
template <class T>
using Zonohedron = Zonotope<T, 3>;

template <class T, unsigned n>
inline Zonotope<T, n>::Zonotope(std::initializer_list<Vector<T, n>> vectors)
: mVectors { vectors } {}

template <class T, unsigned n>
inline Vector<T, n>& Zonotope<T, n>::operator[](unsigned index) {
    return mVectors[index];
}

template <class T, unsigned n>
inline Vector<T, n> const& Zonotope<T, n>::operator[](unsigned index) const {
    return mVectors[index];
}

template <class T, unsigned n>
inline constexpr unsigned Zonotope<T, n>::size() const {
    return mVectors.size();
}

template <class T, unsigned n>
inline void Zonotope<T, n>::accept(typename Convex<T, n>::Reader& v) const {
    v.visit(*this);
}

template <class T, unsigned n>
inline void Zonotope<T, n>::accept(typename Convex<T, n>::Writer& v) {
    v.visit(*this);
}

template <class T, unsigned n>
inline void Zonotope<T, n>::accept(typename Convex<T, n>::ConstReader const& v) const {
    v.visit(*this);
}

template <class T, unsigned n>
inline void Zonotope<T, n>::accept(typename Convex<T, n>::ConstWriter const& v) {
    v.visit(*this);
}

template <class T, unsigned n>
inline Vector<T, n> Zonotope<T, n>::support(Vector<T, n> const& dir) const {
    Vector<T, n> result = Vector<T, n>::origin();
    for (auto iterator = mVectors.begin(); iterator != mVectors.end(); ++iterator) {
        result += Float::Compare<T>::positive(*iterator*dir) ? dir : -dir;
    }
    return result;
}

} /* namespace Geometry */
#endif /* ZONOTOPE_H_ */
