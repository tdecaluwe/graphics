/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRANSLATION_H_
#define TRANSLATION_H_

#include "Geometry/Point.h"
#include "Geometry/Vector.h"

#include "boost/operators.hpp"

#include <ostream>

namespace Geometry {

// forward declarations
template <class T, unsigned n>
class Isometry;

/**
 * @brief A translation in n-dimensional space.
 *
 * @tparam T a type modeling a real number
 * @tparam n dimensionality of the Euclidean space
 */
template <class T, unsigned n>
class Translation
: private boost::multiplicative<Translation<T, n>, Translation<T, n>>
, private boost::equality_comparable<Translation<T, n>> {
private:
	friend class Isometry<T, n>;
    Vector<T, n> mVector;
public:
    constexpr Translation();
    constexpr Translation(Vector<T, n> const& vector);
    Vector<T, n> operator()(Vector<T, n> const& vector);
    Point<T, n> operator()(Point<T, n> const& point);
    Translation& operator*=(Translation const& other);
    Translation& operator/=(Translation const& other);
    static constexpr Translation<T, n> identity();
    friend bool operator==(Translation const& a, Translation const& b) {
        return a.mVector == b.mVector;
    }
    friend std::ostream& operator<<(std::ostream& out, Translation const& translation) {
        return out << translation.mVector;
    }
};

template <class T, unsigned n>
inline constexpr Translation<T, n>::Translation()
: mVector { Vector<T, n>::origin() } {}

template <class T, unsigned n>
inline constexpr Translation<T, n>::Translation(Vector<T, n> const& vector)
: mVector { vector } {}

template <class T, unsigned n>
Vector<T, n> Translation<T, n>::operator()(Vector<T, n> const& vector) {
	return vector;
}

template <class T, unsigned n>
Point<T, n> Translation<T, n>::operator()(Point<T, n> const& point) {
	return point + mVector;
}

template <class T, unsigned n>
inline Translation<T, n>& Translation<T, n>::operator*=(Translation const& other) {
	mVector += other.mVector;
    return *this;
}

template <class T, unsigned n>
inline Translation<T, n>& Translation<T, n>::operator/=(Translation const& other) {
	mVector -= other.mVector;
    return *this;
}

template <class T, unsigned n>
inline constexpr Translation<T, n> Translation<T, n>::identity() {
    return { };
}

} /* namespace Geometry */
#endif /* TRANSLATION_H_ */
