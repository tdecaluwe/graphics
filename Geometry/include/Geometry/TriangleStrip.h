/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRIANGLESTRIP_H_
#define TRIANGLESTRIP_H_

#include <vector>
#include <utility>

#include "Geometry/Point.h"

namespace Geometry {

template <class T, unsigned n>
class TriangleStrip {
private:
	std::vector<Point<T, n>> mStrip;
public:
	TriangleStrip(Point<T, n> const& a, Point<T, n> const& b, Point<T, n> const& c);
	void add(Point<T, n> const& p);
	void add(Point<T, n>&& p);
};

template <class T, unsigned n>
TriangleStrip<T, n>::TriangleStrip(Point<T, n> const& a, Point<T, n> const& b, Point<T, n> const& c)
: mStrip{a, b, c} {}

template <class T, unsigned n>
inline void TriangleStrip<T, n>::add(Point<T, n> const& p) {
	mStrip.push_back(p);
}

template <class T, unsigned n>
inline void TriangleStrip<T, n>::add(Point<T, n>&& p) {
	mStrip.push_back(std::move(p));
}

} /* namespace Geometry */
#endif /* TRIANGLESTRIP_H_ */
