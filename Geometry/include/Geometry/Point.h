/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_POINT_H
#define GEOMETRY_POINT_H

#include "Geometry/Predicates.h"
#include "Geometry/ProjectivePoint.h"
#include "Geometry/Vector.h"

#include "Float/Compare.h"

#include "Math/Field.h"

#include <boost/operators.hpp>

#include <iosfwd>
#include <utility>

namespace Geometry {

/**
 * @brief A point in Euclidean space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality of the Euclidean space
 */
template <class T, unsigned n>
class Point
: public Memory::SubArrayType<ProjectivePoint<T, n>, T, n, Math::Field<T>::unity>::Type
, private Predicates<Point<T, n>>
, private boost::additive<Point<T, n>, Vector<T, n>>
, private boost::equality_comparable<Point<T, n>> {
public:
	using Memory::SubArrayType<ProjectivePoint<T, n>, T, n, Math::Field<T>::unity>::Type::Type;
    Point() = default;
    Point(Point const&) = default;
    /**@{
     * @brief Addition and subtraction assignment operators.
     *
     * In Euclidean space only a Vector can be added to a Point.
     *
     * @note Additional addition operators are provided by boost.
     */
    Point& operator+=(Vector<T, n> const& v);
    Point& operator-=(Vector<T, n> const& v);
    /**@}*/
    /**
     * @brief Return a zero-valued Point.
     */
    static constexpr Point origin();
    /**
     * @brief Subtraction operator.
     *
     * Subtracting a Point from a Point yields a Vector. To allow implicit
     * conversion of the parameters this operator is defined as a friend
     * function that is instantiated concurrently with the complete class.
     * It can be found for each class template instance through argument
     * dependent name lookup.
     */
    friend Vector<T, n> operator-(Point const& a, Point const& b) {
        Vector<T, n> result;
        for (unsigned i = 0; i < n; ++i) {
            result[i] = a[i] - b[i];
        }
        return result;
    }
    /**
     * @brief Equality comparison operator.
     * @note Additional (inverse) equivalence operators are provided by boost.
     */
    friend bool operator==(Point const& a, Point const& b) {
        bool result = true;
        for (unsigned i = 0; i < n; ++i) {
            result &= Float::Compare<T>::equal(a[i], b[i]);
        }
        return result;
    }
    /**
     * @brief Compute the midpoint of two Point instances.
     */
    friend Point center(Point const& a, Point const& b) {
        Point result;
        for (unsigned i = 0; i < n; ++i) {
            result[i] = (a[i] + b[i])/2;
        }
        return result;
    }
};

template <class T, unsigned n>
inline Point<T, n>& Point<T, n>::operator+=(Vector<T, n> const& v) {
    for (unsigned i = 0; i < n; ++i) {
    	this->operator[](i) += v[i];
    }
    return *this;
}

template <class T, unsigned n>
inline Point<T, n>& Point<T, n>::operator-=(Vector<T, n> const& v) {
    for (unsigned i = 0; i < n; ++i) {
    	this->operator[](i) -= v[i];
    }
    return *this;
}

template <class T, unsigned int n>
inline constexpr Point<T, n> Point<T, n>::origin() {
    return Point{ };
}

} /* namespace Geometry */
#endif /* GEOMETRY_POINT_H */
