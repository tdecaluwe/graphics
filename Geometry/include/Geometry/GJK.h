/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copyright 2013 Tom De Caluwé.
 */

#ifndef GEOMETRY_GJK_H
#define GEOMETRY_GJK_H

#include "Geometry/Convex.h"
#include "Geometry/Vector.h"

#include "Float/Compare.h"

#include "Lookup/TrailingOnes.h"

namespace Geometry {

// forward declarations
template <class, unsigned>
class Convex;

template <class T, unsigned n>
class GJK {
private:
    using Set = unsigned;
    // working sets
    Set bits, mask;
    // working set for storing the current closest simplex
    Vector<T, n> set[n + 1];
    // cache calculated barycentric coordinates
    T barycs[2 << n][n + 1];
    // cache calculated dot products
    T dots[n + 1][n + 1];
    static constexpr Set vacant(Set index);
    static constexpr Set min(Set index);
    void update(unsigned spot);
    bool implementation(Convex<T, n> const& a, Convex<T, n> const& b);
public:
    bool collide(Convex<T, n> const& a, Convex<T, n> const& b);
};

template <>
bool GJK<float, 1>::collide(Convex<float, 1> const& a, Convex<float, 1> const& b);

template <>
bool GJK<float, 2>::collide(Convex<float, 2> const& a, Convex<float, 2> const& b);

template <>
bool GJK<float, 3>::collide(Convex<float, 3> const& a, Convex<float, 3> const& b);

template <>
bool GJK<float, 4>::collide(Convex<float, 4> const& a, Convex<float, 4> const& b);

template <>
bool GJK<double, 1>::collide(Convex<double, 1> const& a, Convex<double, 1> const& b);

template <>
bool GJK<double, 2>::collide(Convex<double, 2> const& a, Convex<double, 2> const& b);

template <>
bool GJK<double, 3>::collide(Convex<double, 3> const& a, Convex<double, 3> const& b);

template <>
bool GJK<double, 4>::collide(Convex<double, 4> const& a, Convex<double, 4> const& b);

template <class T, unsigned n>
inline constexpr typename GJK<T, n>::Set GJK<T, n>::vacant(Set index) {
    return Lookup::TrailingOnes<unsigned char, 2 << n>::get(index);
}

template <class T, unsigned n>
inline constexpr typename GJK<T, n>::Set GJK<T, n>::min(Set index) {
    return Lookup::TrailingOnes<unsigned char, 2 << n>::get(index - 1);
}

template <class T, unsigned n>
inline void GJK<T, n>::update(unsigned spot) {
    dots[spot][spot] = set[spot]*set[spot];
    for (Set todo = bits; todo != 0; todo &= (todo - 1) & bits) {
        dots[spot][min(todo)] = set[spot]*set[min(todo)];
        dots[min(todo)][spot] = set[spot]*set[min(todo)];
    }
    // update the singleton subset for set[spot]
    barycs[mask][spot] = 1;
    // update all active subsets including set[spot]
    for (Set subset = -bits & bits; subset != 0; subset = (subset - bits) & bits) {
        for (Set todo = subset | mask;  todo != 0; todo &= (todo - 1) & (subset | mask)) {
            barycs[subset | mask][min(todo)] = 0;
            for (Set left = (subset | mask) ^ (-todo & todo); left != 0; left &= (left - 1) & ((subset | mask) ^ (-todo & todo))) {
                barycs[subset | mask][min(todo)] += barycs[(subset | mask) ^ (-todo & todo)][min(left)]*(dots[min(left)][min((subset | mask) ^ (-todo & todo))] - dots[min(left)][min(todo)]);
            }
        }
    }
}

template <class T, unsigned n>
inline bool GJK<T, n>::implementation(Convex<T, n> const& a, Convex<T, n> const& b) {
    // indicate which points in the working set are active
    bits = 0;
    // initial distance
    double distance = 0;
    // initial guess
    double total;
    Vector<T, n> closest = Vector<T, n>::origin();
    //Vector<double, n> closest;
    bool noSeparatingPlaneFound = true;
    bool originInSimplex = false;
    do {
        // pick a free spot in the working set
        unsigned spot = vacant(bits);
        // compute the new simplex point
        set[spot] = a.support(-closest) - b.support(closest);
        noSeparatingPlaneFound = Float::Compare<T>::sign(set[spot]*closest);
        if (noSeparatingPlaneFound) {
            // update dot products and barycentric determinants
            mask = 1 << spot;
            update(spot);
            // loop over all unions of subsets of the previous working set and
            // set[spot] or equivalently all subsets of the current working set
            // containing set[spot]
            Set subset, tryset = 0;
            bool result;
            do {
                subset = tryset;
                result = true;
                for (Set todo = bits | mask; todo != 0 && result; todo &= (todo - 1) & (bits | mask)) {
                    // The barycentric coordinate with respect to the Vector at
                    // index (-todo & todo) should be negative for the superset
                    // (subset | (-todo & todo)) of subset and positive for the
                    // current set subset. The first operand in the following
                    // xor statement flips the sign bit of the barycentric
                    // coordinate in the latter case.
                    result = (-todo & todo & (subset | mask))^Float::Compare<T>::sign(barycs[subset | mask | (-todo & todo)][min(todo)]);
                }
            } while (!result && (tryset = (subset - bits) & bits) != 0);
            if (result) {
                bits = subset | mask;
                // update closest
                total = 0.0;
                closest = Math::Vector<T, n>();
                for (Set todo = bits; todo != 0; todo &= (todo - 1) & bits) {
                    total += barycs[bits][min(todo)];
                    closest += set[min(todo)]*barycs[bits][min(todo)];
                }
                originInSimplex = closest*closest < 0.0001*0.0001*total*total;
            } else {
                noSeparatingPlaneFound = false;
            }
        }
    } while (noSeparatingPlaneFound && !originInSimplex);
    return originInSimplex;
}

template <class T, unsigned n>
inline bool GJK<T, n>::collide(Convex<T, n> const& a, Convex<T, n> const& b) {
    return implementation(a, b);
}

} /* namespace Geometry */
#endif /* GEOMETRY_GJK_H */
