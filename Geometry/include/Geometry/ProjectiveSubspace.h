/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_PROJECTIVESUBSPACE_H
#define GEOMETRY_PROJECTIVESUBSPACE_H

#include "Geometry/ProjectivePoint.h"
#include "Geometry/Vector.h"

namespace Geometry {

/**
 * @brief A k-dimensional projective subspace of an n-dimensional projective
 * space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 * @tparam k subspace dimensionality
 */
template <class T, unsigned n, unsigned k>
class ProjectiveSubspace
: protected LinearSubspace<T, n + 1, k + 1> {
private:

public:
    static constexpr unsigned size() {
        return n*LinearSubspace<T, n + 1, k + 1>::size()/k;
    }
};

template <class T>
class ProjectiveSubspace<T, 3, 2> {
private:
    ProjectiveSubspace() = default;
public:
    static constexpr unsigned size() {
        return 3*LinearSubspace<T, 3, 2>::size()/2;
    }
};

/**
 * @brief A representation of a line in n-dimensional projective space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
using ProjectiveLine = ProjectiveSubspace<T, n, 1>;

/**
 * @brief A representation of a plane in n-dimensional projective space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
using ProjectivePlane = ProjectiveSubspace<T, n, 2>;

/**
 * @brief A representation of a hyperplane in n-dimensional projective space.
 *
 * @tparam T models the concept Field
 * @tparam n dimensionality
 */
template <class T, unsigned n>
using ProjectiveHyperPlane = ProjectiveSubspace<T, n, 2>;

} /* namespace Geometry */
#endif /* GEOMETRY_PROJECTIVESUBSPACE_H */
