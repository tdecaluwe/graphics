/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef NODE_H_
#define NODE_H_

#include "Visit/Visitor.h"

namespace Geometry {

// forward declarations
template <class BV, class ... Mixins>
class BVH;
template <class BV, class ... Mixins>
class Branch;
template <class BV, class ... Mixins>
class Leaf;

/**
 * @brief A node in a boundary volume hierarchy.
 *
 * @tparam BV bounding volume
 */
template <class BV, class ... Mixins>
class Node
: public Mixins ... {
protected:
	using Reader = Visit::Visitor<BVH<BV, Mixins ...> const, Branch<BV, Mixins ...> const>;
	using Writer = Visit::Visitor<BVH<BV, Mixins ...>, Branch<BV, Mixins ...>>;
public:
	virtual ~Node() = default;
	Node(Node const&) = delete;
	Node& operator=(Node const&) = delete;
	virtual void accept(Reader&) const = 0;
	virtual void accept(Writer&) = 0;
	virtual void accept(Reader const&) const = 0;
	virtual void accept(Writer const&) = 0;
};

} /* namespace Geometry */
#endif /* NODE_H_ */
