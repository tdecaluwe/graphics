/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SUBTREE_H_
#define SUBTREE_H_

#include "Geometry/Node.h"
#include "Geometry/DOP.h"

#include <array>
#include <memory>

namespace Geometry {

/**
 * @brief An unrolled section of a binary bounding volume hierarchy. Leaf
 * pointers are provided at all incomplete internal nodes.
 *
 * @tparam BV bounding volume
 * @tparam size the number of Node objects contained in the Subtree
 */
template <class BV, unsigned size, class ... Mixins>
class Subtree;

template <class T, unsigned int n, class ... P, unsigned size, class ... Mixins>
class Subtree<DOP<T, n, P...>, size, Mixins ...>
: public Node<DOP<T, n, P...>, Mixins ...> {
private:
	class InnerNode;
	std::array<InnerNode, size> mInner;
	std::array<std::unique_ptr<Node<DOP<T, n, P...>>>, size + 1> mOuter;
public:
	Subtree();
	virtual ~Subtree() = default;
	Subtree(Subtree const&) = delete;
	Subtree& operator=(Subtree const&) = delete;
	Subtree(Subtree&&) = default;
	Subtree& operator=(Subtree&&) = default;
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader& r) const { mInner[0].accept(r); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer& w) { mInner[0].accept(w); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader const& r) const { mInner[0].accept(r); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer const& w) { mInner[0].accept(w); }
};

template <class T, unsigned int n, class ... P, unsigned size, class ... Mixins>
class Subtree<DOP<T, n, P...>, size, Mixins ...>::InnerNode {
private:

public:
	void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader& r) const { r.visit(*this); }
	void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer& w) { w.visit(*this); }
	void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader const& r) const { r.visit(*this); }
	void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer const& w) { w.visit(*this); }
};

template <unsigned ... levels>
class Traverser {
private:
	class Iterator;
	class ReverseIterator;
	static constexpr std::array<unsigned, sizeof...(levels)> sLevels { levels ... };
	template <class T>
	static constexpr T find(T i, unsigned l) {
		return i + 1 == T::end() ? (sLevels[(i + 1).index()] == l ? i + 1 : find(i + 1, l)) : T::end();
	}
	template <class T>
	static constexpr T findDeep(T i, unsigned l) {
		return i + 1 == T::end() ? (sLevels[(i + 1).index()] > l ? i + 1 : findDeep(i + 1, l)) : T::end();
	}
	template <class T>
	static constexpr T findShallow(T i, unsigned l) {
		return i + 1 == T::end() ? (sLevels[(i + 1).index()] < l ? i + 1 : findLeft(i + 1, l)) : T::end();
	}
	template <class T>
	static constexpr T findAncestor(T i) {
		return findShallow(i, sLevels[i.index()]);
	}
	template <class T>
	static constexpr T findChild(T i) {
		return find(i, sLevels[i.index()] + 1);
	}
	template <class T>
	static constexpr T findRoot() {
		return sLevels[T::begin().index()] == 0u ? T::begin() : find(T::begin(), 0u);
	}
public:
};

template <unsigned ... levels>
class Traverser<levels ...>::Iterator {
private:
	unsigned mPosition;
	constexpr Iterator(unsigned p): mPosition(p) {}
public:
	constexpr Iterator(ReverseIterator& i);
	constexpr unsigned index() const { return mPosition; }
	static constexpr Iterator begin() { return Iterator(0u); }
	static constexpr Iterator end() { return Iterator(sizeof...(levels)); }
	/*
	 * @todo Replace arithmetic operators with boost operators versions
	 * once arithmetic assignment operators can be declared constexpr
	 * (planned for c++14).
	 */
	friend constexpr Iterator operator+(Iterator i, unsigned offset) {
		return Iterator(i.mPosition + offset);
	}
	friend constexpr Iterator operator-(Iterator i, unsigned offset) {
		return Iterator(i.mPosition - offset);
	}
};

template <unsigned ... levels>
class Traverser<levels ...>::ReverseIterator {
private:
	unsigned mPosition;
	constexpr ReverseIterator(unsigned p): mPosition(p) {}
public:
	constexpr ReverseIterator(Iterator& i);
	constexpr unsigned index() const { return sizeof...(levels) - 1 - mPosition; }
	static constexpr ReverseIterator begin() { return ReverseIterator(0u); }
	static constexpr ReverseIterator end() { return ReverseIterator(sizeof...(levels)); }
	/*
	 * @todo Replace arithmetic operators with boost operators versions
	 * once arithmetic assignment operators can be declared constexpr
	 * (planned for c++14).
	 */
	friend constexpr ReverseIterator operator+(ReverseIterator i, unsigned offset) {
		return ReverseIterator(i.mPosition + offset);
	}
	friend constexpr ReverseIterator operator-(ReverseIterator i, unsigned offset) {
		return ReverseIterator(i.mPosition - offset);
	}
};

template <unsigned ... levels>
inline constexpr Traverser<levels ...>::Iterator::Iterator(ReverseIterator& i)
: mPosition(sizeof...(levels) - 1 - i.index()) {}

template <unsigned ... levels>
inline constexpr Traverser<levels ...>::ReverseIterator::ReverseIterator(Iterator& i)
: mPosition(sizeof...(levels) - 1 - i.index()) {}

} /* namespace Geometry */
#endif /* SUBTREE_H_ */
