/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef GEOMETRY_PREDICATES_H
#define GEOMETRY_PREDICATES_H

#include "Math/Predicates.h"

namespace Geometry {

// forward declarations
template <class T, unsigned n>
class Point;

template <class T>
class Predicates
: Math::Predicates<T> {};

/**
 * @brief Some predicates on Point objects.
 */
template <class T>
class Predicates<Point<T, 2>> {
	/**
	 * @brief Return true if the points are provided in counter clockwise
	 * order.
	 *
	 * @note This predicate differs from the opposite of the clockwise()
	 * predicate when all three Point objects are collinear since these are
	 * considered to be neither counter clockwise nor clockwise.
	 */
    friend constexpr bool counterClockwise(Point<T, 2> const& a, Point<T, 2> const& b, Point<T, 2> const& c) {
        return counterClockwise(b - a, c - b);
    }
	/**
	 * @brief Return true if the points are provided in clockwise order.
	 *
	 * @note This predicate differs from the opposite of the counterClockwise()
	 * predicate when all three Point objects are collinear since these are
	 * considered to be neither clockwise nor counter clockwise.
	 */
    friend constexpr bool clockwise(Point<T, 2> const& a, Point<T, 2> const& b, Point<T, 2> const& c) {
        return clockwise(b - a, c - b);
    }
};

} /* namespace Math */
#endif /* GEOMETRY_PREDICATES_H */
