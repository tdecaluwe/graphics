/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Geometry/Point.h"
#include "Geometry/SurfaceNormal.h"

#include <array>

namespace Geometry {

template <class T, unsigned n>
class Triangle
: private SurfaceNormal<Triangle<T, n>> {
private:
    std::array<Point<T, n>, 3> mPoints;
public:
    constexpr Triangle(Point<T, n> a, Point<T, n> b, Point<T, n> c);
    Point<T, n> const& operator[](unsigned index) const;
    Point<T, n>& operator[](unsigned index);
};

template <class T, unsigned n>
inline constexpr Triangle<T, n>::Triangle(Point<T, n> a, Point<T, n> b, Point<T, n> c)
: mPoints { a, b, c } {}

template <class T, unsigned n>
inline Point<T, n> const& Triangle<T, n>::operator[](unsigned index) const {
	return mPoints[index];
}

template <class T, unsigned n>
inline Point<T, n>& Triangle<T, n>::operator[](unsigned index) {
	return mPoints[index];
}

} /* namespace Geometry */
#endif /* TRIANGLE_H_ */
