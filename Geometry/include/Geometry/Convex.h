/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef CONVEX_H_
#define CONVEX_H_

#include "Geometry/Vector.h"
#include "Geometry/Point.h"

#include "Visit/ConstVisitor.h"
#include "Visit/Visitor.h"

namespace Geometry {

// forward declarations
template <class T, unsigned n>
class HyperSphere;
template <class T, unsigned n>
class Zonotope;

/**
 * @brief An abstract base class for bounded convex shapes
 *
 * The restriction to bounded shapes is needed because the support() map can not
 * be expressed in non-homogenuous coordinates for unbounded shapes.
 *
 * @tparam n dimensionality of the Euclidean space
 */
template <class T, unsigned n>
class Convex {
protected:
    using Reader = Visit::Visitor<HyperSphere<T, n> const, Zonotope<T, n> const>;
    using Writer = Visit::Visitor<HyperSphere<T, n>, Zonotope<T, n>>;
    using ConstReader = Visit::ConstVisitor<HyperSphere<T, n> const, Zonotope<T, n> const>;
    using ConstWriter = Visit::ConstVisitor<HyperSphere<T, n>, Zonotope<T, n>>;
public:
    /**@{
     * @name Constructors
     */
    Convex() = default;
    Convex(Convex const&) = default;
    Convex(Convex&&) = default;
    /**@}*/
    virtual ~Convex() = default;
    Convex& operator=(Convex const&) = default;
    Convex& operator=(Convex&&) = default;
    /**@{
     * @name Visitor interface
     */
    virtual void accept(typename Convex<T, n>::Reader&) const = 0;
    virtual void accept(typename Convex<T, n>::Writer&) = 0;
    virtual void accept(typename Convex<T, n>::ConstReader const&) const = 0;
    virtual void accept(typename Convex<T, n>::ConstWriter const&) = 0;
    /**@}*/
    /**
     * @brief Map a Math::Vector direction to its associated support point.
     *
     * The support map of a convex object is the function that maps each
     * direction vector \f$v\f$ to a boundary point \f$x\f$ such that the
     * condition \f$\vec{xy}\cdot\vec{v}\geq 0\f$ holds for all \f$y\f$ in the
     * convex set.
     * 
     * @param direction a direction vector
     */
    virtual Vector<T, n> support(Vector<T, n> const& direction) const = 0;
};

} /* namespace Geometry */
#endif /* CONVEX_H_ */
