/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef BRANCH_H_
#define BRANCH_H_

#include "Geometry/DOP.h"
#include "Geometry/Node.h"

namespace Geometry {

template <class BV, class ... Mixins>
class Branch;

/**
 * @brief An inner node in a boundary volume hierarchy.
 *
 * @tparam BV bounding volume
 */
template <class T, unsigned int n, class ... P, class ... Mixins>
class Branch<DOP<T, n, P...>, Mixins ...>
: public Node<DOP<T, n, P...>, Mixins ...> {
private:
public:
	Branch();
	virtual ~Branch() = default;
	Branch(Branch const&) = delete;
	Branch& operator=(Branch const&) = delete;
	Branch(Branch&&) = default;
	Branch& operator=(Branch&&) = default;
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader& r) const { r.visit(*this); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer& w) { w.visit(*this); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Reader const& r) const { r.visit(*this); }
	virtual void accept(typename Node<DOP<T, n, P...>, Mixins ...>::Writer const& w) { w.visit(*this); }
};

} /* namespace Geometry */
#endif /* BRANCH_H_ */
