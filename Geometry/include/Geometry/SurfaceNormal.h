/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef SURFACENORMAL_H_
#define SURFACENORMAL_H_

#include "Geometry/Vector.h"

namespace Geometry {

// forward declarations
template <class T, unsigned n>
class Triangle;

template <class T>
class SurfaceNormal {};

template <class T>
class SurfaceNormal<Triangle<T, 3>> {
public:
	friend Vector<T, 3> normal(Triangle<T, 3> const& triangle) {
		return (triangle[2] - triangle[1])%(triangle[1] - triangle[0]);
	}
};

} /* namespace Geometry */
#endif /* SURFACENORMAL_H_ */
