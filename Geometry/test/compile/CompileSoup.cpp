#include "Geometry/Soup.h"
#include "Geometry/Triangle.h"

extern Geometry::Triangle<float, 3u> a, b, c;

void function() {
    {
        Geometry::Soup<float> soup = { a, b, c };
    }
    {
        Geometry::Soup<float> soup { a, b, c };
    }
}
