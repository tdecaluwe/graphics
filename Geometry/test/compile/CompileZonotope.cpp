#include "Geometry/Zonotope.h"

#include "Math/Vector.h"

extern Math::Vector<float, 3u> a, b, c;

void function() {
    {
        Geometry::Zonotope<float, 3u> zonotope = { { a, b, c } };
    }
    {
        Geometry::Zonotope<float, 3u> zonotope { { a, b, c } };
    }
}
