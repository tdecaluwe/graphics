#include "Geometry/Translation.h"

#include "Geometry/Point.h"
#include "Geometry/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Geometry {

BOOST_AUTO_TEST_SUITE(Translations)

using Types = list<float, double>;

BOOST_AUTO_TEST_SUITE(Planar)

BOOST_AUTO_TEST_CASE_TEMPLATE(Composition, T, Types) {
	Translation<T, 2> a = { Vector<T, 2>{ 4.0, 5.0 } };
	Translation<T, 2> b = { Vector<T, 2>{ 2.0, 3.0 } };
	BOOST_CHECK_EQUAL(a*b, (Vector<T, 2>{ 6.0, 8.0 }));
	BOOST_CHECK_EQUAL(a/b, (Vector<T, 2>{ 2.0, 2.0 }));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(IdentityVector, T, Types) {
    Vector<T, 2> v { 1.0, 0.0 };
    Vector<T, 2> w { 0.0, 1.0 };
    Translation<T, 2> identity = Translation<T, 2>::identity();
    BOOST_CHECK_EQUAL(identity(v), v);
    BOOST_CHECK_EQUAL(identity(w), w);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(IdentityPoint, T, Types) {
    Point<T, 2> p { 1.0, 0.0 };
    Point<T, 2> q { 0.0, 1.0 };
    Translation<T, 2> identity = Translation<T, 2>::identity();
    BOOST_CHECK_EQUAL(identity(p), p);
    BOOST_CHECK_EQUAL(identity(q), q);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(TranslateVector, T, Types) {
	Translation<T, 2> t = { Vector<T, 2>{ 4.0, 5.0 } };
    Vector<T, 2> v { 1.0, 2.0 };
    BOOST_CHECK_EQUAL(t(v), v);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(TranslatePoint, T, Types) {
	Translation<T, 2> t = { Vector<T, 2>{ 4.0, 5.0 } };
    Point<T, 2> p { 1.0, 2.0 };
    BOOST_CHECK_EQUAL(t(p), (Point<T, 2>{ 5.0, 7.0 }));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(Spatial)

BOOST_AUTO_TEST_CASE_TEMPLATE(Composition, T, Types) {
	Translation<T, 3> a = { Vector<T, 3>{ 4.0, 5.0, 6.0 } };
	Translation<T, 3> b = { Vector<T, 3>{ 2.0, 3.0, 4.0 } };
	BOOST_CHECK_EQUAL(a*b, (Vector<T, 3>{ 6.0, 8.0, 10.0 }));
	BOOST_CHECK_EQUAL(a/b, (Vector<T, 3>{ 2.0, 2.0, 2.0 }));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(IdentityVector, T, Types) {
    Vector<T, 3> v { 1.0, 0.0, 0.0 };
    Vector<T, 3> w { 0.0, 1.0, 0.0 };
    Vector<T, 3> x { 0.0, 0.0, 1.0 };
    Translation<T, 3> identity = Translation<T, 3>::identity();
    BOOST_CHECK_EQUAL(identity(v), v);
    BOOST_CHECK_EQUAL(identity(w), w);
    BOOST_CHECK_EQUAL(identity(x), x);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(IdentityPoint, T, Types) {
    Point<T, 3> p { 1.0, 0.0, 0.0 };
    Point<T, 3> q { 0.0, 1.0, 0.0 };
    Point<T, 3> r { 0.0, 0.0, 1.0 };
    Translation<T, 3> identity = Translation<T, 3>::identity();
    BOOST_CHECK_EQUAL(identity(p), p);
    BOOST_CHECK_EQUAL(identity(q), q);
    BOOST_CHECK_EQUAL(identity(r), r);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(TransformVector, T, Types) {
	Translation<T, 3> t = { Vector<T, 3>{ 4.0, 5.0, 6.0 } };
    Vector<T, 3> v { 1.0, 2.0, 3.0 };
    BOOST_CHECK_EQUAL(t(v), v);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(TransformPoint, T, Types) {
	Translation<T, 3> t = { Vector<T, 3>{ 4.0, 5.0, 6.0 } };
    Point<T, 3> p { 1.0, 2.0, 3.0 };
    BOOST_CHECK_EQUAL(t(p), (Point<T, 3>{ 5.0, 7.0, 9.0 }));
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
