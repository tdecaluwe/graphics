#include "Geometry/HyperSphere.h"
#include "Geometry/Soup.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Geometry {

BOOST_AUTO_TEST_SUITE(TestSoup)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(SphereTriangulation, Type, Types) {

}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
