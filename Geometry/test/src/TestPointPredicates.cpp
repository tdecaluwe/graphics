#include "Geometry/Point.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

using Geometry::Point;

BOOST_AUTO_TEST_SUITE(PointPredicates)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Clockwise, T, Types) {
    Point<T, 2> a { 0.0, 0.0 };
    Point<T, 2> b { 1.0, 0.0 };
    Point<T, 2> c { 1.0, 1.0 };
    BOOST_CHECK(clockwise(c, b, a));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CounterClockwise, T, Types) {
    Point<T, 2> a { 0.0, 0.0 };
    Point<T, 2> b { 1.0, 0.0 };
    Point<T, 2> c { 1.0, 1.0 };
    BOOST_CHECK(counterClockwise(a, b, c));
}

BOOST_AUTO_TEST_SUITE_END()
