#include "Geometry/Zonotope.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Geometry {

BOOST_AUTO_TEST_SUITE(TestZonotope)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(SizeSamples, Type, Types) {
    {
        Vector<Type, 1> a, b, c;
        BOOST_CHECK_EQUAL((Zonotope<Type, 1>{ a }.maximumPolytopeSize()), 2);
        BOOST_CHECK_EQUAL((Zonotope<Type, 1>{ a, b }.maximumPolytopeSize()), 2);
        BOOST_CHECK_EQUAL((Zonotope<Type, 1>{ a, b, c }.maximumPolytopeSize()), 2);
    }
    {
        Vector<Type, 2> a, b, c, d;
        BOOST_CHECK_EQUAL((Zonotope<Type, 2>{ a }.maximumPolytopeSize()), 2);
        BOOST_CHECK_EQUAL((Zonotope<Type, 2>{ a, b }.maximumPolytopeSize()), 4);
        BOOST_CHECK_EQUAL((Zonotope<Type, 2>{ a, b, c }.maximumPolytopeSize()), 6);
        BOOST_CHECK_EQUAL((Zonotope<Type, 2>{ a, b, c, d }.maximumPolytopeSize()), 8);
    }
    {
        Vector<Type, 3> a, b, c, d, e;
        BOOST_CHECK_EQUAL((Zonotope<Type, 3>{ a }.maximumPolytopeSize()), 2);
        BOOST_CHECK_EQUAL((Zonotope<Type, 3>{ a, b }.maximumPolytopeSize()), 4);
        BOOST_CHECK_EQUAL((Zonotope<Type, 3>{ a, b, c }.maximumPolytopeSize()), 8);
        BOOST_CHECK_EQUAL((Zonotope<Type, 3>{ a, b, c, d }.maximumPolytopeSize()), 14);
        BOOST_CHECK_EQUAL((Zonotope<Type, 3>{ a, b, c, d, e }.maximumPolytopeSize()), 22);
    }
    {
        Vector<Type, 4> a, b, c, d, e, f;
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a }.maximumPolytopeSize()), 2);
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a, b }.maximumPolytopeSize()), 4);
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a, b, c }.maximumPolytopeSize()), 8);
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a, b, c, d }.maximumPolytopeSize()), 16);
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a, b, c, d, e }.maximumPolytopeSize()), 30);
        BOOST_CHECK_EQUAL((Zonotope<Type, 4>{ a, b, c, d, e, f }.maximumPolytopeSize()), 52);
    }
}

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
