#include "Geometry/Point.h"
#include "Geometry/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

#include <utility>

using Geometry::Point;
using Geometry::Vector;

using boost::mpl::list;

BOOST_AUTO_TEST_SUITE(Points)

using Types = list<float, double>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Constructors, Type, Types) {
    Point<Type, 3> vector { 1.0, 2.0, 3.0 };
    BOOST_CHECK_EQUAL(vector[0], 1.0);
    BOOST_CHECK_EQUAL(vector[1], 2.0);
    BOOST_CHECK_EQUAL(vector[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(DefaultInitialization, Type, Types) {
    Point<Type, 3> vector { 1.0, 2.0, 3.0 };
    Point<Type, 3>* pointer = new (&vector) Point<Type, 3>;
    BOOST_CHECK_EQUAL(vector[0], 1.0);
    BOOST_CHECK_EQUAL(vector[1], 2.0);
    BOOST_CHECK_EQUAL(vector[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(ValueInitialization, Type, Types) {
    Point<Type, 3> vector { 1.0, 2.0, 3.0 };
    Point<Type, 3>* pointer = new (&vector) Point<Type, 3> {};
    BOOST_CHECK_EQUAL(vector[0], 0.0);
    BOOST_CHECK_EQUAL(vector[1], 0.0);
    BOOST_CHECK_EQUAL(vector[2], 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Origin, Type, Types) {
    Point<Type, 3> vector = Point<Type, 3>::origin();
    BOOST_CHECK_EQUAL(vector[0], 0.0);
    BOOST_CHECK_EQUAL(vector[1], 0.0);
    BOOST_CHECK_EQUAL(vector[2], 0.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(CopyConstructor, Type, Types) {
    Point<Type, 3> vector { 1.0, 2.0, 3.0 };
    Point<Type, 3> copy { vector };
    BOOST_CHECK_EQUAL(copy[0], 1.0);
    BOOST_CHECK_EQUAL(copy[1], 2.0);
    BOOST_CHECK_EQUAL(copy[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Assignment, Type, Types) {
    Point<Type, 3> vector { 1.0, 2.0, 3.0 };
    Point<Type, 3> other;
    other = vector;
    BOOST_CHECK_EQUAL(other[0], 1.0);
    BOOST_CHECK_EQUAL(other[1], 2.0);
    BOOST_CHECK_EQUAL(other[2], 3.0);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Comparable, Type, Types) {
    Point<Type, 3> a { 1.0, 1.0, 1.0 };
    Point<Type, 3> b { 1.0, 1.0, 1.0 };
    Point<Type, 3> xy { 1.0, 1.0, 0.0 };
    Point<Type, 3> xz { 1.0, 0.0, 1.0 };
    Point<Type, 3> yz { 0.0, 1.0, 1.0 };
    BOOST_CHECK_EQUAL(a, a);
    BOOST_CHECK_EQUAL(b, b);
    BOOST_CHECK_EQUAL(a, b);
    BOOST_CHECK_NE(a, xy);
    BOOST_CHECK_NE(a, xz);
    BOOST_CHECK_NE(a, yz);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Addition, Type, Types) {
    Point<Type, 3> from { 6.0, 2.0, -2.0 };
    Vector<Type, 3> offset { -1.0, 1.0, 3.0 };
    Point<Type, 3> to { 5.0, 3.0, 1.0 };
    BOOST_CHECK_EQUAL(from + offset, to);
    BOOST_CHECK_EQUAL(from += offset, to);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Subtraction, Type, Types) {
    Point<Type, 3> from { 6.0, 2.0, -2.0 };
    Vector<Type, 3> offset { -1.0, 1.0, 3.0 };
    Point<Type, 3> to { 7.0, 1.0, -5.0 };
    BOOST_CHECK_EQUAL(from - offset, to);
    BOOST_CHECK_EQUAL(from -= offset, to);
}

BOOST_AUTO_TEST_SUITE_END()
