#include "Geometry/Isometry.h"
#include "Geometry/Point.h"
#include "Geometry/Translation.h"
#include "Geometry/Vector.h"

#include <boost/mpl/list.hpp>
#include <boost/test/unit_test.hpp>

using boost::mpl::list;

namespace Geometry {

BOOST_AUTO_TEST_SUITE(Isometries)

using Types = list<float, double>;

BOOST_AUTO_TEST_SUITE(Spatial)

/*BOOST_AUTO_TEST_CASE_TEMPLATE(Composition, T, Types) {
	Isometry<T, 3> a = { Vector<T, 3>{ 4.0, 5.0, 6.0 } };
	Isometry<T, 3> b = { Vector<T, 3>{ 2.0, 3.0, 4.0 } };
	BOOST_CHECK_EQUAL(a*b, (Vector<T, 3>{ 6.0, 8.0, 10.0 }));
	BOOST_CHECK_EQUAL(a/b, (Vector<T, 3>{ 2.0, 2.0, 2.0 }));
}*/

BOOST_AUTO_TEST_CASE_TEMPLATE(Identity, T, Types) {
    Point<T, 3> p { 1.0, 0.0, 0.0 };
    Point<T, 3> q { 0.0, 1.0, 0.0 };
    Point<T, 3> r { 0.0, 0.0, 1.0 };
    Isometry<T, 3> identity = Isometry<T, 3>::identity();
    BOOST_CHECK_EQUAL(identity(p), p);
    BOOST_CHECK_EQUAL(identity(q), q);
    BOOST_CHECK_EQUAL(identity(r), r);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(Translate, T, Types) {
    Translation<T, 3> t = { Vector<T, 3>{ 4.0, 5.0, 6.0 } };
    Isometry<T, 3> i = t;
    Point<T, 3> p { 1.0, 2.0, 3.0 };
    BOOST_CHECK_EQUAL(i(p), (Point<T, 3>{ 5.0, 7.0, 9.0 }));
}

//BOOST_AUTO_TEST_CASE_TEMPLATE(Rotate, T, Types) {
//    Rotation<T, 3> rx = Rotation<T, 3>::x(3.141592653589793/2.0);
//    Isometry<T, 3> i = rx;
//    Point<T, 3> p { 1.0, 2.0, 3.0 };
//    BOOST_CHECK_EQUAL(i(p), (Point<T, 3>{ 1.0, -3.0, 2.0 }));
//}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
