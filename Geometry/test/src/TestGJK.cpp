#include "Geometry/GJK.h"

#include "Geometry/HyperSphere.h"
#include "Geometry/Point.h"

#include <boost/mpl/list.hpp>
#include <boost/mpl/pair.hpp>
#include <boost/mpl/size_t.hpp>
#include <boost/test/unit_test.hpp>

#include <vector>
#include <limits>

using boost::mpl::list;
using boost::mpl::pair;

namespace Geometry {

BOOST_AUTO_TEST_SUITE(TestGJK)

using Types = list
< pair<float, boost::mpl::size_t<1>>
, pair<float, boost::mpl::size_t<2>>
, pair<float, boost::mpl::size_t<3>>
, pair<float, boost::mpl::size_t<4>>
, pair<double, boost::mpl::size_t<1>>
, pair<double, boost::mpl::size_t<2>>
, pair<double, boost::mpl::size_t<3>>
, pair<double, boost::mpl::size_t<4>>>;

/*BOOST_AUTO_TEST_CASE_TEMPLATE(ApproachingSpheres, Type, Types) {
    GJK<typename Type::first, Type::second::value> gjk;
    Math::Point<typename Type::first, Type::second::value> p, q;
    p[0] = 1.0;
    q[0] = -2.0;
    HyperSphere<typename Type::first, Type::second::value> a(p, 1.0);
    HyperSphere<typename Type::first, Type::second::value> b(q, 1.0);
    bool sign = true;
	for (unsigned int i = 0; i < std::numeric_limits<typename Type::first>::digits - 3; ++i) {
        a.center()[0] = -a.center()[0]/2.0;
        BOOST_CHECK_EQUAL(gjk.collide(a, b), sign);
        sign ^= true;
    }
}

BOOST_AUTO_TEST_CASE_TEMPLATE(RandomSphereCollisions, Type, Types) {
    GJK<typename Type::first, Type::second::value> gjk;
    unsigned number = 100;
    std::gamma_distribution<typename Type::first> gamma(1.0, 42.0);
    std::uniform_real_distribution<typename Type::first> uniform(-42.0, 42.0);
    std::mt19937 twister(42);
    std::vector<HyperSphere<typename Type::first, Type::second::value>> circles;
    circles.reserve(number);
    for (unsigned int i = 0; i < number; ++i) {
        Math::Point<typename Type::first, Type::second::value> center;
        for (unsigned int i = 0; i < Type::second::value; ++i) {
            center[i] = uniform(twister);
        }
        circles.emplace_back(center, gamma(twister));
    }
    for (typename std::vector<HyperSphere<typename Type::first, Type::second::value>>::const_iterator a = circles.begin(); a != circles.end(); ++a) {
        for (typename std::vector<HyperSphere<typename Type::first, Type::second::value>>::const_iterator b = circles.begin(); b != a; ++b) {
            bool collision = gjk.collide(*a, *b);
            if (collision) {
                BOOST_CHECK_LE(length(a->center() - b->center()), a->radius() + b->radius());
            } else {
                BOOST_CHECK_GE(length(a->center() - b->center()), a->radius() + b->radius());
            }
        }
    }
}*/

BOOST_AUTO_TEST_SUITE_END()

} /* namespace Geometry */
