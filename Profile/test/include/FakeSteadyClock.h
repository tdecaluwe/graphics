#include <chrono>

template <class Duration>
class FakeSteadyClock {
public:
	using rep = typename Duration::rep;
	using period = typename Duration::period;
	using duration = Duration;
	using time_point = std::chrono::time_point<FakeSteadyClock>;
    static bool const is_steady = true;
    static time_point now();
    static void add(unsigned ticks);
private:
    static time_point sNow;
};

template <class Duration>
typename FakeSteadyClock<Duration>::time_point FakeSteadyClock<Duration>::sNow = { };

template <class Duration>
inline typename FakeSteadyClock<Duration>::time_point FakeSteadyClock<Duration>::now() {
	return sNow;
}

template <class Duration>
inline void FakeSteadyClock<Duration>::add(unsigned ticks) {
	sNow = sNow + duration{ ticks };
}
