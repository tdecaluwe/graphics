#include <chrono>

template <class Duration>
class FakeClock {
public:
	using rep = typename Duration::rep;
	using period = typename Duration::period;
	using duration = Duration;
	using time_point = std::chrono::time_point<FakeClock>;
    static bool const is_steady = false;
    static time_point now();
    static void add(int ticks);
private:
    static time_point sNow;
};

template <class Duration>
typename FakeClock<Duration>::time_point FakeClock<Duration>::sNow = { };

template <class Duration>
inline typename FakeClock<Duration>::time_point FakeClock<Duration>::now() {
	return sNow;
}

template <class Duration>
inline void FakeClock<Duration>::add(int ticks) {
	sNow = sNow + duration{ ticks };
}
