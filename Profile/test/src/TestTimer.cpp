#include "Profile/Timer.h"

#include <boost/test/unit_test.hpp>

#include <thread>
#include <utility>

struct TimerFixture {
    std::chrono::microseconds::rep microseconds = 1000u;
    Profile::Timer<std::chrono::microseconds> timer;
};

BOOST_FIXTURE_TEST_SUITE(TestTimer, TimerFixture)

BOOST_AUTO_TEST_CASE(Initialization) {
    BOOST_CHECK_EQUAL(timer.read(), 0u);
}

BOOST_AUTO_TEST_CASE(Copy) {
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    std::chrono::microseconds::rep value = timer.stop();
    BOOST_CHECK_EQUAL(Profile::Timer<std::chrono::microseconds>(timer).read(), value);
}

BOOST_AUTO_TEST_CASE(Move) {
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    std::chrono::microseconds::rep value = timer.stop();
    BOOST_CHECK_EQUAL(Profile::Timer<std::chrono::microseconds>(std::move(timer)).read(), value);
}

BOOST_AUTO_TEST_CASE(Assignment) {
    Profile::Timer<std::chrono::microseconds> a, b;
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    std::chrono::microseconds::rep value = timer.stop();
    BOOST_CHECK_EQUAL((a = timer).read(), value);
    BOOST_CHECK_EQUAL((b = std::move(timer)).read(), value);
}

BOOST_AUTO_TEST_CASE(Read) {
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    timer.stop();
    BOOST_CHECK(timer.read() >= microseconds);
}

BOOST_AUTO_TEST_CASE(Split) {
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    BOOST_CHECK(timer.split() >= microseconds);
}

BOOST_AUTO_TEST_CASE(Reset) {
    timer.start();
    std::this_thread::sleep_for(std::chrono::microseconds(microseconds));
    timer.stop();
    timer.reset();
    BOOST_CHECK_EQUAL(timer.read(), 0u);
}

BOOST_AUTO_TEST_SUITE_END()
