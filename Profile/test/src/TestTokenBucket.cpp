#include "FakeClock.h"
#include "FakeSteadyClock.h"

#include "Profile/TokenBucket.h"

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>

#include <chrono>

using boost::mpl::list;
using std::chrono::duration;
using std::ratio;

template <template <class> class C>
class TokenBucketFixture {
protected:
	// pick two numbers which are not coprime and have a greatest common
	// divisor greater than one
	static constexpr unsigned a = 35;
	static constexpr unsigned b = 21;
	// pick a number which is coprime with both previous numbers
	static constexpr unsigned period = 4;
	using A = duration<int, ratio<1, a>>;
	using B = duration<int, ratio<1, b>>;
	using Clock = C<A>;
	Profile::TokenBucket<Clock> tokenBucket;
public:
	TokenBucketFixture()
	: tokenBucket(20, B{ period }) {
		// remove all tokens from the TokenBucket
		tokenBucket.drain();
	}
};

template <template <class> class C>
constexpr unsigned TokenBucketFixture<C>::a;

template <template <class> class C>
constexpr unsigned TokenBucketFixture<C>::b;

template <template <class> class C>
constexpr unsigned TokenBucketFixture<C>::period;

BOOST_FIXTURE_TEST_SUITE(TestTokenBucket, TokenBucketFixture<FakeSteadyClock>)

BOOST_AUTO_TEST_CASE(Available) {
    Clock::add(14);
	BOOST_CHECK_EQUAL(tokenBucket.available(), 2);
	Clock::add(1000);
	BOOST_CHECK_EQUAL(tokenBucket.available(), 20);
}

BOOST_AUTO_TEST_CASE(Poll) {
    Clock::add(7);
    BOOST_CHECK_EQUAL(tokenBucket.poll(), true);
    BOOST_CHECK_EQUAL(tokenBucket.poll(), false);
}

BOOST_AUTO_TEST_CASE(Drain) {
    Clock::add(138);
    BOOST_CHECK_EQUAL(tokenBucket.drain(), 20);
}

BOOST_FIXTURE_TEST_CASE(DrainNegative, TokenBucketFixture<FakeClock>) {
    Clock::add(-138);
    BOOST_CHECK_EQUAL(tokenBucket.drain(), 0);
}

/**
 * @brief Check whether the TokenBucket is well-behaved when used in
 * conjunction with unsteady clocks.
 *
 * @test When updated the number of tokens in the bucket should be allowed to
 * be negative, however when checking for availability, zero should be
 * returned.
 */
BOOST_FIXTURE_TEST_CASE(ReverseTime, TokenBucketFixture<FakeClock>) {
    Clock::add(-7);
    tokenBucket.update();
    BOOST_CHECK_EQUAL(tokenBucket.poll(), false);
    Clock::add(7);
    BOOST_CHECK_EQUAL(tokenBucket.poll(), false);
    Clock::add(7);
    BOOST_CHECK_EQUAL(tokenBucket.poll(), true);
}

BOOST_AUTO_TEST_SUITE_END()
