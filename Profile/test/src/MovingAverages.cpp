#include "Profile/ExponentialMovingAverage.h"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(MovingAverages)

BOOST_AUTO_TEST_CASE(Constant) {
    Profile::ExponentialMovingAverage<double> a(0.5);
    BOOST_CHECK_EQUAL(a(1.0), 1.0/1.0);
    BOOST_CHECK_EQUAL(a.variance(), 0.0/1.0/1.0/1.0);
    BOOST_CHECK_EQUAL(a(2.0), 5.0/3.0);
    BOOST_CHECK_EQUAL(a.variance(), 6.0/3.0/3.0/3.0);
    BOOST_CHECK_EQUAL(a(3.0), 17.0/7.0);
    BOOST_CHECK_EQUAL(a.variance(), 182.0/7.0/7.0/7.0);
}

BOOST_AUTO_TEST_SUITE_END()
