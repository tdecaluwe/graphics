/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PROFILE_RUNNINGAVERAGE_H
#define PROFILE_RUNNINGAVERAGE_H

#include "MovingAverage.h"

namespace Profile {

/**
 * @brief A running average filter.
 *
 * The functions \f$w_i(k)\f$ as described in @ref statfilters are constant.
 */
template <class T>
class RunningAverage {
public:
    /**
     * @brief Construct a new running average filter
     */
    constexpr RunningAverage();
    virtual ~RunningAverage() = default;
    /**
     * @brief Returns a constant weight.
     */
    virtual T weight(T value) const;
};

template <class T>
inline constexpr RunningAverage<T>::RunningAverage() {}

} /* namespace Profile */
#endif /* PROFILE_RUNNINGAVERAGE_H */
