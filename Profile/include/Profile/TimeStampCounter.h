/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TIMESTAMPCOUNTER_H_
#define TIMESTAMPCOUNTER_H_

#include <ratio>
#include <chrono>

#include <boost/integer.hpp>

namespace Profile {

/**
 * @brief An implementation of the standard library clock concept using the time
 * stamp counter instruction (rdtsc).
 *
 * @tparam Frequency the processor frequency expressed as a std::ratio rational
 * number
 */
template <class Frequency>
struct TimeStampCounter;

template <long nom, long den>
class TimeStampCounter<std::ratio<nom, den>> {
public:
    using rep = typename boost::uint_t<64>::least;
    using period = std::ratio<den, nom>;
    using duration = std::chrono::duration<rep, period>;
    using time_point = std::chrono::time_point<TimeStampCounter>;
    static bool const is_steady = true;
    /**
     * @brief Get the current time as an instance of time_point
     */
    static time_point now();
private:
    TimeStampCounter() = default;
};

template <long nom, long den>
inline typename TimeStampCounter<std::ratio<nom, den>>::time_point TimeStampCounter<std::ratio<nom, den>>::now() {
    typename boost::uint_t<64>::least high;
    typename boost::uint_t<32>::least low;
    asm volatile("rdtsc" : "=d"(high), "=a"(low) : :);
    return time_point(duration((high << 32) + low));
}

} /* namespace Profile */
#endif /* TIMESTAMPCOUNTER_H_ */
