/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PROFILE_EXPONENTIALMOVINGAVERAGE_H
#define PROFILE_EXPONENTIALMOVINGAVERAGE_H

#include "MovingAverage.h"

namespace Profile {

/**
 * @brief An exponential moving average filter.
 *
 * The smoothing factor \f$\alpha\f$ is a constant provided on construction and
 * can be changed during operation.
 *
 * See also @ref statfilters.
 */
template <class T>
class ExponentialMovingAverage
: public MovingAverage<T> {
public:
    /**
     * @brief Construct a new exponential moving average filter
     *
     * @param alpha the smoothing factor \f$\alpha\f$
     */
    constexpr ExponentialMovingAverage(T alpha);
    virtual ~ExponentialMovingAverage() = default;
    /**
     * @brief Returns a constant weight \f$\alpha\f$.
     */
    virtual T weight(T value) const;
    /**@{
     * @brief Smoothing factor getters.
     */
    T& alpha();
    T const& alpha() const;
    /**@}*/
private:
    T mAlpha;
};

template <class T>
using EMA = ExponentialMovingAverage<T>;

template <class T>
inline constexpr ExponentialMovingAverage<T>::ExponentialMovingAverage(T alpha)
: mAlpha{ alpha } {}

template <class T>
inline T& ExponentialMovingAverage<T>::alpha() {
    return mAlpha;
}

template <class T>
inline T const& ExponentialMovingAverage<T>::alpha() const {
    return mAlpha;
}

} /* namespace Profile */
#endif /* PROFILE_EXPONENTIALMOVINGAVERAGE_H */
