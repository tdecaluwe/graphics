/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <chrono>

namespace Profile {

/**
 * @brief A lightweight wrapper for any standard library compliant clock
 * providing basic timing functionality.
 *
 * @tparam Duration an instantiation of the standard library template
 * std::chrono::duration
 * @tparam Clock a class which implements the standard library clock concept
 */
template <class Duration = std::chrono::milliseconds, class Clock = std::chrono::high_resolution_clock>
class Timer {
public:
    /**
     * @todo Remove explicit constructor definition when the trivial
     * std::chrono::duration constructor is fixed in the standard library.
     */
#ifndef FIXED
    Timer(): info(typename Clock::duration(0)) {}
#endif
    /**
     * @brief Start the timer. This results in undefined behaviour if the Timer
     * was already running.
     */
    void start();
    /**
     * @brief Reset the timer. This results in undefined behaviour if the Timer
     * was running.
     */
    void reset();
    /**
     * @brief Reset and restart the timer. This results in undefined behaviour
     * if the Timer wasn't running.
     */
    void restart();
    /**
     * @brief Stop the timer and return the elapsed time. This results in
     * undefined behaviour if the Timer wasn't running.
     */
    typename Duration::rep stop();
    /**
     * @brief Read the elapsed time. The result is undefined if the Timer is
     * still running.
     */
    typename Duration::rep read() const;
    /**
     * @brief Read the elapsed time. The result is undefined if the Timer is not
     * running.
     */
    typename Duration::rep split() const;
private:
    typedef typename Clock::duration::period Period;
    union Info {
    public:
        /**
         * @todo Remove explicit constructor definition when the trivial
         * std::chrono::duration constructor is fixed in the standard library.
         */
#ifndef FIXED
        Info(typename Clock::duration d): duration(d) {}
        Info(typename Clock::time_point t): time(t) {}
#endif
        typename Clock::duration duration;
        typename Clock::time_point time;
    } info;
};

template <class Duration, class Clock>
inline void Timer<Duration, Clock>::start() {
    info.time = Clock::now() - info.duration;
}

template <class Duration, class Clock>
inline void Timer<Duration, Clock>::reset() {
    info.duration = typename Clock::duration(0);
}

template <class Duration, class Clock>
inline void Timer<Duration, Clock>::restart() {
    info.time = Clock::now();
}

template <class Duration, class Clock>
inline typename Duration::rep Timer<Duration, Clock>::stop() {
    info.duration = Clock::now() - info.time;
    return read();
}

template <class Duration, class Clock>
inline typename Duration::rep Timer<Duration, Clock>::read() const {
    return std::chrono::duration_cast<Duration>(info.duration).count();
}

template <class Duration, class Clock>
inline typename Duration::rep Timer<Duration, Clock>::split() const {
    return std::chrono::duration_cast<Duration>(Clock::now() - info.time).count();
}

} /* namespace Profile */
#endif /* TIMER_H_ */
