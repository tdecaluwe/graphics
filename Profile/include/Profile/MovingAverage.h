/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef PROFILE_MOVINGAVERAGE_H
#define PROFILE_MOVINGAVERAGE_H

namespace Profile {

/**
 * @brief A simple abstract moving average class which implements all the
 * necessary logic to act as a generalized exponential moving average filter.
 *
 * See \ref statfilters for more information. A derived class should provide a
 * weight function that calculates a weight between 0 and 1 based on the
 * provided value, the current average and the current variance (accessible as
 * the member functions average() and variance() respectively).
 */
template <class T>
class MovingAverage {
public:
    /**
     * @brief The default constructor initializes all state variables to zero.
     */
    constexpr MovingAverage();
    virtual ~MovingAverage() = default;
    /**
     * @brief The abstract weight function.
     */
    virtual T weight(T value) const = 0;
    /**
     * @brief Access the current average.
     */
    T operator()() const;
    /**
     * @brief Pass a value to the filter. Also returns the current average.
     */
    T operator()(T value);
    /**
     * @brief Access the current average.
     */
    T average() const;
    /**
     * @brief Access the current variance.
     */
    T variance() const;
private:
    T mTotal;
    T mAverage;
    T mWeight;
    T mTotalSquares;
};

template <class T>
inline constexpr MovingAverage<T>::MovingAverage()
: mTotal{ }
, mAverage{ }
, mWeight{ }
, mTotalSquares{ } {}

template <class T>
inline T MovingAverage<T>::operator()() const {
    return mAverage;
}

template <class T>
inline T MovingAverage<T>::average() const {
    return mAverage;
}

template <class T>
inline T MovingAverage<T>::variance() const {
    return mTotalSquares/mWeight;
}

} /* namespace Profile */
#endif /* PROFILE_MOVINGAVERAGE_H */
