/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2013 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef TOKENBUCKET_H_
#define TOKENBUCKET_H_

#include "Profile/Timer.h"

#include <boost/mpl/if.hpp>

#include <chrono>
#include <utility>

namespace Profile {

template <class Clock = std::chrono::high_resolution_clock>
class TokenBucket {
private:
	class Stable;
    class Unstable;
	typename boost::mpl::if_c<Clock::is_steady, Stable, Unstable>::type mImplementation;
public:
	template <class Duration>
    TokenBucket(unsigned bucket, Duration period);
    /**
     * @brief Try to acquire a number of tokens from the TokenBucket.
     *
     * @param tokens the requested number of tokens
     *
     * @result true if the number of tokens was successfully acquired
     */
    bool poll(unsigned tokens);
    /**
     * @brief Try to acquire a token from the TokenBucket.
     *
     * @result true if a token was successfully acquired
     */
    bool poll();
    /**
     * @brief Inspect the number of available tokens in the TokenBucket.
     *
     * @result the number of tokens acquired
     */
    unsigned available() const;
    /**
     * @brief Acquire all available tokens from the TokenBucket.
     *
     * @result the number of tokens acquired
     */
    unsigned drain();
    /**
	 * @brief Reset the TokenBucket to its initial state.
	 */
	void reset();
private:
public:
    typedef typename Clock::duration::period Period;
    Timer<typename Clock::duration, Clock> mTimer;
    unsigned mBucket;
    long mLastAccessTimesNum;
    long mTokens;
    long mPeriodNum;
    long mPeriodDen;
    void update();
};

template <class Clock>
class TokenBucket<Clock>::Stable {
protected:
    std::intmax_t mClockToCommonMultiplier;
    std::intmax_t mCommonToPeriodDivider;
    std::intmax_t mLastAccess;
public:
    template <class Duration>
    Stable(Duration period);
    std::intmax_t lastAccess() const;
    std::intmax_t now() const;
    std::intmax_t update();
};

template <class Clock>
class TokenBucket<Clock>::Unstable: public Stable {
public:
    template <class Duration>
    Unstable(Duration&& period);
    std::intmax_t update();
};

template <class Clock>
template <class Duration>
inline TokenBucket<Clock>::Stable::Stable(Duration period)
: mClockToCommonMultiplier { Clock::period::num*std::common_type<typename Clock::duration, Duration>::type::period::den/Clock::period::den/std::common_type<typename Clock::duration, Duration>::type::period::num }
, mCommonToPeriodDivider { std::chrono::duration_cast<typename std::common_type<typename Clock::duration, Duration>::type>(period).count() }
, mLastAccess { now() } {}

template <class Clock>
inline std::intmax_t TokenBucket<Clock>::Stable::lastAccess() const {
    return mLastAccess;
}

template <class Clock>
inline std::intmax_t TokenBucket<Clock>::Stable::now() const {
    return Clock::now().time_since_epoch().count()*mClockToCommonMultiplier/mCommonToPeriodDivider;
}

template <class Clock>
inline std::intmax_t TokenBucket<Clock>::Stable::update() {
    return mLastAccess = now();
}

template <class Clock>
template <class Duration>
inline TokenBucket<Clock>::Unstable::Unstable(Duration&& period)
: Stable(std::forward<Duration>(period)) {}

template <class Clock>
inline std::intmax_t TokenBucket<Clock>::Unstable::update() {
    std::intmax_t time = Stable::now();
    return Stable::mLastAccess = Stable::mLastAccess < time ? time : Stable::mLastAccess;
}

template <class Clock>
inline unsigned TokenBucket<Clock>::available() const {
	unsigned missingTokens = mImplementation.now() - mImplementation.lastAccess();
    return mTokens + missingTokens > mBucket ? mBucket : mTokens + missingTokens;
}

template <class Clock>
inline void TokenBucket<Clock>::update() {
    std::intmax_t lastAccess = mImplementation.lastAccess();
    unsigned missingTokens = mImplementation.update() - lastAccess;
	mTokens = mTokens + missingTokens > mBucket ? mBucket : mTokens + missingTokens;
}

template <class Clock>
template <class Duration>
inline TokenBucket<Clock>::TokenBucket(unsigned bucket, Duration period)
: mTimer { }
, mBucket { bucket }
, mLastAccessTimesNum { 0u }
, mTokens { static_cast<int>(bucket) }
, mPeriodNum { static_cast<unsigned>(std::common_type<Duration, typename Clock::duration>::type::period::den/Clock::duration::period::den) }
, mPeriodDen { static_cast<unsigned>(static_cast<typename std::common_type<Duration, typename Clock::duration>::type>(period).count()*std::common_type<Duration, typename Clock::duration>::type::period::num/Clock::duration::period::num) }
, mImplementation { period } {
    mTimer.start();
}

template <class Clock>
inline bool TokenBucket<Clock>::poll(unsigned tokens) {
	update();
    bool result;
    if (mTokens >= tokens) {
        result = true;
        mTokens -= tokens;
    } else {
        result = false;
    }
    return result;
}

template <class Clock>
inline bool TokenBucket<Clock>::poll() {
    return poll(1u);
}

template <class Clock>
inline unsigned TokenBucket<Clock>::drain() {
	update();
    unsigned result = mTokens > 0 ? mTokens : 0;
    mTokens -= result;
    return result;
}

template <class Clock>
inline void TokenBucket<Clock>::reset() {
    mTokens = mBucket;
    mLastAccessTimesNum = 0u;
    mTimer.restart();
}

} /* namespace Profile */
#endif /* TOKENBUCKET_H_ */
