# reset local variables
unset(SOURCES)

# get all source files
include(sources.cmake OPTIONAL)

foreach(SOURCE ${SOURCES})
    # add test target
    get_filename_component(SOURCENAME ${SOURCE} NAME_WE)
    add_library(${SOURCENAME} EXCLUDE_FROM_ALL ${SOURCE})

    # CMake 2.8.7 hack
    set_property(TARGET ${SOURCENAME} PROPERTY INCLUDE_DIRECTORIES)

    # dependency on current project
    get_property(INCLUDES TARGET ${PROJECT_NAME} PROPERTY INCLUDE_DIRECTORIES)
    set_property(TARGET ${SOURCENAME} APPEND PROPERTY INCLUDE_DIRECTORIES ${INCLUDES})

    # add test include files
    set_property(TARGET ${SOURCENAME} APPEND PROPERTY INCLUDE_DIRECTORIES ${PROJECT_SOURCE_DIR}/test/include)

    # CMake 2.8.7 hack
    get_property(INCLUDES TARGET ${SOURCENAME} PROPERTY INCLUDE_DIRECTORIES)
    include_directories(${INCLUDES})

    # test for unsuccessful build
    add_test(${PROJECT_NAME}/${SOURCENAME} "${CMAKE_COMMAND}" --build ${CMAKE_BINARY_DIR} --target ${SOURCENAME})
    set_tests_properties(${PROJECT_NAME}/${SOURCENAME} PROPERTIES WILL_FAIL true)
endforeach(SOURCE ${SOURCES})
