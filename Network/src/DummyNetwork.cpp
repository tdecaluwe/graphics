/*
 * DummyNetwork.cpp
 *
 *  Created on: 18-dec.-2012
 *      Author: tom
 */

#include "Network/DummyNetwork.h"

namespace Network {

void DummyNetwork::stats(std::ostream& out) const {}

void DummyNetwork::reserve(Index n) {}

} /* namespace Network */
