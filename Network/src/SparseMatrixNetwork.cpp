/*
 * SparseMatrixNetwork.cpp
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#include "Network/SparseMatrixNetwork.h"

#include <iomanip>

namespace Network {

void SparseMatrixNetwork::stats(std::ostream& out) const {
	Index size = 0, capacity = 0;
	Index mapSize = (sizeof(std::unordered_map<Index, Index>::value_type) + sizeof(std::unordered_map<Index, Index>::pointer))*mAddresses.size() + sizeof(std::unordered_map<Index, Index>::pointer)*mAddresses.bucket_count();
	size += mBlocks.size()*sizeof(Block) + mapSize;
	capacity += mBlocks.capacity()*sizeof(Block) + mapSize;
	out << std::left << std::setw(50) << "Sparse matrix network:" << std::right << std::setw(10) << size << " / " << std::setw(10) << capacity << " bytes" << std::endl;
}

void SparseMatrixNetwork::reserve(Index n) {
	mAddresses.reserve(n);
}

} /* namespace Network */
