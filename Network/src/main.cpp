/*
 * main.cpp
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#include <cmath>
#include <set>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "Profile/Timer.h"

#include "Network/SparseMatrixNetwork.h"
#include "Network/AdjacencyNetwork.h"
#include "Network/DummyNetwork.h"

#include "Network/Analyzer.h"

unsigned int m = 2;

double D(unsigned int n) {
    double result = m*(m+1);
    return result/(n*(n+1));
}

using Network::Analyzer;

using Network::DummyNetwork;
using Network::AdjacencyNetwork;
using Network::SparseMatrixNetwork;

int main() {
	unsigned long total = 10000000u;
	unsigned long nodes = 1u;
	unsigned long show = 10u;
	std::ostringstream stream;
	std::vector<unsigned long> dis;
	Profile::Timer<> t;
//	t.restart();
//	std::cout << "Sparse triangular matrix network and degree distribution generation" << std::endl;
//	Analyzer<SparseMatrixNetwork> analyzer(nodes);
//	analyzer.expand(total);
//	t.stop();
//	std::cout << "Time: " << t.read() << std::endl;
//	t.restart();
//	dis.clear();
//	dis.resize(analyzer.maxDegree() + 1);
//	for (unsigned int i = 0; i < total; ++i) {
//		dis[analyzer.degree(i)] += 1;
//	}
//    t.stop();
//	for (unsigned int i = 0; i < show; ++i) {
//		std::cout << std::setw(7) << dis[i] << " ";
//	}
//	std::cout << std::endl;
//	std::cout << "Time: " << t.read() << std::endl;
//	analyzer.stats(std::cout);
	t.restart();
	std::cout << "Adjacency list network and offline hirsch index distribution generation" << std::endl;
	Analyzer<AdjacencyNetwork> adjacency(nodes);
	adjacency.expand(total);
    t.stop();
    std::cout << "Time: " << t.read() << std::endl;
	t.restart();
	dis.clear();
	dis.resize(adjacency.maxDegree() + 1);
	for (unsigned int i = 0; i < total; ++i) {
		dis[adjacency.degree(i)] += 1;
	}
	t.stop();
	for (unsigned int i = 0; i < show; ++i) {
		std::cout << std::setw(7) << dis[i] << " ";
	}
	std::cout << std::endl;
	int n20 = 0, n21 = 0, n22 = 0;
	std::cout << "Time: " << t.read() << std::endl;
	for (unsigned i = 0; i < total; ++i) {
		unsigned count = 0;
		if (adjacency.degree(i) == 3) {
			for (auto it = adjacency.network().begin(i); it != adjacency.network().end(i); ++it) {
				count += adjacency.degree(*it) > nodes;
			}
			if (count == 0) n20++;
			if (count == 1) n21++;
			if (count == 2) n22++;
		}
	}
	std::cout << n20 << " " << n21 << " " << n22 << std::endl;
	adjacency.stats(std::cout);
	t.restart();
	std::cout << "Degree distribution generation without additional network structures" << std::endl;
//	Analyzer<DummyNetwork> dummy(nodes);
//	dummy.expand(total);
//	t.stop();
//	std::cout << "Time: " << t.read() << std::endl;
//	t.restart();
//	dis.clear();
//	dis.resize(dummy.maxDegree() + 1);
//	for (unsigned i = 0; i < total; ++i) {
//		dis[dummy.degree(i)] += 1;
//	}
//	for (unsigned i = 0; i < show; ++i) {
//		std::cout << std::setw(7) << dis[i] << " ";
//	}
//    t.stop();
//	std::cout << std::endl;
//	std::cout << "Time: " << t.read() << std::endl;
//	t.restart();

//    dis.clear();
//    dis.resize(show*show);
//    unsigned long count = 0;
//	for (unsigned i = 0; i < 1000000; ++ i) {
//	    t.restart();
//	    Analyzer<DummyNetwork> dummy(nodes);
//	    unsigned long job = total - bool(i % 2);
//	    dummy.expand(job);
//	    t.stop();
//	    std::cout << "Time: " << t.read() << std::endl;
//        t.restart();
//        for (unsigned i = 0; i < dummy.mFrom.size(); ++i) {
//            unsigned a = dummy.degree(dummy.mFrom[i]);
//            unsigned b = dummy.degree(dummy.mTo[i]);
//            if (a < show && b < show) {
//                dis[dummy.degree(dummy.mFrom[i])*show + dummy.degree(dummy.mTo[i])] += 1;
//                dis[dummy.degree(dummy.mFrom[i]) + dummy.degree(dummy.mTo[i])*show] += 1;
//            }
//        }
//        t.stop();
//        std::cout << "Total edges: " << (count += nodes*(2*job - nodes - 1)) << std::endl;
//        for (unsigned i = 0; i < show; ++i) {
//            for (unsigned j = 0; j < show; ++j) {
//                std::cout << std::setw(12) << dis[i*show + j] << " ";
//            }
//            std::cout << std::endl;
//        }
//        std::cout << "Time: " << t.read() << std::endl;
//	}

//	dummy.stats(std::cout);
	t.restart();
}
