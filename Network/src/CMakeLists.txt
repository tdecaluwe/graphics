# reset local variables
unset(DEPENDENCIES)
unset(SOURCES)
unset(LIBRARIES)

# read dependencies
include(dependencies.cmake OPTIONAL)

# set target name
set(TARGET ${PROJECT_NAME})

# get all source files
discover_sources("${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")
include("${CMAKE_CURRENT_BINARY_DIR}/sources.cmake")

discover_target("${TARGET}" "${SOURCES}")
discover_sources_hook("${TARGET}" "${CMAKE_CURRENT_SOURCE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}")

# build dependencies
build_dependencies("${TARGET}" "${DEPENDENCIES}")

# CMake 2.8.7 hack
get_property(INCLUDE_DIRECTORIES TARGET ${TARGET} PROPERTY INCLUDE_DIRECTORIES)
include_directories(${INCLUDE_DIRECTORIES})

# add libraries
include("libraries.cmake" OPTIONAL)
target_link_libraries("${TARGET}" ${LIBRARIES})

# add include directory
if(EXISTS "${PROJECT_SOURCE_DIR}/include")
    # add include files
    set_property(TARGET "${TARGET}" APPEND PROPERTY INCLUDE_DIRECTORIES "${PROJECT_SOURCE_DIR}/include")
endif()
