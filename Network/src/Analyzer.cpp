/*
 * Analyzer.cpp
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#include "Network/Analyzer.h"

#include "Network/SparseMatrixNetwork.h"
#include "Network/AdjacencyNetwork.h"
#include "Network/DummyNetwork.h"

#include <iomanip>
#include <iostream>

namespace Network {

template <class Network>
Analyzer<Network>::Analyzer(Index n): mNodes(n), mMaxDegree(0) {
	resize(nodes() + 1);
	for (Index i = 0; i <= nodes(); ++i) {
		for (Index j = 0; j < i; ++j) {
			mNetwork.connect(i, j);
			create(i, j);
		}
        create(i);
	}
}

template <class Network>
void Analyzer<Network>::stats(std::ostream& out) const {
	mNetwork.stats(out);
	out << std::left << std::setw(50) << "Degree cache:" << std::right << std::setw(10) << mDegrees.size()*sizeof(unsigned int) << " / " << std::setw(10) << mDegrees.capacity()*sizeof(unsigned int) << " bytes" << std::endl;
	out << std::left << std::setw(50) << "New link distribution structure:" << std::right << std::setw(10) << mShuffle.size()*sizeof(Index) << " / " << std::setw(10) << mShuffle.capacity()*sizeof(Index) << " bytes" << std::endl;
	out << std::left << std::setw(50) << "Edges:" << std::right << std::setw(10) << (mFrom.size() + mTo.size())*sizeof(Index) << " / " << std::setw(10) << (mFrom.capacity() + mTo.capacity())*sizeof(Index) << " bytes" << std::endl;
}

template <class Network>
void Analyzer<Network>::add(Index n) {
    expand(size() + n);
}

template <class Network>
void Analyzer<Network>::expand(Index n) {
    Index current = size();
    resize(n);
    for (Index i = current; i < n; ++i) {
        Index temporary;
        for (unsigned j = 0; j < nodes(); ++j) {
            Index r = random();
            mShuffle[j] ^= mShuffle[r];
            mShuffle[r] ^= mShuffle[j];
            mShuffle[j] ^= mShuffle[r];
        }
        for (unsigned j = 0; j < nodes(); ++j) {
            mNetwork.connect(mShuffle[j], i);
            create(mShuffle[j], i);
            mMaxDegree = mMaxDegree < degree(mShuffle[j]) ? degree(mShuffle[j]) : mMaxDegree;
        }
        create(i);
    }
}

template class Analyzer<SparseMatrixNetwork>;
template class Analyzer<AdjacencyNetwork>;
template class Analyzer<DummyNetwork>;

} /* namespace Network */
