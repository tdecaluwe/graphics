/*
 * AdjacencyNetwork.cpp
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#include "Network/AdjacencyNetwork.h"

#include <iomanip>

namespace Network {

void AdjacencyNetwork::stats(std::ostream& out) const {
	Index size = 0, capacity = 0;
	size += mAdjacencies.size()*sizeof(std::vector<Index>) + sizeof(std::vector<std::vector<Index>>);
	capacity += mAdjacencies.capacity()*sizeof(std::vector<Index>) + sizeof(std::vector<std::vector<Index>>);
	for (std::vector<std::vector<Index>>::const_iterator iterator = mAdjacencies.begin(); iterator != mAdjacencies.end(); ++iterator) {
		size += iterator->size() + sizeof(std::vector<Index>);
		capacity += iterator->capacity() + sizeof(std::vector<Index>);
	}
	out << std::left << std::setw(50) << "Adjacency network:" << std::right << std::setw(10) << size << " / " << std::setw(10) << capacity << " bytes" << std::endl;
}

void AdjacencyNetwork::reserve(Index n) {
	mAdjacencies.resize(n);
}

} /* namespace Network */
