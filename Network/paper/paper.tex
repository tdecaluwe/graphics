\documentclass[10pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}

\title{The Hirsch index in scale free networks}
\author{Tom De Caluwé}

\begin{document}

\maketitle

\tableofcontents

\section{The Barabási-Albert model}

\section{Basic properties}

\noindent The probability mass function of the degree distribution in a large scale free network constructed using the Barabási-Albert model can be found using the rate equation approach. It is given by:
\begin{equation}
d(k) = \frac{2m(m+1)}{k(k+1)(k+2)}
\end{equation}

\noindent The tail distribution can be found as follows:
\begin{align}
\overline{D}(k)&=\sum_{i=k}^\infty d(i) = \sum_{i=k}^\infty \frac{2m(m+1)}{i(i+1)(i+2)} \nonumber\\
&=\sum_{i=k}^\infty m(m+1)\left(\frac{1}{i} - \frac{2}{i+1} + \frac{1}{i+2}\right) \nonumber\\
&=m(m+1)\left(\sum_{i=k}^\infty \frac{1}{i} - \sum_{i=k+1}^\infty \frac{2}{i} + \sum_{i=k+2}^\infty \frac{1}{i}\right) \nonumber\\
&=m(m+1)\left(\frac{1}{k}+\frac{1}{k+1}-\frac{2}{k+1}\right) \nonumber\\
&=\frac{m(m+1)}{k(k+1)}
\end{align}

\noindent Using a similar approach the probability mass function for edges connecting nodes of degree $k$ and $l$ can be found. The rate equation governing the evolution of the number of those edges where $k>m$ and $l>m$ is:
\begin{align}
N_e(t+1)(k,l) = N_e(t)(k,l)
&- \frac{mk}{2mt}N_e(t)(k,l) + \frac{m(k-1)}{2mt}N_e(t)(k-1,l) \nonumber\\
&- \frac{ml}{2mt}N_e(t)(k,l) + \frac{m(l-1)}{2mt}N_e(t)(k,l-1)
\end{align}

\noindent Furthermore $N_e(t)(m,m)=0$ holds and when $k=m$ or $l=m$ the rate equation needs to take into account the new edges created on each step. Since the problem is symmetric in $k$ and $l$ only one rate equation suffices:
\begin{align}
N_e(t+1)(k,m) = N_e(t)(k,m)
&- \frac{k}{2t}N_e(t)(k,m) + \frac{k-1}{2t}N_e(t)(k-1,m) \nonumber\\
&- \frac{m}{2t}N_e(t)(k,m) + \frac{k-1}{2t}td(k-1)
\end{align}

\noindent When $t\rightarrow\infty$	we can substitute $N_e(t)(k,l)$ for $te(k,l)$ where $e(k,l)$ is the probability mass function for edges connecting nodes of degree $k$ and $l$:
\begin{align}
(2+k+m)e(k,m) = (k-1)e(k-1,m) + (k-1)d(k-1)
\end{align}

\noindent Substituting $f(k)=e(k,m)-\frac{m}{k(k+1)}$ results in:
\begin{align}
(2+k+m)\left[f(k)+\frac{m}{k(k+1)}\right] &= (k-1)\left[f(k-1)+\frac{m}{(k-1)k}\right] \nonumber\\
&\phantom{=} + \frac{2m(m+1)}{k(k+1)(k+2)} \nonumber\\
(2+k+m)f(k) &= (k-1)f(k-1) + \frac{2m(m+1)}{k(k+1)(k+2)} \nonumber\\
&\phantom{=} + \frac{m(k+1)}{k(k+1)} - \frac{m(2+k+m)}{k(k+1)} \nonumber\\
(2+k+m)f(k) &= (k-1)f(k-1) + \frac{2m(m+1)}{k(k+1)(k+2)} \nonumber\\
&\phantom{=} - \frac{m(m+1)}{k(k+1)} \nonumber
\end{align}

\begin{equation}
e(k,l)=\frac{m(m+1)}{k(k+1)l(l+1)}-\frac{(2m+2)!}{(m+1)!(m-1)!}\frac{(k-1)!}{(k-m)!}\frac{(l-1)!}{(l-m)!}\frac{(k+l-2m)!}{(k+l+2)!}
\end{equation}

\section{The $k$-degree and related results}

\noindent Defining the $k$-degree $d_k$ as the number of neighbors with a given degree $k$, a recurrence relation for the joint distribution of the degree and the $m$-degree can be found using the following rate equation which holds for $l>0$:
\begin{align}
(t+1)p(k,l)&=tp(k,l) - \frac{mk}{2mt}tp(k,l) + \frac{m(k-1)}{2mt}tp(k-1,l-1) \nonumber\\
&\phantom{=tp(k,l)} - \frac{m^2l}{2mt}tp(k,l) + \frac{m^2(l+1)}{2mt}tp(k,l+1) \nonumber\\
(2+k+ml)p(k,l)&=(k-1)p(k-1,l) + m(l+1)p(k,l+1)
\end{align}

\noindent Considering the case $l=k-m$ yields:
\begin{align}
(k(m+1)+2-m^2)p(k,k-m)&=(k-1)p(k-1,k-1-m)
\end{align}

\noindent Since all new nodes have $m$-degree zero, $p(m,0)=d(m)=\frac{2}{2+m}$ holds, so that:
\begin{align}
p(k,k-m)&=\frac{2(k-1)!}{(m-1)!\prod_{i=m}^k i(m+1)+2-m^2}
\end{align}

\noindent The probability mass $h(m)$ can then be calculated when $m=1$:
\begin{align}
h(m)\bigg|_{m=1} &= \sum_{i=1}^\infty p(i,i-1) = \sum_{i=1}^\infty \frac{2(i-1)!}{\prod_{j=1}^i 2j+1} \nonumber\\
&= \sum_{i=1}^\infty \frac{2(i-1)!}{(2i+1)!!} = \sum_{i=1}^\infty \frac{2^{i+1}(i-1)!i!}{(2i+1)!} \nonumber\\
&= \sum_{i=0}^\infty \frac{(2x)^{2i+4}i!(i+1)!}{(2i+3)!} \bigg|_{x=\frac{1}{\sqrt{2}}} \nonumber\\
&= 8x\left[x-\sqrt{1-x^2}\arcsin(x)\right] \bigg|_{x=\frac{1}{\sqrt{2}}}=4-\pi
\end{align}

\end{document}