/*
 * SparseMatrixNetwork.h
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#ifndef SPARSEMATRIXNETWORK_H_
#define SPARSEMATRIXNETWORK_H_

#include <utility>
#include <unordered_map>
#include <vector>
#include <ostream>
#include <boost/integer.hpp>

namespace Network {

class SparseMatrixNetwork {
public:
	typedef unsigned Index;
	void stats(std::ostream& out) const;
	void connect(Index a, Index b);
	bool status(Index a, Index b) const;
	void reserve(Index n);
private:
	static constexpr unsigned int sBlockSize = 4u;
	typedef typename boost::uint_t<sBlockSize*sBlockSize>::least Block;
	static constexpr Index min(Index a, Index b) { return a < b ? a : b; }
	static constexpr Index max(Index a, Index b) { return a < b ? b : a; }
	static constexpr Index block(Index i) { return i/sBlockSize; }
	static constexpr Index block(Index a, Index b) {
		return block(max(a, b))*(block(max(a, b)) + 1)/2 + block(min(a, b));
	}
	static constexpr Index offset(Index i) { return i%sBlockSize; }
	static constexpr Index offset(Index a, Index b) {
		return offset(max(a, b))*sBlockSize + offset(min(a, b));
	}
	std::unordered_map<Index, Index> mAddresses;
	std::vector<Block> mBlocks;
};

inline void SparseMatrixNetwork::connect(Index a, Index b) {
	static constexpr Block one = 1u;
	if (mAddresses.count(block(a, b)) > 0) {
		Index index = mAddresses.find(block(a, b))->second;
		if ((mBlocks[index] & (one << offset(a, b))) == 0) {
			mBlocks[index] |= one << offset(a, b);
		}
	} else {
		mAddresses.insert(std::make_pair(block(a, b), mBlocks.size()));
		mBlocks.push_back(one << offset(a, b));
	}
}

inline bool SparseMatrixNetwork::status(Index a, Index b) const {
	static constexpr Block one = 1u;
	return mAddresses.count(block(a, b)) > 0 ? mBlocks[mAddresses.find(block(a, b))->second] & (one << offset(a, b)) : false;
}

} /* namespace Network */
#endif /* SPARSEMATRIXNETWORK_H_ */
