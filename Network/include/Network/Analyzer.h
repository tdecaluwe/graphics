/*
 * Analyzer.h
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#ifndef ANALYZER_H_
#define ANALYZER_H_

#include <random>
#include <vector>
#include <ostream>

namespace Network {

template <class Network>
class Analyzer {
public:
	typedef typename Network::Index Index;
	explicit Analyzer(Index);
	virtual ~Analyzer() = default;
	void stats(std::ostream& out) const;
	void add(Index);
	void expand(Index);
	unsigned size() const;
	unsigned degree(Index i) const;
	unsigned maxDegree() const;
	Network& network();
	Network const& network() const;
    std::vector<Index> mFrom;
    std::vector<Index> mTo;
protected:
	virtual void resize(Index n);
	virtual void create(Index i);
	virtual void create(Index i, Index j);
	Index nodes() const;
	Index random() const;
private:
	Index mNodes;
	Network mNetwork;
	std::vector<unsigned> mDegrees;
    std::vector<Index> mShuffle;
	unsigned mMaxDegree;
};

template <class Network>
inline unsigned int Analyzer<Network>::size() const {
	return mDegrees.size();
}

template <class Network>
inline unsigned Analyzer<Network>::degree(Index i) const {
	return mDegrees[i];
}

template <class Network>
inline unsigned int Analyzer<Network>::maxDegree() const {
	return mMaxDegree;
}

template <class Network>
Network& Analyzer<Network>::network() {
	return mNetwork;
}

template <class Network>
Network const& Analyzer<Network>::network() const {
	return mNetwork;
}

template <class Network>
inline void Analyzer<Network>::resize(Index n) {
	mDegrees.resize(n, 0);
	mFrom.reserve(mNodes*(2*n - mNodes - 1)/2);
	mTo.reserve(mNodes*(2*n - mNodes - 1)/2);
	mShuffle.reserve(mNodes*(2*n - mNodes - 1));
	mNetwork.reserve(mNodes*n);
}

template <class Network>
inline void Analyzer<Network>::create(Index i) {}

template <class Network>
inline void Analyzer<Network>::create(Index i, Index j) {
    mDegrees[i] += 1;
    mDegrees[j] += 1;
    mShuffle.push_back(i);
    mShuffle.push_back(j);
    mFrom.push_back(i);
    mTo.push_back(j);
}

template <class Network>
inline typename Analyzer<Network>::Index Analyzer<Network>::nodes() const {
	return mNodes;
}

template <class Network>
inline typename Analyzer<Network>::Index Analyzer<Network>::random() const {
	 static std::random_device device;
	 static std::mt19937 generator(device());
	 std::uniform_int_distribution<typename std::vector<Index>::size_type> distribution(0, mShuffle.size() - 1);
	 return distribution(generator);
}

} /* namespace Network */
#endif /* ANALYZER_H_ */
