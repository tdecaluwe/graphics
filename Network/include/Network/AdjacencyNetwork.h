/*
 * AdjacencyNetwork.h
 *
 *  Created on: 17-dec.-2012
 *      Author: tom
 */

#ifndef ADJACENCYNETWORK_H_
#define ADJACENCYNETWORK_H_

#include <vector>
#include <ostream>

namespace Network {

class AdjacencyNetwork {
public:
	typedef unsigned int Index;
	void stats(std::ostream& out) const;
	void connect(Index a, Index b);
	std::vector<Index>::iterator begin(Index);
	std::vector<Index>::iterator end(Index);
	void reserve(Index n);
private:
	std::vector<std::vector<Index>> mAdjacencies;
};

inline void AdjacencyNetwork::connect(Index a, Index b) {
	mAdjacencies[a].push_back(b);
	mAdjacencies[b].push_back(a);
}

inline std::vector<AdjacencyNetwork::Index>::iterator AdjacencyNetwork::begin(Index i) {
	return mAdjacencies[i].begin();
}
inline std::vector<AdjacencyNetwork::Index>::iterator AdjacencyNetwork::end(Index i) {
	return mAdjacencies[i].end();
}

} /* namespace Network */
#endif /* ADJACENCYNETWORK_H_ */
