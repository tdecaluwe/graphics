/*
 * DummyNetwork.h
 *
 *  Created on: 18-dec.-2012
 *      Author: tom
 */

#ifndef DUMMYNETWORK_H_
#define DUMMYNETWORK_H_

#include <ostream>

namespace Network {

class DummyNetwork {
public:
	typedef unsigned int Index;
	void stats(std::ostream& out) const;
	void connect(Index a, Index b);
	void reserve(Index n);
};

inline void DummyNetwork::connect(Index a, Index b) {}

} /* namespace Network */
#endif /* DUMMYNETWORK_H_ */
