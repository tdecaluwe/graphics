/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2014 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MEMORY_BASEARRAY_H
#define MEMORY_BASEARRAY_H

#include <iosfwd>

namespace Memory {

template <class T, unsigned n>
class BaseArray {
public:
    constexpr BaseArray() = default;
    template <class ... List>
    constexpr BaseArray(List&&... args);
    /**@{
     * @brief Coordinate accessors.
     *
     * @param index the index of the requested coordinate
     */
    T const& operator[](unsigned int index) const;
    T& operator[](unsigned int index);
    /**@}*/
private:
    T mArray[n];
};

template <class T, unsigned n>
std::ostream& operator<<(std::ostream& out, BaseArray<T, n> const& a);

template <class T, unsigned n>
template <class ... List>
inline constexpr BaseArray<T, n>::BaseArray(List&&... args)
: mArray { args... } {}

template <class T, unsigned n>
inline T const& BaseArray<T, n>::operator[](unsigned int index) const {
    return mArray[index];
}

template <class T, unsigned n>
inline T& BaseArray<T, n>::operator[](unsigned int index) {
    return mArray[index];
}

} /* namespace Memory */
#endif /* MEMORY_BASEARRAY_H */
