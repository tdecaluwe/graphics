/**
 * @file
 * @author Tom De Caluwé <decaluwe.t at gmail.com>
 *
 * @copyright Copright 2014 Tom De Caluwé.
 *
 * @par License
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see http://www.gnu.org/licenses/.
 */

#ifndef MEMORY_SUBARRAY_H
#define MEMORY_SUBARRAY_H

#include <iosfwd>

namespace Memory {

/**
 * @brief A forward declared class for handling template parameter packs.
 */
template <class ... List>
class Pack;

template <class B, class T, class P = Pack<>, T (* ... list)()>
class SubArray;

template <class B, class T, class ... List, T (* ... list)()>
class SubArray<B, T, Pack<List...>, list...>
: public B {
public:
    constexpr SubArray() = default;
    constexpr SubArray(List... args);
    /**@{
     * @brief Coordinate accessors.
     *
     * @param index the index of the requested coordinate
     */
    T const& operator[](unsigned int index) const;
    T& operator[](unsigned int index);
    /**@}*/
};

template <class B, class T, unsigned m, class P = Pack<>, T (* ... list)()>
struct SubArrayUnroll;

template <class B, class T, unsigned m, class ... List, T (* ... list)()>
struct SubArrayUnroll<B, T, m, Pack<List...>, list...> {
    using Type = typename SubArrayUnroll<B, T, m - 1, Pack<List..., T>, list...>::Type;
};

template <class B, class T, class ... List, T (* ... list)()>
struct SubArrayUnroll<B, T, 0u, Pack<List...>, list...> {
    using Type = SubArray<B, T, Pack<List...>, list...>;
};

template <class B, class T, unsigned n, T (* ... list)()>
struct SubArrayType {
    using Type = typename SubArrayUnroll<B, T, n, Pack<>, list...>::Type;
};

template <class B, class T, class ... List, T (* ... list)()>
inline constexpr SubArray<B, T, Pack<List...>, list...>::SubArray(List ... args)
: B { list()..., args... } {}

template <class B, class T, class ... List, T (* ... list)()>
inline T const& SubArray<B, T, Pack<List...>, list...>::operator[](unsigned int index) const {
    return B::operator[](sizeof...(list) + index);
}

template <class B, class T, class ... List, T (* ... list)()>
inline T& SubArray<B, T, Pack<List...>, list...>::operator[](unsigned int index) {
    return B::operator[](sizeof...(list) + index);
}

} /* namespace Memory */
#endif /* MEMORY_SUBARRAY_H */
